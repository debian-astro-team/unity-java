// Demo of use of the Unity C library
//
// To build:
//
//     % cc -o unity-basic -I path/to/includes -L path/to/lib -lunity unity-basic.c
//     % ./unity-basic mm/s

#include <stdio.h>

#include "unity.h"

static char* progname;

int main(int argc, char** argv)
{
    progname = argv[0];

    UnitySyntax input_syntax = UNITY_SYNTAX_VOUNITS;
    UnitySyntax output_syntax = UNITY_SYNTAX_DEBUG;

    for (argc--, argv++; argc>0; argc--, argv++) {
        const UnitExpression* parsed;

        if ((parsed = unity_parse_string(*argv, input_syntax)) == NULL) {
            fprintf(stderr, "parse error: %s\n", unity_parse_error());
        } else {
            char buf[1024];
            printf("%s\n", unity_write_formatted(buf, 1024, parsed, output_syntax));

            printf("check: all units recognised?           %s\n",
                   unity_check_expression(parsed, input_syntax, UNITY_CHECK_RECOGNISED) ? "yes" : "no");
            printf("check: all units recommended?          %s\n",
                   unity_check_expression(parsed, input_syntax, UNITY_CHECK_RECOMMENDED) ? "yes" : "no");
            printf("check: all units satisfy constraints?  %s\n",
                   unity_check_expression(parsed, input_syntax, UNITY_CHECK_CONSTRAINTS) ? "yes" : "no");

            unity_free_expression(parsed);
        }
    }
}
