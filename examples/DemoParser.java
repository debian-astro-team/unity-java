// How to use the Unity Java library.
//
// To build and run:
//
//     % javac -cp path/to/unity.jar DemoParser.java
//     % java -cp .:path/to/unity.jar DemoParser mm/s

import uk.me.nxg.unity.*;

class DemoParser {
    public static void main(String[] args) {
        Syntax inputSyntax = Syntax.VOUNITS;
        Syntax outputSyntax = Syntax.FITS;

        try {
            UnitParser p = new UnitParser(inputSyntax);

            for (String arg : args) {
                UnitExpr e = p.parse(arg);

                boolean allRecognised = e.allUnitsRecognised(inputSyntax);
                boolean allRecognisedWithGuess = e.allUnitsRecognised(inputSyntax, true);
                boolean allRecommended = e.allUnitsRecommended(inputSyntax);
                boolean allConstraints = e.allUsageConstraintsSatisfied(inputSyntax);

                System.out.println(e.toString(outputSyntax));
                System.out.println("all recognised?      " + (allRecognised ? "yes" : "no"));
                System.out.println("...with guessing?    " + (allRecognisedWithGuess ? "yes" : "no"));
                System.out.println("all recommended?     " + (allRecommended ? "yes" : "no"));
                System.out.println("all constraints OK?  " + (allConstraints ? "yes" : "no"));
            }
        } catch (Exception e) {
            System.err.println("Exception: " + e);
        }
    }
}
