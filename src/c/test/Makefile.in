LEX=@LEX@
CC=@CC@
CPPFLAGS=@CPPFLAGS@
CFLAGS=@CFLAGS@

# To run with valgrind, do
#    make RUNTESTS=/path/to/valgrind\ --leak-check=yes\ ./runtests
RUNTESTS=./runtests

PARSERS=@PARSERLIST@

../../grammar/testcases-%.csv: ../../grammar/testcases.csv
	cd ../../grammar; $(MAKE) `basename $@`

runtests: runtests.o ../libunity.a
	$(CC) -o $@ $(CPPFLAGS) $(CFLAGS) -DYY_NO_INPUT=1 runtests.o -L.. -lunity -lm

runtests.c: runtests.lex
	$(LEX) -oruntests.c runtests.lex

unit-tests: unit-tests.o lcut/lcut.o ../libunity.a
	$(CC) -o $@ $(CPPFLAGS) $(CFLAGS) -Wall unit-tests.o lcut/lcut.o -L.. -lunity -lm

# The flex output includes a yyinput(), which is redundant in this
# case (and so causes a compilation error with -Wall -Werror), but
# which is omitted if (undocumented) YY_NO_INPUT is defined.
runtests.o: runtests.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -DYY_NO_INPUT=1 -c -o runtests.o runtests.c

lcut/lcut.o:
	cd lcut; $(CC) $(CFLAGS) $(CPPFLAGS) -c -o lcut.o lcut.c

check: runtests $(patsubst %,../../grammar/testcases-%.csv,$(PARSERS)) unit-tests
	./unit-tests
	@for parser in $(PARSERS); do \
		$(RUNTESTS) -p$$parser ../../grammar/testcases-$$parser.csv; \
	done


clean:
	rm -f *.o
	cd lcut; rm -f *.o
	rm -f runtests runtests.c unit-tests
