#ifndef KNOWN_SYNTAXES_H
#define KNOWN_SYNTAXES_H 1

/**
 * Constants for the various syntaxes which the library knows about.
 */
typedef enum {
    /**
     * Indicates the 'FITS' syntax.
     * Parses unit strings according to the prescriptions in the
     * FITS specification, v3.0, section 4.3
     * (W.D. Pence et al., A&amp;A 524, A42, 2010.
     * <a href='http://dx.doi.org/10.1051/0004-6361/201015362'
     *    >doi:10.1051/0004-6361/201015362</a>).
     * @see #unity_identify_parser
     */
    UNITY_SYNTAX_FITS,
    /**
     * Indicates the 'OGIP' syntax.
     * The format defined in OGIP memo OGIP/93-001, 1993
     * (<a href='ftp://legacy.gsfc.nasa.gov/fits_info/fits_formats/docs/general/ogip_93_001/ogip_93_001.ps'
     *    >postscript via FTP</a>)
     * @see #unity_identify_parser
     */
    UNITY_SYNTAX_OGIP,
    /**
     * Indicates the 'CDS' syntax.
     * A syntax based on the CDS document
     * <em>Standards for Astronomical Catalogues, Version 2.0</em>, 2000,
     * specifically section 3.2.
     * See <a href='http://cdsweb.u-strasbg.fr/doc/catstd-3.2.htx'
     *     ><code>http://cdsweb.u-strasbg.fr/doc/catstd-3.2.htx</code></a>
     * @see #unity_identify_parser
     */
    UNITY_SYNTAX_CDS,
    /**
     * Indicates the 'VOUNITS' syntax.
     * This is intended to be
     * as nearly as possible in the intersection of the various
     * other grammars. It is a strict subset of the FITS and CDS
     * grammars (in the sense that any VOUnit unit string is a valid
     * FITS and CDS string, too), and it is almost a subset of the
     * OGIP grammar, except that it uses the dot for multiplication
     * rather than star.  See <a
     * href='http://ivoa.net/Documents/VOUnits/' >IVOA VOUnits
     * Proposed Recommendation</a>.
     * @see #unity_identify_parser
     */
    UNITY_SYNTAX_VOUNITS,
    /**
     * Indicates the 'LaTeX' syntax.
     * This produces output which is intended to be used with the
     * LaTeX 'siunitx' package.
     * This is for writing only.
     */
    UNITY_SYNTAX_LATEX,
    /**
     * The formatter (not parser) for debugging output.  This is
     * intended to display the results of a parse unambiguously.  The
     * format of the output is not specified, and may change without
     * notice.
     */
    UNITY_SYNTAX_DEBUG,
    /**
     * A non-syntax.  This is an error return value.
     */
    UNITY_SYNTAX_NONE,
} UnitySyntax;

#if 0
/* XXX DELETE */
#ifdef UNITY_INTERNAL
/* Number of syntaxes */
#define U_NSYNTAXES 6
#endif
#endif

#endif /* KNOWN_SYNTAXES_H */
