/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     SIGNED_INTEGER = 258,
     UNSIGNED_INTEGER = 259,
     FLOAT = 260,
     STRING = 261,
     QUOTED_STRING = 262,
     VOUFLOAT = 263,
     CDSFLOAT = 264,
     WHITESPACE = 265,
     STARSTAR = 266,
     CARET = 267,
     DIVISION = 268,
     DOT = 269,
     STAR = 270,
     PERCENT = 271,
     OPEN_P = 272,
     CLOSE_P = 273,
     OPEN_SQ = 274,
     CLOSE_SQ = 275,
     LIT10 = 276,
     LIT1 = 277
   };
#endif
/* Tokens.  */
#define SIGNED_INTEGER 258
#define UNSIGNED_INTEGER 259
#define FLOAT 260
#define STRING 261
#define QUOTED_STRING 262
#define VOUFLOAT 263
#define CDSFLOAT 264
#define WHITESPACE 265
#define STARSTAR 266
#define CARET 267
#define DIVISION 268
#define DOT 269
#define STAR 270
#define PERCENT 271
#define OPEN_P 272
#define CLOSE_P 273
#define OPEN_SQ 274
#define CLOSE_SQ 275
#define LIT10 276
#define LIT1 277




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 14 "unity-c,fits.y"
{
        int i;
        float f;
        float l10f;  /* log10(f) */
        char c;
        char* s;
        struct unit_struct* u;
        struct unit_struct* uList;
        // uExpr appears only in the VOUnits case
        struct unit_expression* uExpr;
}
/* Line 1529 of yacc.c.  */
#line 105 "yy-unity-fits.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

