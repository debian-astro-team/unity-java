#ifndef WRITERS_H_LOADED
#define WRITERS_H_LOADED 1

#include <stdio.h>
#include "unitylib.h"

#ifdef UNITY_INTERNAL
typedef struct {
    int nwritten;
    int len;
    char* buf;
} Buf;
typedef Buf* (*writef_ptr) (Buf*, const UnitExpression*);
#endif

char* unity_write_formatted(char* buf, int buflen, const UnitExpression*, UnitySyntax);
const char** unity_formatter_names(void);

#endif
