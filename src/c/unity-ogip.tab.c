#include "known-syntaxes.h"
static UnitySyntax PARSE_SYNTAX = UNITY_SYNTAX_OGIP;
#define ogip_yylex yylex
#define ogip_yyerror yyerror
#define ogip_yylval yylval
#define ogip_yydebug yydebug
/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with ogip_yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum ogip_yytokentype {
     SIGNED_INTEGER = 258,
     UNSIGNED_INTEGER = 259,
     FLOAT = 260,
     STRING = 261,
     QUOTED_STRING = 262,
     VOUFLOAT = 263,
     CDSFLOAT = 264,
     WHITESPACE = 265,
     STARSTAR = 266,
     CARET = 267,
     DIVISION = 268,
     DOT = 269,
     STAR = 270,
     PERCENT = 271,
     OPEN_P = 272,
     CLOSE_P = 273,
     OPEN_SQ = 274,
     CLOSE_SQ = 275,
     LIT10 = 276,
     LIT1 = 277
   };
#endif
/* Tokens.  */
#define SIGNED_INTEGER 258
#define UNSIGNED_INTEGER 259
#define FLOAT 260
#define STRING 261
#define QUOTED_STRING 262
#define VOUFLOAT 263
#define CDSFLOAT 264
#define WHITESPACE 265
#define STARSTAR 266
#define CARET 267
#define DIVISION 268
#define DOT 269
#define STAR 270
#define PERCENT 271
#define OPEN_P 272
#define CLOSE_P 273
#define OPEN_SQ 274
#define CLOSE_SQ 275
#define LIT10 276
#define LIT1 277




/* Copy the first part of user declarations.  */
#line 3 "unity-c,ogip.y"

    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #define UNITY_INTERNAL 1
    #include "unity.h"
    int ogip_yylex(void);
    void ogip_yyerror(const char*);
    void force_lexer_state(int);


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 14 "unity-c,ogip.y"
{
        int i;
        float f;
        float l10f;  /* log10(f) */
        char c;
        char* s;
        struct unit_struct* u;
        struct unit_struct* uList;
        // uExpr appears only in the VOUnits case
        struct unit_expression* uExpr;
}
/* Line 193 of yacc.c.  */
#line 163 "ogip_yy-unity-ogip.tab.c"
	YYSTYPE;
# define ogip_yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 176 "ogip_yy-unity-ogip.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 ogip_yytype_uint8;
#else
typedef unsigned char ogip_yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 ogip_yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char ogip_yytype_int8;
#else
typedef short int ogip_yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 ogip_yytype_uint16;
#else
typedef unsigned short int ogip_yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 ogip_yytype_int16;
#else
typedef short int ogip_yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined ogip_yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined ogip_yyoverflow || YYERROR_VERBOSE */


#if (! defined ogip_yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union ogip_yyalloc
{
  ogip_yytype_int16 ogip_yyss;
  YYSTYPE ogip_yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union ogip_yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (ogip_yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T ogip_yyi;				\
	  for (ogip_yyi = 0; ogip_yyi < (Count); ogip_yyi++)	\
	    (To)[ogip_yyi] = (From)[ogip_yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T ogip_yynewbytes;						\
	YYCOPY (&ogip_yyptr->Stack, Stack, ogip_yysize);				\
	Stack = &ogip_yyptr->Stack;						\
	ogip_yynewbytes = ogip_yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	ogip_yyptr += ogip_yynewbytes / sizeof (*ogip_yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  22
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   60

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  23
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  15
/* YYNRULES -- Number of rules.  */
#define YYNRULES  37
/* YYNRULES -- Number of states.  */
#define YYNSTATES  58

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   277

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? ogip_yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const ogip_yytype_uint8 ogip_yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const ogip_yytype_uint8 ogip_yyprhs[] =
{
       0,     0,     3,     5,     8,    12,    14,    16,    19,    23,
      27,    29,    31,    35,    40,    44,    46,    48,    50,    53,
      57,    60,    62,    66,    68,    70,    72,    74,    76,    80,
      84,    90,    92,    94,    96,    98,   101,   105
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const ogip_yytype_int8 ogip_yyrhs[] =
{
      24,     0,    -1,    25,    -1,    29,    25,    -1,    29,    10,
      25,    -1,    26,    -1,    27,    -1,    30,    27,    -1,    26,
      37,    27,    -1,    26,    30,    27,    -1,    31,    -1,    28,
      -1,    17,    25,    18,    -1,     6,    17,    25,    18,    -1,
      21,    33,    34,    -1,    21,    -1,     5,    -1,    13,    -1,
      10,    13,    -1,    10,    13,    10,    -1,    13,    10,    -1,
      32,    -1,    32,    33,    34,    -1,     6,    -1,    11,    -1,
       4,    -1,     5,    -1,    35,    -1,    17,    36,    18,    -1,
      17,     5,    18,    -1,    17,    36,    30,     4,    18,    -1,
       3,    -1,     4,    -1,    10,    -1,    15,    -1,    10,    15,
      -1,    10,    15,    10,    -1,    15,    10,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const ogip_yytype_uint8 ogip_yyrline[] =
{
       0,    65,    65,    68,    74,    79,    87,    88,    91,    94,
      99,   102,   105,   110,   114,   117,   122,   135,   135,   136,
     136,   138,   139,   144,   149,   151,   156,   163,   166,   169,
     172,   178,   178,   180,   180,   180,   181,   181
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const ogip_yytname[] =
{
  "$end", "error", "$undefined", "SIGNED_INTEGER", "UNSIGNED_INTEGER",
  "FLOAT", "STRING", "QUOTED_STRING", "VOUFLOAT", "CDSFLOAT", "WHITESPACE",
  "STARSTAR", "CARET", "DIVISION", "DOT", "STAR", "PERCENT", "OPEN_P",
  "CLOSE_P", "OPEN_SQ", "CLOSE_SQ", "LIT10", "LIT1", "$accept", "input",
  "complete_expression", "product_of_units", "unit_expression",
  "function_application", "scalefactor", "division", "term", "unit",
  "power", "numeric_power", "parenthesized_number", "integer", "product", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const ogip_yytype_uint16 ogip_yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const ogip_yytype_uint8 ogip_yyr1[] =
{
       0,    23,    24,    24,    24,    25,    26,    26,    26,    26,
      27,    27,    27,    28,    29,    29,    29,    30,    30,    30,
      30,    31,    31,    32,    33,    34,    34,    34,    35,    35,
      35,    36,    36,    37,    37,    37,    37,    37
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const ogip_yytype_uint8 ogip_yyr2[] =
{
       0,     2,     1,     2,     3,     1,     1,     2,     3,     3,
       1,     1,     3,     4,     3,     1,     1,     1,     2,     3,
       2,     1,     3,     1,     1,     1,     1,     1,     3,     3,
       5,     1,     1,     1,     1,     2,     3,     2
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const ogip_yytype_uint8 ogip_yydefact[] =
{
       0,    16,    23,     0,    17,     0,    15,     0,     2,     5,
       6,    11,     0,     0,    10,    21,     0,    18,    20,     0,
      24,     0,     1,    33,    34,     0,     0,     0,     3,     7,
       0,     0,    19,    12,    25,    26,     0,    14,    27,    35,
      37,     9,     8,    17,     4,    22,    13,    31,    32,     0,
       0,    36,    19,    29,    28,     0,     0,    30
};

/* YYDEFGOTO[NTERM-NUM].  */
static const ogip_yytype_int8 ogip_yydefgoto[] =
{
      -1,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      21,    37,    38,    50,    26
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -12
static const ogip_yytype_int8 ogip_yypact[] =
{
      -3,   -12,   -11,    -2,    11,     3,    14,    24,   -12,    30,
     -12,   -12,    16,    13,   -12,    14,     3,    27,   -12,    31,
     -12,     0,   -12,    19,    40,    13,    13,    25,   -12,   -12,
       0,    33,   -12,   -12,   -12,   -12,    43,   -12,   -12,    42,
     -12,   -12,   -12,    44,   -12,   -12,   -12,   -12,   -12,    35,
      26,   -12,   -12,   -12,   -12,    51,    38,   -12
};

/* YYPGOTO[NTERM-NUM].  */
static const ogip_yytype_int8 ogip_yypgoto[] =
{
     -12,   -12,    -4,   -12,     2,   -12,   -12,    -9,   -12,   -12,
      45,    28,   -12,   -12,   -12
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const ogip_yytype_uint8 ogip_yytable[] =
{
      25,    19,     1,     2,    34,    35,    16,     3,    28,     2,
       4,    17,    31,     3,     5,    29,     4,    36,     6,     2,
       5,    18,     2,    44,    22,    20,    27,    41,    42,     4,
       5,     2,    17,     5,    39,     3,     3,    32,    43,     4,
      23,    55,     5,     4,    54,    24,    47,    48,    49,    33,
      40,    46,    51,    53,    52,    56,    57,     0,    45,     0,
      30
};

static const ogip_yytype_int8 ogip_yycheck[] =
{
       9,     5,     5,     6,     4,     5,    17,    10,    12,     6,
      13,    13,    16,    10,    17,    13,    13,    17,    21,     6,
      17,    10,     6,    27,     0,    11,    10,    25,    26,    13,
      17,     6,    13,    17,    15,    10,    10,    10,    13,    13,
      10,    50,    17,    13,    18,    15,     3,     4,     5,    18,
      10,    18,    10,    18,    10,     4,    18,    -1,    30,    -1,
      15
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const ogip_yytype_uint8 ogip_yystos[] =
{
       0,     5,     6,    10,    13,    17,    21,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    17,    13,    10,    25,
      11,    33,     0,    10,    15,    30,    37,    10,    25,    27,
      33,    25,    10,    18,     4,     5,    17,    34,    35,    15,
      10,    27,    27,    13,    25,    34,    18,     3,     4,     5,
      36,    10,    10,    18,    18,    30,     4,    18
};

#define ogip_yyerrok		(ogip_yyerrstatus = 0)
#define ogip_yyclearin	(ogip_yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto ogip_yyacceptlab
#define YYABORT		goto ogip_yyabortlab
#define YYERROR		goto ogip_yyerrorlab


/* Like YYERROR except do call ogip_yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto ogip_yyerrlab

#define YYRECOVERING()  (!!ogip_yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (ogip_yychar == YYEMPTY && ogip_yylen == 1)				\
    {								\
      ogip_yychar = (Token);						\
      ogip_yylval = (Value);						\
      ogip_yytoken = YYTRANSLATE (ogip_yychar);				\
      YYPOPSTACK (1);						\
      goto ogip_yybackup;						\
    }								\
  else								\
    {								\
      ogip_yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `ogip_yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX ogip_yylex (YYLEX_PARAM)
#else
# define YYLEX ogip_yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (ogip_yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (ogip_yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      ogip_yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
ogip_yy_symbol_value_print (FILE *ogip_yyoutput, int ogip_yytype, YYSTYPE const * const ogip_yyvaluep)
#else
static void
ogip_yy_symbol_value_print (ogip_yyoutput, ogip_yytype, ogip_yyvaluep)
    FILE *ogip_yyoutput;
    int ogip_yytype;
    YYSTYPE const * const ogip_yyvaluep;
#endif
{
  if (!ogip_yyvaluep)
    return;
# ifdef YYPRINT
  if (ogip_yytype < YYNTOKENS)
    YYPRINT (ogip_yyoutput, ogip_yytoknum[ogip_yytype], *ogip_yyvaluep);
# else
  YYUSE (ogip_yyoutput);
# endif
  switch (ogip_yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
ogip_yy_symbol_print (FILE *ogip_yyoutput, int ogip_yytype, YYSTYPE const * const ogip_yyvaluep)
#else
static void
ogip_yy_symbol_print (ogip_yyoutput, ogip_yytype, ogip_yyvaluep)
    FILE *ogip_yyoutput;
    int ogip_yytype;
    YYSTYPE const * const ogip_yyvaluep;
#endif
{
  if (ogip_yytype < YYNTOKENS)
    YYFPRINTF (ogip_yyoutput, "token %s (", ogip_yytname[ogip_yytype]);
  else
    YYFPRINTF (ogip_yyoutput, "nterm %s (", ogip_yytname[ogip_yytype]);

  ogip_yy_symbol_value_print (ogip_yyoutput, ogip_yytype, ogip_yyvaluep);
  YYFPRINTF (ogip_yyoutput, ")");
}

/*------------------------------------------------------------------.
| ogip_yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
ogip_yy_stack_print (ogip_yytype_int16 *bottom, ogip_yytype_int16 *top)
#else
static void
ogip_yy_stack_print (bottom, top)
    ogip_yytype_int16 *bottom;
    ogip_yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (ogip_yydebug)							\
    ogip_yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
ogip_yy_reduce_print (YYSTYPE *ogip_yyvsp, int ogip_yyrule)
#else
static void
ogip_yy_reduce_print (ogip_yyvsp, ogip_yyrule)
    YYSTYPE *ogip_yyvsp;
    int ogip_yyrule;
#endif
{
  int ogip_yynrhs = ogip_yyr2[ogip_yyrule];
  int ogip_yyi;
  unsigned long int ogip_yylno = ogip_yyrline[ogip_yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     ogip_yyrule - 1, ogip_yylno);
  /* The symbols being reduced.  */
  for (ogip_yyi = 0; ogip_yyi < ogip_yynrhs; ogip_yyi++)
    {
      fprintf (stderr, "   $%d = ", ogip_yyi + 1);
      ogip_yy_symbol_print (stderr, ogip_yyrhs[ogip_yyprhs[ogip_yyrule] + ogip_yyi],
		       &(ogip_yyvsp[(ogip_yyi + 1) - (ogip_yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (ogip_yydebug)				\
    ogip_yy_reduce_print (ogip_yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
extern int ogip_yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef ogip_yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define ogip_yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
ogip_yystrlen (const char *ogip_yystr)
#else
static YYSIZE_T
ogip_yystrlen (ogip_yystr)
    const char *ogip_yystr;
#endif
{
  YYSIZE_T ogip_yylen;
  for (ogip_yylen = 0; ogip_yystr[ogip_yylen]; ogip_yylen++)
    continue;
  return ogip_yylen;
}
#  endif
# endif

# ifndef ogip_yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define ogip_yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
ogip_yystpcpy (char *ogip_yydest, const char *ogip_yysrc)
#else
static char *
ogip_yystpcpy (ogip_yydest, ogip_yysrc)
    char *ogip_yydest;
    const char *ogip_yysrc;
#endif
{
  char *ogip_yyd = ogip_yydest;
  const char *ogip_yys = ogip_yysrc;

  while ((*ogip_yyd++ = *ogip_yys++) != '\0')
    continue;

  return ogip_yyd - 1;
}
#  endif
# endif

# ifndef ogip_yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for ogip_yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from ogip_yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
ogip_yytnamerr (char *ogip_yyres, const char *ogip_yystr)
{
  if (*ogip_yystr == '"')
    {
      YYSIZE_T ogip_yyn = 0;
      char const *ogip_yyp = ogip_yystr;

      for (;;)
	switch (*++ogip_yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++ogip_yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (ogip_yyres)
	      ogip_yyres[ogip_yyn] = *ogip_yyp;
	    ogip_yyn++;
	    break;

	  case '"':
	    if (ogip_yyres)
	      ogip_yyres[ogip_yyn] = '\0';
	    return ogip_yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! ogip_yyres)
    return ogip_yystrlen (ogip_yystr);

  return ogip_yystpcpy (ogip_yyres, ogip_yystr) - ogip_yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
ogip_yysyntax_error (char *ogip_yyresult, int ogip_yystate, int ogip_yychar)
{
  int ogip_yyn = ogip_yypact[ogip_yystate];

  if (! (YYPACT_NINF < ogip_yyn && ogip_yyn <= YYLAST))
    return 0;
  else
    {
      int ogip_yytype = YYTRANSLATE (ogip_yychar);
      YYSIZE_T ogip_yysize0 = ogip_yytnamerr (0, ogip_yytname[ogip_yytype]);
      YYSIZE_T ogip_yysize = ogip_yysize0;
      YYSIZE_T ogip_yysize1;
      int ogip_yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *ogip_yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int ogip_yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *ogip_yyfmt;
      char const *ogip_yyf;
      static char const ogip_yyunexpected[] = "syntax error, unexpected %s";
      static char const ogip_yyexpecting[] = ", expecting %s";
      static char const ogip_yyor[] = " or %s";
      char ogip_yyformat[sizeof ogip_yyunexpected
		    + sizeof ogip_yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof ogip_yyor - 1))];
      char const *ogip_yyprefix = ogip_yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int ogip_yyxbegin = ogip_yyn < 0 ? -ogip_yyn : 0;

      /* Stay within bounds of both ogip_yycheck and ogip_yytname.  */
      int ogip_yychecklim = YYLAST - ogip_yyn + 1;
      int ogip_yyxend = ogip_yychecklim < YYNTOKENS ? ogip_yychecklim : YYNTOKENS;
      int ogip_yycount = 1;

      ogip_yyarg[0] = ogip_yytname[ogip_yytype];
      ogip_yyfmt = ogip_yystpcpy (ogip_yyformat, ogip_yyunexpected);

      for (ogip_yyx = ogip_yyxbegin; ogip_yyx < ogip_yyxend; ++ogip_yyx)
	if (ogip_yycheck[ogip_yyx + ogip_yyn] == ogip_yyx && ogip_yyx != YYTERROR)
	  {
	    if (ogip_yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		ogip_yycount = 1;
		ogip_yysize = ogip_yysize0;
		ogip_yyformat[sizeof ogip_yyunexpected - 1] = '\0';
		break;
	      }
	    ogip_yyarg[ogip_yycount++] = ogip_yytname[ogip_yyx];
	    ogip_yysize1 = ogip_yysize + ogip_yytnamerr (0, ogip_yytname[ogip_yyx]);
	    ogip_yysize_overflow |= (ogip_yysize1 < ogip_yysize);
	    ogip_yysize = ogip_yysize1;
	    ogip_yyfmt = ogip_yystpcpy (ogip_yyfmt, ogip_yyprefix);
	    ogip_yyprefix = ogip_yyor;
	  }

      ogip_yyf = YY_(ogip_yyformat);
      ogip_yysize1 = ogip_yysize + ogip_yystrlen (ogip_yyf);
      ogip_yysize_overflow |= (ogip_yysize1 < ogip_yysize);
      ogip_yysize = ogip_yysize1;

      if (ogip_yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (ogip_yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *ogip_yyp = ogip_yyresult;
	  int ogip_yyi = 0;
	  while ((*ogip_yyp = *ogip_yyf) != '\0')
	    {
	      if (*ogip_yyp == '%' && ogip_yyf[1] == 's' && ogip_yyi < ogip_yycount)
		{
		  ogip_yyp += ogip_yytnamerr (ogip_yyp, ogip_yyarg[ogip_yyi++]);
		  ogip_yyf += 2;
		}
	      else
		{
		  ogip_yyp++;
		  ogip_yyf++;
		}
	    }
	}
      return ogip_yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
ogip_yydestruct (const char *ogip_yymsg, int ogip_yytype, YYSTYPE *ogip_yyvaluep)
#else
static void
ogip_yydestruct (ogip_yymsg, ogip_yytype, ogip_yyvaluep)
    const char *ogip_yymsg;
    int ogip_yytype;
    YYSTYPE *ogip_yyvaluep;
#endif
{
  YYUSE (ogip_yyvaluep);

  if (!ogip_yymsg)
    ogip_yymsg = "Deleting";
  YY_SYMBOL_PRINT (ogip_yymsg, ogip_yytype, ogip_yyvaluep, ogip_yylocationp);

  switch (ogip_yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int ogip_yyparse (void *YYPARSE_PARAM);
#else
int ogip_yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int ogip_yyparse (void);
#else
int ogip_yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int ogip_yychar;

/* The semantic value of the look-ahead symbol.  */
extern YYSTYPE ogip_yylval;

/* Number of syntax errors so far.  */
int ogip_yynerrs;



/*----------.
| ogip_yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
ogip_yyparse (void *YYPARSE_PARAM)
#else
int
ogip_yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
ogip_yyparse (void)
#else
int
ogip_yyparse ()

#endif
#endif
{
  
  int ogip_yystate;
  int ogip_yyn;
  int ogip_yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int ogip_yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int ogip_yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char ogip_yymsgbuf[128];
  char *ogip_yymsg = ogip_yymsgbuf;
  YYSIZE_T ogip_yymsg_alloc = sizeof ogip_yymsgbuf;
#endif

  /* Three stacks and their tools:
     `ogip_yyss': related to states,
     `ogip_yyvs': related to semantic values,
     `ogip_yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow ogip_yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  ogip_yytype_int16 ogip_yyssa[YYINITDEPTH];
  ogip_yytype_int16 *ogip_yyss = ogip_yyssa;
  ogip_yytype_int16 *ogip_yyssp;

  /* The semantic value stack.  */
  YYSTYPE ogip_yyvsa[YYINITDEPTH];
  YYSTYPE *ogip_yyvs = ogip_yyvsa;
  YYSTYPE *ogip_yyvsp;



#define YYPOPSTACK(N)   (ogip_yyvsp -= (N), ogip_yyssp -= (N))

  YYSIZE_T ogip_yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE ogip_yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int ogip_yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  ogip_yystate = 0;
  ogip_yyerrstatus = 0;
  ogip_yynerrs = 0;
  ogip_yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  ogip_yyssp = ogip_yyss;
  ogip_yyvsp = ogip_yyvs;

  goto ogip_yysetstate;

/*------------------------------------------------------------.
| ogip_yynewstate -- Push a new state, which is found in ogip_yystate.  |
`------------------------------------------------------------*/
 ogip_yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  ogip_yyssp++;

 ogip_yysetstate:
  *ogip_yyssp = ogip_yystate;

  if (ogip_yyss + ogip_yystacksize - 1 <= ogip_yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T ogip_yysize = ogip_yyssp - ogip_yyss + 1;

#ifdef ogip_yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *ogip_yyvs1 = ogip_yyvs;
	ogip_yytype_int16 *ogip_yyss1 = ogip_yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if ogip_yyoverflow is a macro.  */
	ogip_yyoverflow (YY_("memory exhausted"),
		    &ogip_yyss1, ogip_yysize * sizeof (*ogip_yyssp),
		    &ogip_yyvs1, ogip_yysize * sizeof (*ogip_yyvsp),

		    &ogip_yystacksize);

	ogip_yyss = ogip_yyss1;
	ogip_yyvs = ogip_yyvs1;
      }
#else /* no ogip_yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto ogip_yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= ogip_yystacksize)
	goto ogip_yyexhaustedlab;
      ogip_yystacksize *= 2;
      if (YYMAXDEPTH < ogip_yystacksize)
	ogip_yystacksize = YYMAXDEPTH;

      {
	ogip_yytype_int16 *ogip_yyss1 = ogip_yyss;
	union ogip_yyalloc *ogip_yyptr =
	  (union ogip_yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (ogip_yystacksize));
	if (! ogip_yyptr)
	  goto ogip_yyexhaustedlab;
	YYSTACK_RELOCATE (ogip_yyss);
	YYSTACK_RELOCATE (ogip_yyvs);

#  undef YYSTACK_RELOCATE
	if (ogip_yyss1 != ogip_yyssa)
	  YYSTACK_FREE (ogip_yyss1);
      }
# endif
#endif /* no ogip_yyoverflow */

      ogip_yyssp = ogip_yyss + ogip_yysize - 1;
      ogip_yyvsp = ogip_yyvs + ogip_yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) ogip_yystacksize));

      if (ogip_yyss + ogip_yystacksize - 1 <= ogip_yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", ogip_yystate));

  goto ogip_yybackup;

/*-----------.
| ogip_yybackup.  |
`-----------*/
ogip_yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  ogip_yyn = ogip_yypact[ogip_yystate];
  if (ogip_yyn == YYPACT_NINF)
    goto ogip_yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (ogip_yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      ogip_yychar = YYLEX;
    }

  if (ogip_yychar <= YYEOF)
    {
      ogip_yychar = ogip_yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      ogip_yytoken = YYTRANSLATE (ogip_yychar);
      YY_SYMBOL_PRINT ("Next token is", ogip_yytoken, &ogip_yylval, &ogip_yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  ogip_yyn += ogip_yytoken;
  if (ogip_yyn < 0 || YYLAST < ogip_yyn || ogip_yycheck[ogip_yyn] != ogip_yytoken)
    goto ogip_yydefault;
  ogip_yyn = ogip_yytable[ogip_yyn];
  if (ogip_yyn <= 0)
    {
      if (ogip_yyn == 0 || ogip_yyn == YYTABLE_NINF)
	goto ogip_yyerrlab;
      ogip_yyn = -ogip_yyn;
      goto ogip_yyreduce;
    }

  if (ogip_yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (ogip_yyerrstatus)
    ogip_yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", ogip_yytoken, &ogip_yylval, &ogip_yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (ogip_yychar != YYEOF)
    ogip_yychar = YYEMPTY;

  ogip_yystate = ogip_yyn;
  *++ogip_yyvsp = ogip_yylval;

  goto ogip_yynewstate;


/*-----------------------------------------------------------.
| ogip_yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
ogip_yydefault:
  ogip_yyn = ogip_yydefact[ogip_yystate];
  if (ogip_yyn == 0)
    goto ogip_yyerrlab;
  goto ogip_yyreduce;


/*-----------------------------.
| ogip_yyreduce -- Do a reduction.  |
`-----------------------------*/
ogip_yyreduce:
  /* ogip_yyn is the number of a rule to reduce with.  */
  ogip_yylen = ogip_yyr2[ogip_yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  ogip_yyval = ogip_yyvsp[1-ogip_yylen];


  YY_REDUCE_PRINT (ogip_yyn);
  switch (ogip_yyn)
    {
        case 2:
#line 65 "unity-c,ogip.y"
    {
                u_receive_result(u_new_expression(0, (ogip_yyvsp[(1) - (1)].uList)));
          }
    break;

  case 3:
#line 68 "unity-c,ogip.y"
    {
                u_receive_result(u_new_expression((ogip_yyvsp[(1) - (2)].f), (ogip_yyvsp[(2) - (2)].uList)));
        }
    break;

  case 4:
#line 74 "unity-c,ogip.y"
    {
                u_receive_result(u_new_expression((ogip_yyvsp[(1) - (3)].f), (ogip_yyvsp[(3) - (3)].uList)));
          }
    break;

  case 5:
#line 79 "unity-c,ogip.y"
    {
                (ogip_yyval.uList) = (ogip_yyvsp[(1) - (1)].uList);
          }
    break;

  case 7:
#line 88 "unity-c,ogip.y"
    {
                      (ogip_yyval.uList) = u_unit_reciprocal((ogip_yyvsp[(2) - (2)].uList));
          }
    break;

  case 8:
#line 91 "unity-c,ogip.y"
    {
                      (ogip_yyval.uList) = u_unit_append((ogip_yyvsp[(1) - (3)].uList), (ogip_yyvsp[(3) - (3)].uList));
          }
    break;

  case 9:
#line 94 "unity-c,ogip.y"
    {
                      (ogip_yyval.uList) = u_unit_append((ogip_yyvsp[(1) - (3)].uList), u_unit_reciprocal((ogip_yyvsp[(3) - (3)].uList)));
          }
    break;

  case 10:
#line 99 "unity-c,ogip.y"
    {
                (ogip_yyval.uList) = (ogip_yyvsp[(1) - (1)].u);
          }
    break;

  case 11:
#line 102 "unity-c,ogip.y"
    {
            (ogip_yyval.uList) = (ogip_yyvsp[(1) - (1)].uList);
          }
    break;

  case 12:
#line 105 "unity-c,ogip.y"
    {
            (ogip_yyval.uList) = (ogip_yyvsp[(2) - (3)].uList);
          }
    break;

  case 13:
#line 110 "unity-c,ogip.y"
    {
                      (ogip_yyval.uList) = u_function_application((ogip_yyvsp[(1) - (4)].s), 0.0, (ogip_yyvsp[(3) - (4)].uList), PARSE_SYNTAX);
          }
    break;

  case 14:
#line 114 "unity-c,ogip.y"
    { // eg 10^3
(ogip_yyval.f) = (ogip_yyvsp[(3) - (3)].f);
                  }
    break;

  case 15:
#line 117 "unity-c,ogip.y"
    {
            (ogip_yyval.f) = 1;  /* log(10) = 1 */
          }
    break;

  case 16:
#line 122 "unity-c,ogip.y"
    {
                      float l;
                      if ((ogip_yyvsp[(1) - (1)].f) <= 0.0) { ogip_yyerror("leading factors must be positive"); YYERROR; }
                      l = log10((ogip_yyvsp[(1) - (1)].f));
                      if (floorf(l) != l) { // check the base-10 log has no fractional part
                          ogip_yyerror("leading factors must be powers of ten"); // throws an exception
                          YYERROR; // throw an error
                      }
                      (ogip_yyval.f) = l;
          }
    break;

  case 21:
#line 138 "unity-c,ogip.y"
    { (ogip_yyval.u) = (ogip_yyvsp[(1) - (1)].u); }
    break;

  case 22:
#line 139 "unity-c,ogip.y"
    {
                      (ogip_yyval.u) = u_power_of_unit((ogip_yyvsp[(1) - (3)].u), (ogip_yyvsp[(3) - (3)].f));
          }
    break;

  case 23:
#line 144 "unity-c,ogip.y"
    {
                      (ogip_yyval.u) = u_new_unit((ogip_yyvsp[(1) - (1)].s), 1, PARSE_SYNTAX);
          }
    break;

  case 25:
#line 151 "unity-c,ogip.y"
    { (ogip_yyval.f) = (ogip_yyvsp[(1) - (1)].i); }
    break;

  case 26:
#line 156 "unity-c,ogip.y"
    {
            if ((ogip_yyvsp[(1) - (1)].f) < 0) {
                ogip_yyerror("Can't have bare negative power in exponent");
YYERROR;
            }
            (ogip_yyval.f) = (ogip_yyvsp[(1) - (1)].f);
          }
    break;

  case 27:
#line 163 "unity-c,ogip.y"
    { (ogip_yyval.f) = (ogip_yyvsp[(1) - (1)].f); }
    break;

  case 28:
#line 166 "unity-c,ogip.y"
    {
            (ogip_yyval.f) = (ogip_yyvsp[(2) - (3)].i);
          }
    break;

  case 29:
#line 169 "unity-c,ogip.y"
    {
            (ogip_yyval.f) = (ogip_yyvsp[(2) - (3)].f);
          }
    break;

  case 30:
#line 172 "unity-c,ogip.y"
    {
            // This is certainly OK for FITS, not clear for OGIP
            (ogip_yyval.f) = ((float)(ogip_yyvsp[(2) - (5)].i))/((float)((ogip_yyvsp[(4) - (5)].i)));
          }
    break;


/* Line 1267 of yacc.c.  */
#line 1582 "ogip_yy-unity-ogip.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", ogip_yyr1[ogip_yyn], &ogip_yyval, &ogip_yyloc);

  YYPOPSTACK (ogip_yylen);
  ogip_yylen = 0;
  YY_STACK_PRINT (ogip_yyss, ogip_yyssp);

  *++ogip_yyvsp = ogip_yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  ogip_yyn = ogip_yyr1[ogip_yyn];

  ogip_yystate = ogip_yypgoto[ogip_yyn - YYNTOKENS] + *ogip_yyssp;
  if (0 <= ogip_yystate && ogip_yystate <= YYLAST && ogip_yycheck[ogip_yystate] == *ogip_yyssp)
    ogip_yystate = ogip_yytable[ogip_yystate];
  else
    ogip_yystate = ogip_yydefgoto[ogip_yyn - YYNTOKENS];

  goto ogip_yynewstate;


/*------------------------------------.
| ogip_yyerrlab -- here on detecting error |
`------------------------------------*/
ogip_yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!ogip_yyerrstatus)
    {
      ++ogip_yynerrs;
#if ! YYERROR_VERBOSE
      ogip_yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T ogip_yysize = ogip_yysyntax_error (0, ogip_yystate, ogip_yychar);
	if (ogip_yymsg_alloc < ogip_yysize && ogip_yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T ogip_yyalloc = 2 * ogip_yysize;
	    if (! (ogip_yysize <= ogip_yyalloc && ogip_yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      ogip_yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (ogip_yymsg != ogip_yymsgbuf)
	      YYSTACK_FREE (ogip_yymsg);
	    ogip_yymsg = (char *) YYSTACK_ALLOC (ogip_yyalloc);
	    if (ogip_yymsg)
	      ogip_yymsg_alloc = ogip_yyalloc;
	    else
	      {
		ogip_yymsg = ogip_yymsgbuf;
		ogip_yymsg_alloc = sizeof ogip_yymsgbuf;
	      }
	  }

	if (0 < ogip_yysize && ogip_yysize <= ogip_yymsg_alloc)
	  {
	    (void) ogip_yysyntax_error (ogip_yymsg, ogip_yystate, ogip_yychar);
	    ogip_yyerror (ogip_yymsg);
	  }
	else
	  {
	    ogip_yyerror (YY_("syntax error"));
	    if (ogip_yysize != 0)
	      goto ogip_yyexhaustedlab;
	  }
      }
#endif
    }



  if (ogip_yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (ogip_yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (ogip_yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  ogip_yydestruct ("Error: discarding",
		      ogip_yytoken, &ogip_yylval);
	  ogip_yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto ogip_yyerrlab1;


/*---------------------------------------------------.
| ogip_yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
ogip_yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label ogip_yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto ogip_yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (ogip_yylen);
  ogip_yylen = 0;
  YY_STACK_PRINT (ogip_yyss, ogip_yyssp);
  ogip_yystate = *ogip_yyssp;
  goto ogip_yyerrlab1;


/*-------------------------------------------------------------.
| ogip_yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
ogip_yyerrlab1:
  ogip_yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      ogip_yyn = ogip_yypact[ogip_yystate];
      if (ogip_yyn != YYPACT_NINF)
	{
	  ogip_yyn += YYTERROR;
	  if (0 <= ogip_yyn && ogip_yyn <= YYLAST && ogip_yycheck[ogip_yyn] == YYTERROR)
	    {
	      ogip_yyn = ogip_yytable[ogip_yyn];
	      if (0 < ogip_yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (ogip_yyssp == ogip_yyss)
	YYABORT;


      ogip_yydestruct ("Error: popping",
		  ogip_yystos[ogip_yystate], ogip_yyvsp);
      YYPOPSTACK (1);
      ogip_yystate = *ogip_yyssp;
      YY_STACK_PRINT (ogip_yyss, ogip_yyssp);
    }

  if (ogip_yyn == YYFINAL)
    YYACCEPT;

  *++ogip_yyvsp = ogip_yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", ogip_yystos[ogip_yyn], ogip_yyvsp, ogip_yylsp);

  ogip_yystate = ogip_yyn;
  goto ogip_yynewstate;


/*-------------------------------------.
| ogip_yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
ogip_yyacceptlab:
  ogip_yyresult = 0;
  goto ogip_yyreturn;

/*-----------------------------------.
| ogip_yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
ogip_yyabortlab:
  ogip_yyresult = 1;
  goto ogip_yyreturn;

#ifndef ogip_yyoverflow
/*-------------------------------------------------.
| ogip_yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
ogip_yyexhaustedlab:
  ogip_yyerror (YY_("memory exhausted"));
  ogip_yyresult = 2;
  /* Fall through.  */
#endif

ogip_yyreturn:
  if (ogip_yychar != YYEOF && ogip_yychar != YYEMPTY)
     ogip_yydestruct ("Cleanup: discarding lookahead",
		 ogip_yytoken, &ogip_yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (ogip_yylen);
  YY_STACK_PRINT (ogip_yyss, ogip_yyssp);
  while (ogip_yyssp != ogip_yyss)
    {
      ogip_yydestruct ("Cleanup: popping",
		  ogip_yystos[*ogip_yyssp], ogip_yyvsp);
      YYPOPSTACK (1);
    }
#ifndef ogip_yyoverflow
  if (ogip_yyss != ogip_yyssa)
    YYSTACK_FREE (ogip_yyss);
#endif
#if YYERROR_VERBOSE
  if (ogip_yymsg != ogip_yymsgbuf)
    YYSTACK_FREE (ogip_yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (ogip_yyresult);
}


#line 182 "unity-c,ogip.y"



