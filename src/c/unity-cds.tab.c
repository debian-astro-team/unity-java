#include "known-syntaxes.h"
static UnitySyntax PARSE_SYNTAX = UNITY_SYNTAX_CDS;
#define cds_yylex yylex
#define cds_yyerror yyerror
#define cds_yylval yylval
#define cds_yydebug yydebug
/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with cds_yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum cds_yytokentype {
     SIGNED_INTEGER = 258,
     UNSIGNED_INTEGER = 259,
     FLOAT = 260,
     STRING = 261,
     QUOTED_STRING = 262,
     VOUFLOAT = 263,
     CDSFLOAT = 264,
     WHITESPACE = 265,
     STARSTAR = 266,
     CARET = 267,
     DIVISION = 268,
     DOT = 269,
     STAR = 270,
     PERCENT = 271,
     OPEN_P = 272,
     CLOSE_P = 273,
     OPEN_SQ = 274,
     CLOSE_SQ = 275,
     LIT10 = 276,
     LIT1 = 277
   };
#endif
/* Tokens.  */
#define SIGNED_INTEGER 258
#define UNSIGNED_INTEGER 259
#define FLOAT 260
#define STRING 261
#define QUOTED_STRING 262
#define VOUFLOAT 263
#define CDSFLOAT 264
#define WHITESPACE 265
#define STARSTAR 266
#define CARET 267
#define DIVISION 268
#define DOT 269
#define STAR 270
#define PERCENT 271
#define OPEN_P 272
#define CLOSE_P 273
#define OPEN_SQ 274
#define CLOSE_SQ 275
#define LIT10 276
#define LIT1 277




/* Copy the first part of user declarations.  */
#line 3 "unity-c,cds.y"

    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #define UNITY_INTERNAL 1
    #include "unity.h"
    int cds_yylex(void);
    void cds_yyerror(const char*);
    void force_lexer_state(int);


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 14 "unity-c,cds.y"
{
        int i;
        float f;
        float l10f;  /* log10(f) */
        char c;
        char* s;
        struct unit_struct* u;
        struct unit_struct* uList;
        // uExpr appears only in the VOUnits case
        struct unit_expression* uExpr;
}
/* Line 193 of yacc.c.  */
#line 163 "cds_yy-unity-cds.tab.c"
	YYSTYPE;
# define cds_yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 176 "cds_yy-unity-cds.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 cds_yytype_uint8;
#else
typedef unsigned char cds_yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 cds_yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char cds_yytype_int8;
#else
typedef short int cds_yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 cds_yytype_uint16;
#else
typedef unsigned short int cds_yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 cds_yytype_int16;
#else
typedef short int cds_yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined cds_yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined cds_yyoverflow || YYERROR_VERBOSE */


#if (! defined cds_yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union cds_yyalloc
{
  cds_yytype_int16 cds_yyss;
  YYSTYPE cds_yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union cds_yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (cds_yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T cds_yyi;				\
	  for (cds_yyi = 0; cds_yyi < (Count); cds_yyi++)	\
	    (To)[cds_yyi] = (From)[cds_yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T cds_yynewbytes;						\
	YYCOPY (&cds_yyptr->Stack, Stack, cds_yysize);				\
	Stack = &cds_yyptr->Stack;						\
	cds_yynewbytes = cds_yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	cds_yyptr += cds_yynewbytes / sizeof (*cds_yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  24
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   35

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  23
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  14
/* YYNRULES -- Number of rules.  */
#define YYNRULES  28
/* YYNRULES -- Number of states.  */
#define YYNSTATES  39

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   277

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? cds_yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const cds_yytype_uint8 cds_yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const cds_yytype_uint8 cds_yyprhs[] =
{
       0,     0,     3,     5,     8,    10,    12,    15,    19,    23,
      25,    27,    31,    35,    39,    42,    44,    46,    48,    50,
      52,    54,    57,    59,    61,    63,    65,    67,    69
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const cds_yytype_int8 cds_yyrhs[] =
{
      24,     0,    -1,    25,    -1,    29,    25,    -1,    26,    -1,
      27,    -1,    30,    27,    -1,    26,    36,    27,    -1,    26,
      30,    27,    -1,    31,    -1,    28,    -1,    17,    25,    18,
      -1,    19,    25,    20,    -1,    21,    33,    34,    -1,    21,
       3,    -1,     4,    -1,    21,    -1,     9,    -1,     5,    -1,
      13,    -1,    32,    -1,    32,    34,    -1,     6,    -1,    16,
      -1,    11,    -1,    35,    -1,     3,    -1,     4,    -1,    14,
      -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const cds_yytype_uint8 cds_yyrline[] =
{
       0,    64,    64,    67,    72,    80,    81,    84,    87,    92,
      95,    98,   103,   108,   111,   114,   119,   124,   127,   133,
     135,   136,   141,   144,   149,   151,   155,   155,   157
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const cds_yytname[] =
{
  "$end", "error", "$undefined", "SIGNED_INTEGER", "UNSIGNED_INTEGER",
  "FLOAT", "STRING", "QUOTED_STRING", "VOUFLOAT", "CDSFLOAT", "WHITESPACE",
  "STARSTAR", "CARET", "DIVISION", "DOT", "STAR", "PERCENT", "OPEN_P",
  "CLOSE_P", "OPEN_SQ", "CLOSE_SQ", "LIT10", "LIT1", "$accept", "input",
  "complete_expression", "product_of_units", "unit_expression",
  "function_application", "scalefactor", "division", "term", "unit",
  "power", "numeric_power", "integer", "product", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const cds_yytype_uint16 cds_yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const cds_yytype_uint8 cds_yyr1[] =
{
       0,    23,    24,    24,    25,    26,    26,    26,    26,    27,
      27,    27,    28,    29,    29,    29,    29,    29,    29,    30,
      31,    31,    32,    32,    33,    34,    35,    35,    36
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const cds_yytype_uint8 cds_yyr2[] =
{
       0,     2,     1,     2,     1,     1,     2,     3,     3,     1,
       1,     3,     3,     3,     2,     1,     1,     1,     1,     1,
       1,     2,     1,     1,     1,     1,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const cds_yytype_uint8 cds_yydefact[] =
{
       0,    15,    18,    22,    17,    19,    23,     0,     0,    16,
       0,     2,     4,     5,    10,     0,     0,     9,    20,     0,
       0,    14,    24,     0,     1,    28,     0,     0,     3,     6,
      26,    27,    21,    25,    11,    12,    13,     8,     7
};

/* YYDEFGOTO[NTERM-NUM].  */
static const cds_yytype_int8 cds_yydefgoto[] =
{
      -1,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      23,    32,    33,    27
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -11
static const cds_yytype_int8 cds_yypact[] =
{
      -4,   -11,   -11,   -11,   -11,   -11,   -11,     5,     5,    22,
       8,   -11,   -10,   -11,   -11,     5,    10,   -11,    31,    -8,
       3,   -11,   -11,    31,   -11,   -11,    10,    10,   -11,   -11,
     -11,   -11,   -11,   -11,   -11,   -11,   -11,   -11,   -11
};

/* YYPGOTO[NTERM-NUM].  */
static const cds_yytype_int8 cds_yypgoto[] =
{
     -11,   -11,    -1,   -11,     4,   -11,   -11,     7,   -11,   -11,
     -11,     9,   -11,   -11
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const cds_yytype_uint8 cds_yytable[] =
{
       1,     2,     3,     5,    25,     4,    19,    20,    24,     5,
      34,     3,     6,     7,    28,     8,     3,     9,     5,    26,
      29,     6,     7,    35,     8,    21,     6,     7,     0,     8,
      37,    38,    36,    22,    30,    31
};

static const cds_yytype_int8 cds_yycheck[] =
{
       4,     5,     6,    13,    14,     9,     7,     8,     0,    13,
      18,     6,    16,    17,    15,    19,     6,    21,    13,    12,
      16,    16,    17,    20,    19,     3,    16,    17,    -1,    19,
      26,    27,    23,    11,     3,     4
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const cds_yytype_uint8 cds_yystos[] =
{
       0,     4,     5,     6,     9,    13,    16,    17,    19,    21,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    25,
      25,     3,    11,    33,     0,    14,    30,    36,    25,    27,
       3,     4,    34,    35,    18,    20,    34,    27,    27
};

#define cds_yyerrok		(cds_yyerrstatus = 0)
#define cds_yyclearin	(cds_yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto cds_yyacceptlab
#define YYABORT		goto cds_yyabortlab
#define YYERROR		goto cds_yyerrorlab


/* Like YYERROR except do call cds_yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto cds_yyerrlab

#define YYRECOVERING()  (!!cds_yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (cds_yychar == YYEMPTY && cds_yylen == 1)				\
    {								\
      cds_yychar = (Token);						\
      cds_yylval = (Value);						\
      cds_yytoken = YYTRANSLATE (cds_yychar);				\
      YYPOPSTACK (1);						\
      goto cds_yybackup;						\
    }								\
  else								\
    {								\
      cds_yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `cds_yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX cds_yylex (YYLEX_PARAM)
#else
# define YYLEX cds_yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (cds_yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (cds_yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      cds_yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
cds_yy_symbol_value_print (FILE *cds_yyoutput, int cds_yytype, YYSTYPE const * const cds_yyvaluep)
#else
static void
cds_yy_symbol_value_print (cds_yyoutput, cds_yytype, cds_yyvaluep)
    FILE *cds_yyoutput;
    int cds_yytype;
    YYSTYPE const * const cds_yyvaluep;
#endif
{
  if (!cds_yyvaluep)
    return;
# ifdef YYPRINT
  if (cds_yytype < YYNTOKENS)
    YYPRINT (cds_yyoutput, cds_yytoknum[cds_yytype], *cds_yyvaluep);
# else
  YYUSE (cds_yyoutput);
# endif
  switch (cds_yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
cds_yy_symbol_print (FILE *cds_yyoutput, int cds_yytype, YYSTYPE const * const cds_yyvaluep)
#else
static void
cds_yy_symbol_print (cds_yyoutput, cds_yytype, cds_yyvaluep)
    FILE *cds_yyoutput;
    int cds_yytype;
    YYSTYPE const * const cds_yyvaluep;
#endif
{
  if (cds_yytype < YYNTOKENS)
    YYFPRINTF (cds_yyoutput, "token %s (", cds_yytname[cds_yytype]);
  else
    YYFPRINTF (cds_yyoutput, "nterm %s (", cds_yytname[cds_yytype]);

  cds_yy_symbol_value_print (cds_yyoutput, cds_yytype, cds_yyvaluep);
  YYFPRINTF (cds_yyoutput, ")");
}

/*------------------------------------------------------------------.
| cds_yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
cds_yy_stack_print (cds_yytype_int16 *bottom, cds_yytype_int16 *top)
#else
static void
cds_yy_stack_print (bottom, top)
    cds_yytype_int16 *bottom;
    cds_yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (cds_yydebug)							\
    cds_yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
cds_yy_reduce_print (YYSTYPE *cds_yyvsp, int cds_yyrule)
#else
static void
cds_yy_reduce_print (cds_yyvsp, cds_yyrule)
    YYSTYPE *cds_yyvsp;
    int cds_yyrule;
#endif
{
  int cds_yynrhs = cds_yyr2[cds_yyrule];
  int cds_yyi;
  unsigned long int cds_yylno = cds_yyrline[cds_yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     cds_yyrule - 1, cds_yylno);
  /* The symbols being reduced.  */
  for (cds_yyi = 0; cds_yyi < cds_yynrhs; cds_yyi++)
    {
      fprintf (stderr, "   $%d = ", cds_yyi + 1);
      cds_yy_symbol_print (stderr, cds_yyrhs[cds_yyprhs[cds_yyrule] + cds_yyi],
		       &(cds_yyvsp[(cds_yyi + 1) - (cds_yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (cds_yydebug)				\
    cds_yy_reduce_print (cds_yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
extern int cds_yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef cds_yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define cds_yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
cds_yystrlen (const char *cds_yystr)
#else
static YYSIZE_T
cds_yystrlen (cds_yystr)
    const char *cds_yystr;
#endif
{
  YYSIZE_T cds_yylen;
  for (cds_yylen = 0; cds_yystr[cds_yylen]; cds_yylen++)
    continue;
  return cds_yylen;
}
#  endif
# endif

# ifndef cds_yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define cds_yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
cds_yystpcpy (char *cds_yydest, const char *cds_yysrc)
#else
static char *
cds_yystpcpy (cds_yydest, cds_yysrc)
    char *cds_yydest;
    const char *cds_yysrc;
#endif
{
  char *cds_yyd = cds_yydest;
  const char *cds_yys = cds_yysrc;

  while ((*cds_yyd++ = *cds_yys++) != '\0')
    continue;

  return cds_yyd - 1;
}
#  endif
# endif

# ifndef cds_yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for cds_yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from cds_yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
cds_yytnamerr (char *cds_yyres, const char *cds_yystr)
{
  if (*cds_yystr == '"')
    {
      YYSIZE_T cds_yyn = 0;
      char const *cds_yyp = cds_yystr;

      for (;;)
	switch (*++cds_yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++cds_yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (cds_yyres)
	      cds_yyres[cds_yyn] = *cds_yyp;
	    cds_yyn++;
	    break;

	  case '"':
	    if (cds_yyres)
	      cds_yyres[cds_yyn] = '\0';
	    return cds_yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! cds_yyres)
    return cds_yystrlen (cds_yystr);

  return cds_yystpcpy (cds_yyres, cds_yystr) - cds_yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
cds_yysyntax_error (char *cds_yyresult, int cds_yystate, int cds_yychar)
{
  int cds_yyn = cds_yypact[cds_yystate];

  if (! (YYPACT_NINF < cds_yyn && cds_yyn <= YYLAST))
    return 0;
  else
    {
      int cds_yytype = YYTRANSLATE (cds_yychar);
      YYSIZE_T cds_yysize0 = cds_yytnamerr (0, cds_yytname[cds_yytype]);
      YYSIZE_T cds_yysize = cds_yysize0;
      YYSIZE_T cds_yysize1;
      int cds_yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *cds_yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int cds_yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *cds_yyfmt;
      char const *cds_yyf;
      static char const cds_yyunexpected[] = "syntax error, unexpected %s";
      static char const cds_yyexpecting[] = ", expecting %s";
      static char const cds_yyor[] = " or %s";
      char cds_yyformat[sizeof cds_yyunexpected
		    + sizeof cds_yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof cds_yyor - 1))];
      char const *cds_yyprefix = cds_yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int cds_yyxbegin = cds_yyn < 0 ? -cds_yyn : 0;

      /* Stay within bounds of both cds_yycheck and cds_yytname.  */
      int cds_yychecklim = YYLAST - cds_yyn + 1;
      int cds_yyxend = cds_yychecklim < YYNTOKENS ? cds_yychecklim : YYNTOKENS;
      int cds_yycount = 1;

      cds_yyarg[0] = cds_yytname[cds_yytype];
      cds_yyfmt = cds_yystpcpy (cds_yyformat, cds_yyunexpected);

      for (cds_yyx = cds_yyxbegin; cds_yyx < cds_yyxend; ++cds_yyx)
	if (cds_yycheck[cds_yyx + cds_yyn] == cds_yyx && cds_yyx != YYTERROR)
	  {
	    if (cds_yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		cds_yycount = 1;
		cds_yysize = cds_yysize0;
		cds_yyformat[sizeof cds_yyunexpected - 1] = '\0';
		break;
	      }
	    cds_yyarg[cds_yycount++] = cds_yytname[cds_yyx];
	    cds_yysize1 = cds_yysize + cds_yytnamerr (0, cds_yytname[cds_yyx]);
	    cds_yysize_overflow |= (cds_yysize1 < cds_yysize);
	    cds_yysize = cds_yysize1;
	    cds_yyfmt = cds_yystpcpy (cds_yyfmt, cds_yyprefix);
	    cds_yyprefix = cds_yyor;
	  }

      cds_yyf = YY_(cds_yyformat);
      cds_yysize1 = cds_yysize + cds_yystrlen (cds_yyf);
      cds_yysize_overflow |= (cds_yysize1 < cds_yysize);
      cds_yysize = cds_yysize1;

      if (cds_yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (cds_yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *cds_yyp = cds_yyresult;
	  int cds_yyi = 0;
	  while ((*cds_yyp = *cds_yyf) != '\0')
	    {
	      if (*cds_yyp == '%' && cds_yyf[1] == 's' && cds_yyi < cds_yycount)
		{
		  cds_yyp += cds_yytnamerr (cds_yyp, cds_yyarg[cds_yyi++]);
		  cds_yyf += 2;
		}
	      else
		{
		  cds_yyp++;
		  cds_yyf++;
		}
	    }
	}
      return cds_yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
cds_yydestruct (const char *cds_yymsg, int cds_yytype, YYSTYPE *cds_yyvaluep)
#else
static void
cds_yydestruct (cds_yymsg, cds_yytype, cds_yyvaluep)
    const char *cds_yymsg;
    int cds_yytype;
    YYSTYPE *cds_yyvaluep;
#endif
{
  YYUSE (cds_yyvaluep);

  if (!cds_yymsg)
    cds_yymsg = "Deleting";
  YY_SYMBOL_PRINT (cds_yymsg, cds_yytype, cds_yyvaluep, cds_yylocationp);

  switch (cds_yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int cds_yyparse (void *YYPARSE_PARAM);
#else
int cds_yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int cds_yyparse (void);
#else
int cds_yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int cds_yychar;

/* The semantic value of the look-ahead symbol.  */
extern YYSTYPE cds_yylval;

/* Number of syntax errors so far.  */
int cds_yynerrs;



/*----------.
| cds_yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
cds_yyparse (void *YYPARSE_PARAM)
#else
int
cds_yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
cds_yyparse (void)
#else
int
cds_yyparse ()

#endif
#endif
{
  
  int cds_yystate;
  int cds_yyn;
  int cds_yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int cds_yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int cds_yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char cds_yymsgbuf[128];
  char *cds_yymsg = cds_yymsgbuf;
  YYSIZE_T cds_yymsg_alloc = sizeof cds_yymsgbuf;
#endif

  /* Three stacks and their tools:
     `cds_yyss': related to states,
     `cds_yyvs': related to semantic values,
     `cds_yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow cds_yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  cds_yytype_int16 cds_yyssa[YYINITDEPTH];
  cds_yytype_int16 *cds_yyss = cds_yyssa;
  cds_yytype_int16 *cds_yyssp;

  /* The semantic value stack.  */
  YYSTYPE cds_yyvsa[YYINITDEPTH];
  YYSTYPE *cds_yyvs = cds_yyvsa;
  YYSTYPE *cds_yyvsp;



#define YYPOPSTACK(N)   (cds_yyvsp -= (N), cds_yyssp -= (N))

  YYSIZE_T cds_yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE cds_yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int cds_yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  cds_yystate = 0;
  cds_yyerrstatus = 0;
  cds_yynerrs = 0;
  cds_yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  cds_yyssp = cds_yyss;
  cds_yyvsp = cds_yyvs;

  goto cds_yysetstate;

/*------------------------------------------------------------.
| cds_yynewstate -- Push a new state, which is found in cds_yystate.  |
`------------------------------------------------------------*/
 cds_yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  cds_yyssp++;

 cds_yysetstate:
  *cds_yyssp = cds_yystate;

  if (cds_yyss + cds_yystacksize - 1 <= cds_yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T cds_yysize = cds_yyssp - cds_yyss + 1;

#ifdef cds_yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *cds_yyvs1 = cds_yyvs;
	cds_yytype_int16 *cds_yyss1 = cds_yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if cds_yyoverflow is a macro.  */
	cds_yyoverflow (YY_("memory exhausted"),
		    &cds_yyss1, cds_yysize * sizeof (*cds_yyssp),
		    &cds_yyvs1, cds_yysize * sizeof (*cds_yyvsp),

		    &cds_yystacksize);

	cds_yyss = cds_yyss1;
	cds_yyvs = cds_yyvs1;
      }
#else /* no cds_yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto cds_yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= cds_yystacksize)
	goto cds_yyexhaustedlab;
      cds_yystacksize *= 2;
      if (YYMAXDEPTH < cds_yystacksize)
	cds_yystacksize = YYMAXDEPTH;

      {
	cds_yytype_int16 *cds_yyss1 = cds_yyss;
	union cds_yyalloc *cds_yyptr =
	  (union cds_yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (cds_yystacksize));
	if (! cds_yyptr)
	  goto cds_yyexhaustedlab;
	YYSTACK_RELOCATE (cds_yyss);
	YYSTACK_RELOCATE (cds_yyvs);

#  undef YYSTACK_RELOCATE
	if (cds_yyss1 != cds_yyssa)
	  YYSTACK_FREE (cds_yyss1);
      }
# endif
#endif /* no cds_yyoverflow */

      cds_yyssp = cds_yyss + cds_yysize - 1;
      cds_yyvsp = cds_yyvs + cds_yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) cds_yystacksize));

      if (cds_yyss + cds_yystacksize - 1 <= cds_yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", cds_yystate));

  goto cds_yybackup;

/*-----------.
| cds_yybackup.  |
`-----------*/
cds_yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  cds_yyn = cds_yypact[cds_yystate];
  if (cds_yyn == YYPACT_NINF)
    goto cds_yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (cds_yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      cds_yychar = YYLEX;
    }

  if (cds_yychar <= YYEOF)
    {
      cds_yychar = cds_yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      cds_yytoken = YYTRANSLATE (cds_yychar);
      YY_SYMBOL_PRINT ("Next token is", cds_yytoken, &cds_yylval, &cds_yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  cds_yyn += cds_yytoken;
  if (cds_yyn < 0 || YYLAST < cds_yyn || cds_yycheck[cds_yyn] != cds_yytoken)
    goto cds_yydefault;
  cds_yyn = cds_yytable[cds_yyn];
  if (cds_yyn <= 0)
    {
      if (cds_yyn == 0 || cds_yyn == YYTABLE_NINF)
	goto cds_yyerrlab;
      cds_yyn = -cds_yyn;
      goto cds_yyreduce;
    }

  if (cds_yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (cds_yyerrstatus)
    cds_yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", cds_yytoken, &cds_yylval, &cds_yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (cds_yychar != YYEOF)
    cds_yychar = YYEMPTY;

  cds_yystate = cds_yyn;
  *++cds_yyvsp = cds_yylval;

  goto cds_yynewstate;


/*-----------------------------------------------------------.
| cds_yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
cds_yydefault:
  cds_yyn = cds_yydefact[cds_yystate];
  if (cds_yyn == 0)
    goto cds_yyerrlab;
  goto cds_yyreduce;


/*-----------------------------.
| cds_yyreduce -- Do a reduction.  |
`-----------------------------*/
cds_yyreduce:
  /* cds_yyn is the number of a rule to reduce with.  */
  cds_yylen = cds_yyr2[cds_yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  cds_yyval = cds_yyvsp[1-cds_yylen];


  YY_REDUCE_PRINT (cds_yyn);
  switch (cds_yyn)
    {
        case 2:
#line 64 "unity-c,cds.y"
    {
                u_receive_result(u_new_expression(0, (cds_yyvsp[(1) - (1)].uList)));
          }
    break;

  case 3:
#line 67 "unity-c,cds.y"
    {
                u_receive_result(u_new_expression((cds_yyvsp[(1) - (2)].f), (cds_yyvsp[(2) - (2)].uList)));
        }
    break;

  case 4:
#line 72 "unity-c,cds.y"
    {
                (cds_yyval.uList) = (cds_yyvsp[(1) - (1)].uList);
          }
    break;

  case 6:
#line 81 "unity-c,cds.y"
    {
                      (cds_yyval.uList) = u_unit_reciprocal((cds_yyvsp[(2) - (2)].uList));
          }
    break;

  case 7:
#line 84 "unity-c,cds.y"
    {
                      (cds_yyval.uList) = u_unit_append((cds_yyvsp[(1) - (3)].uList), (cds_yyvsp[(3) - (3)].uList));
          }
    break;

  case 8:
#line 87 "unity-c,cds.y"
    {
                      (cds_yyval.uList) = u_unit_append((cds_yyvsp[(1) - (3)].uList), u_unit_reciprocal((cds_yyvsp[(3) - (3)].uList)));
          }
    break;

  case 9:
#line 92 "unity-c,cds.y"
    {
                (cds_yyval.uList) = (cds_yyvsp[(1) - (1)].u);
          }
    break;

  case 10:
#line 95 "unity-c,cds.y"
    {
            (cds_yyval.uList) = (cds_yyvsp[(1) - (1)].uList);
          }
    break;

  case 11:
#line 98 "unity-c,cds.y"
    {
            (cds_yyval.uList) = (cds_yyvsp[(2) - (3)].uList);
          }
    break;

  case 12:
#line 103 "unity-c,cds.y"
    {
                      (cds_yyval.uList) = u_function_application("log", 0.0, (cds_yyvsp[(2) - (3)].uList), UNITY_SYNTAX_CDS);
          }
    break;

  case 13:
#line 108 "unity-c,cds.y"
    { // eg 10^3
(cds_yyval.f) = (cds_yyvsp[(3) - (3)].f);
                  }
    break;

  case 14:
#line 111 "unity-c,cds.y"
    { // eg 10+3
                      (cds_yyval.f) = (cds_yyvsp[(2) - (2)].i);
          }
    break;

  case 15:
#line 114 "unity-c,cds.y"
    {
                      float factor = (cds_yyvsp[(1) - (1)].i);
                      if (factor <= 0.0) { cds_yyerror("leading factors must be positive"); YYERROR; }
                      (cds_yyval.f) = log10(factor);
          }
    break;

  case 16:
#line 119 "unity-c,cds.y"
    {
            (cds_yyval.f) = 1;  /* log(10) = 1 */
          }
    break;

  case 17:
#line 124 "unity-c,cds.y"
    { // eg 1.5x10+11
                      (cds_yyval.f) = (cds_yyvsp[(1) - (1)].l10f);
          }
    break;

  case 18:
#line 127 "unity-c,cds.y"
    {
                      if ((cds_yyvsp[(1) - (1)].f) <= 0.0) { cds_yyerror("leading factors must be positive"); YYERROR; }
                      (cds_yyval.f) = log10((cds_yyvsp[(1) - (1)].f));
          }
    break;

  case 20:
#line 135 "unity-c,cds.y"
    { (cds_yyval.u) = (cds_yyvsp[(1) - (1)].u); }
    break;

  case 21:
#line 136 "unity-c,cds.y"
    {
                      (cds_yyval.u) = u_power_of_unit((cds_yyvsp[(1) - (2)].u), (cds_yyvsp[(2) - (2)].f));
          }
    break;

  case 22:
#line 141 "unity-c,cds.y"
    {
                      (cds_yyval.u) = u_new_unit((cds_yyvsp[(1) - (1)].s), 1, PARSE_SYNTAX);
          }
    break;

  case 23:
#line 144 "unity-c,cds.y"
    {
                      (cds_yyval.u) = u_new_unit("%", 1, PARSE_SYNTAX);
          }
    break;

  case 25:
#line 151 "unity-c,cds.y"
    { (cds_yyval.f) = (cds_yyvsp[(1) - (1)].i); }
    break;


/* Line 1267 of yacc.c.  */
#line 1542 "cds_yy-unity-cds.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", cds_yyr1[cds_yyn], &cds_yyval, &cds_yyloc);

  YYPOPSTACK (cds_yylen);
  cds_yylen = 0;
  YY_STACK_PRINT (cds_yyss, cds_yyssp);

  *++cds_yyvsp = cds_yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  cds_yyn = cds_yyr1[cds_yyn];

  cds_yystate = cds_yypgoto[cds_yyn - YYNTOKENS] + *cds_yyssp;
  if (0 <= cds_yystate && cds_yystate <= YYLAST && cds_yycheck[cds_yystate] == *cds_yyssp)
    cds_yystate = cds_yytable[cds_yystate];
  else
    cds_yystate = cds_yydefgoto[cds_yyn - YYNTOKENS];

  goto cds_yynewstate;


/*------------------------------------.
| cds_yyerrlab -- here on detecting error |
`------------------------------------*/
cds_yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!cds_yyerrstatus)
    {
      ++cds_yynerrs;
#if ! YYERROR_VERBOSE
      cds_yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T cds_yysize = cds_yysyntax_error (0, cds_yystate, cds_yychar);
	if (cds_yymsg_alloc < cds_yysize && cds_yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T cds_yyalloc = 2 * cds_yysize;
	    if (! (cds_yysize <= cds_yyalloc && cds_yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      cds_yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (cds_yymsg != cds_yymsgbuf)
	      YYSTACK_FREE (cds_yymsg);
	    cds_yymsg = (char *) YYSTACK_ALLOC (cds_yyalloc);
	    if (cds_yymsg)
	      cds_yymsg_alloc = cds_yyalloc;
	    else
	      {
		cds_yymsg = cds_yymsgbuf;
		cds_yymsg_alloc = sizeof cds_yymsgbuf;
	      }
	  }

	if (0 < cds_yysize && cds_yysize <= cds_yymsg_alloc)
	  {
	    (void) cds_yysyntax_error (cds_yymsg, cds_yystate, cds_yychar);
	    cds_yyerror (cds_yymsg);
	  }
	else
	  {
	    cds_yyerror (YY_("syntax error"));
	    if (cds_yysize != 0)
	      goto cds_yyexhaustedlab;
	  }
      }
#endif
    }



  if (cds_yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (cds_yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (cds_yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  cds_yydestruct ("Error: discarding",
		      cds_yytoken, &cds_yylval);
	  cds_yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto cds_yyerrlab1;


/*---------------------------------------------------.
| cds_yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
cds_yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label cds_yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto cds_yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (cds_yylen);
  cds_yylen = 0;
  YY_STACK_PRINT (cds_yyss, cds_yyssp);
  cds_yystate = *cds_yyssp;
  goto cds_yyerrlab1;


/*-------------------------------------------------------------.
| cds_yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
cds_yyerrlab1:
  cds_yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      cds_yyn = cds_yypact[cds_yystate];
      if (cds_yyn != YYPACT_NINF)
	{
	  cds_yyn += YYTERROR;
	  if (0 <= cds_yyn && cds_yyn <= YYLAST && cds_yycheck[cds_yyn] == YYTERROR)
	    {
	      cds_yyn = cds_yytable[cds_yyn];
	      if (0 < cds_yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (cds_yyssp == cds_yyss)
	YYABORT;


      cds_yydestruct ("Error: popping",
		  cds_yystos[cds_yystate], cds_yyvsp);
      YYPOPSTACK (1);
      cds_yystate = *cds_yyssp;
      YY_STACK_PRINT (cds_yyss, cds_yyssp);
    }

  if (cds_yyn == YYFINAL)
    YYACCEPT;

  *++cds_yyvsp = cds_yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", cds_yystos[cds_yyn], cds_yyvsp, cds_yylsp);

  cds_yystate = cds_yyn;
  goto cds_yynewstate;


/*-------------------------------------.
| cds_yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
cds_yyacceptlab:
  cds_yyresult = 0;
  goto cds_yyreturn;

/*-----------------------------------.
| cds_yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
cds_yyabortlab:
  cds_yyresult = 1;
  goto cds_yyreturn;

#ifndef cds_yyoverflow
/*-------------------------------------------------.
| cds_yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
cds_yyexhaustedlab:
  cds_yyerror (YY_("memory exhausted"));
  cds_yyresult = 2;
  /* Fall through.  */
#endif

cds_yyreturn:
  if (cds_yychar != YYEOF && cds_yychar != YYEMPTY)
     cds_yydestruct ("Cleanup: discarding lookahead",
		 cds_yytoken, &cds_yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (cds_yylen);
  YY_STACK_PRINT (cds_yyss, cds_yyssp);
  while (cds_yyssp != cds_yyss)
    {
      cds_yydestruct ("Cleanup: popping",
		  cds_yystos[*cds_yyssp], cds_yyvsp);
      YYPOPSTACK (1);
    }
#ifndef cds_yyoverflow
  if (cds_yyss != cds_yyssa)
    YYSTACK_FREE (cds_yyss);
#endif
#if YYERROR_VERBOSE
  if (cds_yymsg != cds_yymsgbuf)
    YYSTACK_FREE (cds_yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (cds_yyresult);
}


#line 158 "unity-c,cds.y"



