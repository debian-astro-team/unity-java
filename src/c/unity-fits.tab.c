#include "known-syntaxes.h"
static UnitySyntax PARSE_SYNTAX = UNITY_SYNTAX_FITS;
#define fits_yylex yylex
#define fits_yyerror yyerror
#define fits_yylval yylval
#define fits_yydebug yydebug
/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with fits_yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum fits_yytokentype {
     SIGNED_INTEGER = 258,
     UNSIGNED_INTEGER = 259,
     FLOAT = 260,
     STRING = 261,
     QUOTED_STRING = 262,
     VOUFLOAT = 263,
     CDSFLOAT = 264,
     WHITESPACE = 265,
     STARSTAR = 266,
     CARET = 267,
     DIVISION = 268,
     DOT = 269,
     STAR = 270,
     PERCENT = 271,
     OPEN_P = 272,
     CLOSE_P = 273,
     OPEN_SQ = 274,
     CLOSE_SQ = 275,
     LIT10 = 276,
     LIT1 = 277
   };
#endif
/* Tokens.  */
#define SIGNED_INTEGER 258
#define UNSIGNED_INTEGER 259
#define FLOAT 260
#define STRING 261
#define QUOTED_STRING 262
#define VOUFLOAT 263
#define CDSFLOAT 264
#define WHITESPACE 265
#define STARSTAR 266
#define CARET 267
#define DIVISION 268
#define DOT 269
#define STAR 270
#define PERCENT 271
#define OPEN_P 272
#define CLOSE_P 273
#define OPEN_SQ 274
#define CLOSE_SQ 275
#define LIT10 276
#define LIT1 277




/* Copy the first part of user declarations.  */
#line 3 "unity-c,fits.y"

    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #define UNITY_INTERNAL 1
    #include "unity.h"
    int fits_yylex(void);
    void fits_yyerror(const char*);
    void force_lexer_state(int);


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 14 "unity-c,fits.y"
{
        int i;
        float f;
        float l10f;  /* log10(f) */
        char c;
        char* s;
        struct unit_struct* u;
        struct unit_struct* uList;
        // uExpr appears only in the VOUnits case
        struct unit_expression* uExpr;
}
/* Line 193 of yacc.c.  */
#line 163 "fits_yy-unity-fits.tab.c"
	YYSTYPE;
# define fits_yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 176 "fits_yy-unity-fits.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 fits_yytype_uint8;
#else
typedef unsigned char fits_yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 fits_yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char fits_yytype_int8;
#else
typedef short int fits_yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 fits_yytype_uint16;
#else
typedef unsigned short int fits_yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 fits_yytype_int16;
#else
typedef short int fits_yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined fits_yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined fits_yyoverflow || YYERROR_VERBOSE */


#if (! defined fits_yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union fits_yyalloc
{
  fits_yytype_int16 fits_yyss;
  YYSTYPE fits_yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union fits_yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (fits_yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T fits_yyi;				\
	  for (fits_yyi = 0; fits_yyi < (Count); fits_yyi++)	\
	    (To)[fits_yyi] = (From)[fits_yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T fits_yynewbytes;						\
	YYCOPY (&fits_yyptr->Stack, Stack, fits_yysize);				\
	Stack = &fits_yyptr->Stack;						\
	fits_yynewbytes = fits_yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	fits_yyptr += fits_yynewbytes / sizeof (*fits_yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  21
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   55

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  23
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  15
/* YYNRULES -- Number of rules.  */
#define YYNRULES  33
/* YYNRULES -- Number of states.  */
#define YYNSTATES  52

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   277

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? fits_yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const fits_yytype_uint8 fits_yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const fits_yytype_uint8 fits_yyprhs[] =
{
       0,     0,     3,     5,     8,    12,    15,    17,    21,    23,
      27,    29,    32,    34,    38,    43,    47,    50,    52,    54,
      57,    61,    63,    65,    67,    69,    71,    75,    79,    85,
      87,    89,    91,    93
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const fits_yytype_int8 fits_yyrhs[] =
{
      24,     0,    -1,    25,    -1,    29,    25,    -1,    29,    10,
      25,    -1,    30,    27,    -1,    26,    -1,    26,    30,    27,
      -1,    27,    -1,    26,    37,    27,    -1,    31,    -1,     6,
      35,    -1,    28,    -1,    17,    25,    18,    -1,     6,    17,
      25,    18,    -1,    21,    33,    34,    -1,    21,     3,    -1,
      13,    -1,    32,    -1,    32,    34,    -1,    32,    33,    34,
      -1,     6,    -1,    12,    -1,    11,    -1,    36,    -1,    35,
      -1,    17,    36,    18,    -1,    17,     5,    18,    -1,    17,
      36,    30,     4,    18,    -1,     3,    -1,     4,    -1,    10,
      -1,    15,    -1,    14,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const fits_yytype_uint8 fits_yyrline[] =
{
       0,    65,    65,    68,    74,    79,    84,   108,   113,   116,
     121,   125,   128,   131,   136,   140,   143,   148,   150,   151,
     154,   159,   164,   165,   168,   169,   172,   175,   178,   184,
     184,   186,   186,   186
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const fits_yytname[] =
{
  "$end", "error", "$undefined", "SIGNED_INTEGER", "UNSIGNED_INTEGER",
  "FLOAT", "STRING", "QUOTED_STRING", "VOUFLOAT", "CDSFLOAT", "WHITESPACE",
  "STARSTAR", "CARET", "DIVISION", "DOT", "STAR", "PERCENT", "OPEN_P",
  "CLOSE_P", "OPEN_SQ", "CLOSE_SQ", "LIT10", "LIT1", "$accept", "input",
  "complete_expression", "product_of_units", "unit_expression",
  "function_application", "scalefactor", "division", "term", "unit",
  "power", "numeric_power", "parenthesized_number", "integer", "product", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const fits_yytype_uint16 fits_yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const fits_yytype_uint8 fits_yyr1[] =
{
       0,    23,    24,    24,    24,    24,    25,    25,    26,    26,
      27,    27,    27,    27,    28,    29,    29,    30,    31,    31,
      31,    32,    33,    33,    34,    34,    35,    35,    35,    36,
      36,    37,    37,    37
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const fits_yytype_uint8 fits_yyr2[] =
{
       0,     2,     1,     2,     3,     2,     1,     3,     1,     3,
       1,     2,     1,     3,     4,     3,     2,     1,     1,     2,
       3,     1,     1,     1,     1,     1,     3,     3,     5,     1,
       1,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const fits_yytype_uint8 fits_yydefact[] =
{
       0,    21,    17,     0,     0,     0,     2,     6,     8,    12,
       0,     0,    10,    18,     0,    11,     0,    16,    23,    22,
       0,     1,    31,    33,    32,     0,     0,     0,     3,     5,
      29,    30,     0,     0,    19,    25,    24,     0,     0,     0,
      13,    15,     7,     9,     4,    20,    27,    14,    26,     0,
       0,    28
};

/* YYDEFGOTO[NTERM-NUM].  */
static const fits_yytype_int8 fits_yydefgoto[] =
{
      -1,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      20,    34,    35,    36,    26
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -17
static const fits_yytype_int8 fits_yypact[] =
{
      -3,    -8,   -17,    21,    28,    11,   -17,    31,   -17,   -17,
      20,    21,   -17,    12,    30,   -17,    -5,   -17,   -17,   -17,
       2,   -17,   -17,   -17,   -17,    21,    21,    21,   -17,   -17,
     -17,   -17,    46,     2,   -17,   -17,   -17,    10,    24,    35,
     -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,    39,
      34,   -17
};

/* YYPGOTO[NTERM-NUM].  */
static const fits_yytype_int8 fits_yypgoto[] =
{
     -17,   -17,    -2,   -17,    -4,   -17,   -17,    -7,   -17,   -17,
      41,   -16,    54,   -12,   -17
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const fits_yytype_uint8 fits_yytable[] =
{
      25,    16,    39,     1,    41,    30,    31,    29,    28,    14,
       2,    21,    38,    40,     3,    30,    31,    45,     4,    32,
      39,    42,    43,    18,    19,    44,     1,     1,    46,    32,
      27,    17,    49,    30,    31,    37,     1,     3,     3,    18,
      19,    22,    47,    50,     2,    23,    24,     3,     2,    30,
      31,    37,    51,    48,    33,    15
};

static const fits_yytype_uint8 fits_yycheck[] =
{
       7,     3,    14,     6,    20,     3,     4,    11,    10,    17,
      13,     0,    14,    18,    17,     3,     4,    33,    21,    17,
      32,    25,    26,    11,    12,    27,     6,     6,    18,    17,
      10,     3,    39,     3,     4,     5,     6,    17,    17,    11,
      12,    10,    18,     4,    13,    14,    15,    17,    13,     3,
       4,     5,    18,    18,    13,     1
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const fits_yytype_uint8 fits_yystos[] =
{
       0,     6,    13,    17,    21,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    17,    35,    25,     3,    11,    12,
      33,     0,    10,    14,    15,    30,    37,    10,    25,    27,
       3,     4,    17,    33,    34,    35,    36,     5,    25,    36,
      18,    34,    27,    27,    25,    34,    18,    18,    18,    30,
       4,    18
};

#define fits_yyerrok		(fits_yyerrstatus = 0)
#define fits_yyclearin	(fits_yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto fits_yyacceptlab
#define YYABORT		goto fits_yyabortlab
#define YYERROR		goto fits_yyerrorlab


/* Like YYERROR except do call fits_yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto fits_yyerrlab

#define YYRECOVERING()  (!!fits_yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (fits_yychar == YYEMPTY && fits_yylen == 1)				\
    {								\
      fits_yychar = (Token);						\
      fits_yylval = (Value);						\
      fits_yytoken = YYTRANSLATE (fits_yychar);				\
      YYPOPSTACK (1);						\
      goto fits_yybackup;						\
    }								\
  else								\
    {								\
      fits_yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `fits_yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX fits_yylex (YYLEX_PARAM)
#else
# define YYLEX fits_yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (fits_yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (fits_yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      fits_yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
fits_yy_symbol_value_print (FILE *fits_yyoutput, int fits_yytype, YYSTYPE const * const fits_yyvaluep)
#else
static void
fits_yy_symbol_value_print (fits_yyoutput, fits_yytype, fits_yyvaluep)
    FILE *fits_yyoutput;
    int fits_yytype;
    YYSTYPE const * const fits_yyvaluep;
#endif
{
  if (!fits_yyvaluep)
    return;
# ifdef YYPRINT
  if (fits_yytype < YYNTOKENS)
    YYPRINT (fits_yyoutput, fits_yytoknum[fits_yytype], *fits_yyvaluep);
# else
  YYUSE (fits_yyoutput);
# endif
  switch (fits_yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
fits_yy_symbol_print (FILE *fits_yyoutput, int fits_yytype, YYSTYPE const * const fits_yyvaluep)
#else
static void
fits_yy_symbol_print (fits_yyoutput, fits_yytype, fits_yyvaluep)
    FILE *fits_yyoutput;
    int fits_yytype;
    YYSTYPE const * const fits_yyvaluep;
#endif
{
  if (fits_yytype < YYNTOKENS)
    YYFPRINTF (fits_yyoutput, "token %s (", fits_yytname[fits_yytype]);
  else
    YYFPRINTF (fits_yyoutput, "nterm %s (", fits_yytname[fits_yytype]);

  fits_yy_symbol_value_print (fits_yyoutput, fits_yytype, fits_yyvaluep);
  YYFPRINTF (fits_yyoutput, ")");
}

/*------------------------------------------------------------------.
| fits_yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
fits_yy_stack_print (fits_yytype_int16 *bottom, fits_yytype_int16 *top)
#else
static void
fits_yy_stack_print (bottom, top)
    fits_yytype_int16 *bottom;
    fits_yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (fits_yydebug)							\
    fits_yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
fits_yy_reduce_print (YYSTYPE *fits_yyvsp, int fits_yyrule)
#else
static void
fits_yy_reduce_print (fits_yyvsp, fits_yyrule)
    YYSTYPE *fits_yyvsp;
    int fits_yyrule;
#endif
{
  int fits_yynrhs = fits_yyr2[fits_yyrule];
  int fits_yyi;
  unsigned long int fits_yylno = fits_yyrline[fits_yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     fits_yyrule - 1, fits_yylno);
  /* The symbols being reduced.  */
  for (fits_yyi = 0; fits_yyi < fits_yynrhs; fits_yyi++)
    {
      fprintf (stderr, "   $%d = ", fits_yyi + 1);
      fits_yy_symbol_print (stderr, fits_yyrhs[fits_yyprhs[fits_yyrule] + fits_yyi],
		       &(fits_yyvsp[(fits_yyi + 1) - (fits_yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (fits_yydebug)				\
    fits_yy_reduce_print (fits_yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
extern int fits_yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef fits_yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define fits_yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
fits_yystrlen (const char *fits_yystr)
#else
static YYSIZE_T
fits_yystrlen (fits_yystr)
    const char *fits_yystr;
#endif
{
  YYSIZE_T fits_yylen;
  for (fits_yylen = 0; fits_yystr[fits_yylen]; fits_yylen++)
    continue;
  return fits_yylen;
}
#  endif
# endif

# ifndef fits_yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define fits_yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
fits_yystpcpy (char *fits_yydest, const char *fits_yysrc)
#else
static char *
fits_yystpcpy (fits_yydest, fits_yysrc)
    char *fits_yydest;
    const char *fits_yysrc;
#endif
{
  char *fits_yyd = fits_yydest;
  const char *fits_yys = fits_yysrc;

  while ((*fits_yyd++ = *fits_yys++) != '\0')
    continue;

  return fits_yyd - 1;
}
#  endif
# endif

# ifndef fits_yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for fits_yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from fits_yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
fits_yytnamerr (char *fits_yyres, const char *fits_yystr)
{
  if (*fits_yystr == '"')
    {
      YYSIZE_T fits_yyn = 0;
      char const *fits_yyp = fits_yystr;

      for (;;)
	switch (*++fits_yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++fits_yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (fits_yyres)
	      fits_yyres[fits_yyn] = *fits_yyp;
	    fits_yyn++;
	    break;

	  case '"':
	    if (fits_yyres)
	      fits_yyres[fits_yyn] = '\0';
	    return fits_yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! fits_yyres)
    return fits_yystrlen (fits_yystr);

  return fits_yystpcpy (fits_yyres, fits_yystr) - fits_yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
fits_yysyntax_error (char *fits_yyresult, int fits_yystate, int fits_yychar)
{
  int fits_yyn = fits_yypact[fits_yystate];

  if (! (YYPACT_NINF < fits_yyn && fits_yyn <= YYLAST))
    return 0;
  else
    {
      int fits_yytype = YYTRANSLATE (fits_yychar);
      YYSIZE_T fits_yysize0 = fits_yytnamerr (0, fits_yytname[fits_yytype]);
      YYSIZE_T fits_yysize = fits_yysize0;
      YYSIZE_T fits_yysize1;
      int fits_yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *fits_yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int fits_yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *fits_yyfmt;
      char const *fits_yyf;
      static char const fits_yyunexpected[] = "syntax error, unexpected %s";
      static char const fits_yyexpecting[] = ", expecting %s";
      static char const fits_yyor[] = " or %s";
      char fits_yyformat[sizeof fits_yyunexpected
		    + sizeof fits_yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof fits_yyor - 1))];
      char const *fits_yyprefix = fits_yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int fits_yyxbegin = fits_yyn < 0 ? -fits_yyn : 0;

      /* Stay within bounds of both fits_yycheck and fits_yytname.  */
      int fits_yychecklim = YYLAST - fits_yyn + 1;
      int fits_yyxend = fits_yychecklim < YYNTOKENS ? fits_yychecklim : YYNTOKENS;
      int fits_yycount = 1;

      fits_yyarg[0] = fits_yytname[fits_yytype];
      fits_yyfmt = fits_yystpcpy (fits_yyformat, fits_yyunexpected);

      for (fits_yyx = fits_yyxbegin; fits_yyx < fits_yyxend; ++fits_yyx)
	if (fits_yycheck[fits_yyx + fits_yyn] == fits_yyx && fits_yyx != YYTERROR)
	  {
	    if (fits_yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		fits_yycount = 1;
		fits_yysize = fits_yysize0;
		fits_yyformat[sizeof fits_yyunexpected - 1] = '\0';
		break;
	      }
	    fits_yyarg[fits_yycount++] = fits_yytname[fits_yyx];
	    fits_yysize1 = fits_yysize + fits_yytnamerr (0, fits_yytname[fits_yyx]);
	    fits_yysize_overflow |= (fits_yysize1 < fits_yysize);
	    fits_yysize = fits_yysize1;
	    fits_yyfmt = fits_yystpcpy (fits_yyfmt, fits_yyprefix);
	    fits_yyprefix = fits_yyor;
	  }

      fits_yyf = YY_(fits_yyformat);
      fits_yysize1 = fits_yysize + fits_yystrlen (fits_yyf);
      fits_yysize_overflow |= (fits_yysize1 < fits_yysize);
      fits_yysize = fits_yysize1;

      if (fits_yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (fits_yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *fits_yyp = fits_yyresult;
	  int fits_yyi = 0;
	  while ((*fits_yyp = *fits_yyf) != '\0')
	    {
	      if (*fits_yyp == '%' && fits_yyf[1] == 's' && fits_yyi < fits_yycount)
		{
		  fits_yyp += fits_yytnamerr (fits_yyp, fits_yyarg[fits_yyi++]);
		  fits_yyf += 2;
		}
	      else
		{
		  fits_yyp++;
		  fits_yyf++;
		}
	    }
	}
      return fits_yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
fits_yydestruct (const char *fits_yymsg, int fits_yytype, YYSTYPE *fits_yyvaluep)
#else
static void
fits_yydestruct (fits_yymsg, fits_yytype, fits_yyvaluep)
    const char *fits_yymsg;
    int fits_yytype;
    YYSTYPE *fits_yyvaluep;
#endif
{
  YYUSE (fits_yyvaluep);

  if (!fits_yymsg)
    fits_yymsg = "Deleting";
  YY_SYMBOL_PRINT (fits_yymsg, fits_yytype, fits_yyvaluep, fits_yylocationp);

  switch (fits_yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int fits_yyparse (void *YYPARSE_PARAM);
#else
int fits_yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int fits_yyparse (void);
#else
int fits_yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int fits_yychar;

/* The semantic value of the look-ahead symbol.  */
extern YYSTYPE fits_yylval;

/* Number of syntax errors so far.  */
int fits_yynerrs;



/*----------.
| fits_yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
fits_yyparse (void *YYPARSE_PARAM)
#else
int
fits_yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
fits_yyparse (void)
#else
int
fits_yyparse ()

#endif
#endif
{
  
  int fits_yystate;
  int fits_yyn;
  int fits_yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int fits_yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int fits_yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char fits_yymsgbuf[128];
  char *fits_yymsg = fits_yymsgbuf;
  YYSIZE_T fits_yymsg_alloc = sizeof fits_yymsgbuf;
#endif

  /* Three stacks and their tools:
     `fits_yyss': related to states,
     `fits_yyvs': related to semantic values,
     `fits_yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow fits_yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  fits_yytype_int16 fits_yyssa[YYINITDEPTH];
  fits_yytype_int16 *fits_yyss = fits_yyssa;
  fits_yytype_int16 *fits_yyssp;

  /* The semantic value stack.  */
  YYSTYPE fits_yyvsa[YYINITDEPTH];
  YYSTYPE *fits_yyvs = fits_yyvsa;
  YYSTYPE *fits_yyvsp;



#define YYPOPSTACK(N)   (fits_yyvsp -= (N), fits_yyssp -= (N))

  YYSIZE_T fits_yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE fits_yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int fits_yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  fits_yystate = 0;
  fits_yyerrstatus = 0;
  fits_yynerrs = 0;
  fits_yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  fits_yyssp = fits_yyss;
  fits_yyvsp = fits_yyvs;

  goto fits_yysetstate;

/*------------------------------------------------------------.
| fits_yynewstate -- Push a new state, which is found in fits_yystate.  |
`------------------------------------------------------------*/
 fits_yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  fits_yyssp++;

 fits_yysetstate:
  *fits_yyssp = fits_yystate;

  if (fits_yyss + fits_yystacksize - 1 <= fits_yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T fits_yysize = fits_yyssp - fits_yyss + 1;

#ifdef fits_yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *fits_yyvs1 = fits_yyvs;
	fits_yytype_int16 *fits_yyss1 = fits_yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if fits_yyoverflow is a macro.  */
	fits_yyoverflow (YY_("memory exhausted"),
		    &fits_yyss1, fits_yysize * sizeof (*fits_yyssp),
		    &fits_yyvs1, fits_yysize * sizeof (*fits_yyvsp),

		    &fits_yystacksize);

	fits_yyss = fits_yyss1;
	fits_yyvs = fits_yyvs1;
      }
#else /* no fits_yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto fits_yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= fits_yystacksize)
	goto fits_yyexhaustedlab;
      fits_yystacksize *= 2;
      if (YYMAXDEPTH < fits_yystacksize)
	fits_yystacksize = YYMAXDEPTH;

      {
	fits_yytype_int16 *fits_yyss1 = fits_yyss;
	union fits_yyalloc *fits_yyptr =
	  (union fits_yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (fits_yystacksize));
	if (! fits_yyptr)
	  goto fits_yyexhaustedlab;
	YYSTACK_RELOCATE (fits_yyss);
	YYSTACK_RELOCATE (fits_yyvs);

#  undef YYSTACK_RELOCATE
	if (fits_yyss1 != fits_yyssa)
	  YYSTACK_FREE (fits_yyss1);
      }
# endif
#endif /* no fits_yyoverflow */

      fits_yyssp = fits_yyss + fits_yysize - 1;
      fits_yyvsp = fits_yyvs + fits_yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) fits_yystacksize));

      if (fits_yyss + fits_yystacksize - 1 <= fits_yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", fits_yystate));

  goto fits_yybackup;

/*-----------.
| fits_yybackup.  |
`-----------*/
fits_yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  fits_yyn = fits_yypact[fits_yystate];
  if (fits_yyn == YYPACT_NINF)
    goto fits_yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (fits_yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      fits_yychar = YYLEX;
    }

  if (fits_yychar <= YYEOF)
    {
      fits_yychar = fits_yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      fits_yytoken = YYTRANSLATE (fits_yychar);
      YY_SYMBOL_PRINT ("Next token is", fits_yytoken, &fits_yylval, &fits_yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  fits_yyn += fits_yytoken;
  if (fits_yyn < 0 || YYLAST < fits_yyn || fits_yycheck[fits_yyn] != fits_yytoken)
    goto fits_yydefault;
  fits_yyn = fits_yytable[fits_yyn];
  if (fits_yyn <= 0)
    {
      if (fits_yyn == 0 || fits_yyn == YYTABLE_NINF)
	goto fits_yyerrlab;
      fits_yyn = -fits_yyn;
      goto fits_yyreduce;
    }

  if (fits_yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (fits_yyerrstatus)
    fits_yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", fits_yytoken, &fits_yylval, &fits_yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (fits_yychar != YYEOF)
    fits_yychar = YYEMPTY;

  fits_yystate = fits_yyn;
  *++fits_yyvsp = fits_yylval;

  goto fits_yynewstate;


/*-----------------------------------------------------------.
| fits_yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
fits_yydefault:
  fits_yyn = fits_yydefact[fits_yystate];
  if (fits_yyn == 0)
    goto fits_yyerrlab;
  goto fits_yyreduce;


/*-----------------------------.
| fits_yyreduce -- Do a reduction.  |
`-----------------------------*/
fits_yyreduce:
  /* fits_yyn is the number of a rule to reduce with.  */
  fits_yylen = fits_yyr2[fits_yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  fits_yyval = fits_yyvsp[1-fits_yylen];


  YY_REDUCE_PRINT (fits_yyn);
  switch (fits_yyn)
    {
        case 2:
#line 65 "unity-c,fits.y"
    {
                u_receive_result(u_new_expression(0, (fits_yyvsp[(1) - (1)].uList)));
          }
    break;

  case 3:
#line 68 "unity-c,fits.y"
    {
                u_receive_result(u_new_expression((fits_yyvsp[(1) - (2)].f), (fits_yyvsp[(2) - (2)].uList)));
        }
    break;

  case 4:
#line 74 "unity-c,fits.y"
    {
                u_receive_result(u_new_expression((fits_yyvsp[(1) - (3)].f), (fits_yyvsp[(3) - (3)].uList)));
          }
    break;

  case 5:
#line 79 "unity-c,fits.y"
    {
                u_receive_result(u_new_expression(0, u_unit_reciprocal((fits_yyvsp[(2) - (2)].uList))));
          }
    break;

  case 6:
#line 84 "unity-c,fits.y"
    {
                (fits_yyval.uList) = (fits_yyvsp[(1) - (1)].uList);
          }
    break;

  case 7:
#line 108 "unity-c,fits.y"
    {
                (fits_yyval.uList) = u_unit_append((fits_yyvsp[(1) - (3)].uList), u_unit_reciprocal((fits_yyvsp[(3) - (3)].uList)));
          }
    break;

  case 8:
#line 113 "unity-c,fits.y"
    {
            (fits_yyval.uList) = (fits_yyvsp[(1) - (1)].uList);
          }
    break;

  case 9:
#line 116 "unity-c,fits.y"
    {
                      (fits_yyval.uList) = u_unit_append((fits_yyvsp[(1) - (3)].uList), (fits_yyvsp[(3) - (3)].uList));
          }
    break;

  case 10:
#line 121 "unity-c,fits.y"
    {
                (fits_yyval.uList) = (fits_yyvsp[(1) - (1)].u);
          }
    break;

  case 11:
#line 125 "unity-c,fits.y"
    {
                      (fits_yyval.uList) = u_new_unit((fits_yyvsp[(1) - (2)].s), (fits_yyvsp[(2) - (2)].f), PARSE_SYNTAX);
          }
    break;

  case 12:
#line 128 "unity-c,fits.y"
    {
            (fits_yyval.uList) = (fits_yyvsp[(1) - (1)].uList);
          }
    break;

  case 13:
#line 131 "unity-c,fits.y"
    {
            (fits_yyval.uList) = (fits_yyvsp[(2) - (3)].uList);
          }
    break;

  case 14:
#line 136 "unity-c,fits.y"
    {
                      (fits_yyval.uList) = u_function_application((fits_yyvsp[(1) - (4)].s), 0.0, (fits_yyvsp[(3) - (4)].uList), PARSE_SYNTAX);
          }
    break;

  case 15:
#line 140 "unity-c,fits.y"
    { // eg 10^3
(fits_yyval.f) = (fits_yyvsp[(3) - (3)].f);
                  }
    break;

  case 16:
#line 143 "unity-c,fits.y"
    { // eg 10+3
                      (fits_yyval.f) = (fits_yyvsp[(2) - (2)].i);
          }
    break;

  case 18:
#line 150 "unity-c,fits.y"
    { (fits_yyval.u) = (fits_yyvsp[(1) - (1)].u); }
    break;

  case 19:
#line 151 "unity-c,fits.y"
    {
                      (fits_yyval.u) = u_power_of_unit((fits_yyvsp[(1) - (2)].u), (fits_yyvsp[(2) - (2)].f));
          }
    break;

  case 20:
#line 154 "unity-c,fits.y"
    {
                      (fits_yyval.u) = u_power_of_unit((fits_yyvsp[(1) - (3)].u), (fits_yyvsp[(3) - (3)].f));
          }
    break;

  case 21:
#line 159 "unity-c,fits.y"
    {
                      (fits_yyval.u) = u_new_unit((fits_yyvsp[(1) - (1)].s), 1, PARSE_SYNTAX);
          }
    break;

  case 24:
#line 168 "unity-c,fits.y"
    { (fits_yyval.f) = (fits_yyvsp[(1) - (1)].i); }
    break;

  case 25:
#line 169 "unity-c,fits.y"
    { (fits_yyval.f) = (fits_yyvsp[(1) - (1)].f); }
    break;

  case 26:
#line 172 "unity-c,fits.y"
    {
            (fits_yyval.f) = (fits_yyvsp[(2) - (3)].i);
          }
    break;

  case 27:
#line 175 "unity-c,fits.y"
    {
            (fits_yyval.f) = (fits_yyvsp[(2) - (3)].f);
          }
    break;

  case 28:
#line 178 "unity-c,fits.y"
    {
            // This is certainly OK for FITS, not clear for OGIP
            (fits_yyval.f) = ((float)(fits_yyvsp[(2) - (5)].i))/((float)((fits_yyvsp[(4) - (5)].i)));
          }
    break;


/* Line 1267 of yacc.c.  */
#line 1575 "fits_yy-unity-fits.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", fits_yyr1[fits_yyn], &fits_yyval, &fits_yyloc);

  YYPOPSTACK (fits_yylen);
  fits_yylen = 0;
  YY_STACK_PRINT (fits_yyss, fits_yyssp);

  *++fits_yyvsp = fits_yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  fits_yyn = fits_yyr1[fits_yyn];

  fits_yystate = fits_yypgoto[fits_yyn - YYNTOKENS] + *fits_yyssp;
  if (0 <= fits_yystate && fits_yystate <= YYLAST && fits_yycheck[fits_yystate] == *fits_yyssp)
    fits_yystate = fits_yytable[fits_yystate];
  else
    fits_yystate = fits_yydefgoto[fits_yyn - YYNTOKENS];

  goto fits_yynewstate;


/*------------------------------------.
| fits_yyerrlab -- here on detecting error |
`------------------------------------*/
fits_yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!fits_yyerrstatus)
    {
      ++fits_yynerrs;
#if ! YYERROR_VERBOSE
      fits_yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T fits_yysize = fits_yysyntax_error (0, fits_yystate, fits_yychar);
	if (fits_yymsg_alloc < fits_yysize && fits_yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T fits_yyalloc = 2 * fits_yysize;
	    if (! (fits_yysize <= fits_yyalloc && fits_yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      fits_yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (fits_yymsg != fits_yymsgbuf)
	      YYSTACK_FREE (fits_yymsg);
	    fits_yymsg = (char *) YYSTACK_ALLOC (fits_yyalloc);
	    if (fits_yymsg)
	      fits_yymsg_alloc = fits_yyalloc;
	    else
	      {
		fits_yymsg = fits_yymsgbuf;
		fits_yymsg_alloc = sizeof fits_yymsgbuf;
	      }
	  }

	if (0 < fits_yysize && fits_yysize <= fits_yymsg_alloc)
	  {
	    (void) fits_yysyntax_error (fits_yymsg, fits_yystate, fits_yychar);
	    fits_yyerror (fits_yymsg);
	  }
	else
	  {
	    fits_yyerror (YY_("syntax error"));
	    if (fits_yysize != 0)
	      goto fits_yyexhaustedlab;
	  }
      }
#endif
    }



  if (fits_yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (fits_yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (fits_yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  fits_yydestruct ("Error: discarding",
		      fits_yytoken, &fits_yylval);
	  fits_yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto fits_yyerrlab1;


/*---------------------------------------------------.
| fits_yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
fits_yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label fits_yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto fits_yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (fits_yylen);
  fits_yylen = 0;
  YY_STACK_PRINT (fits_yyss, fits_yyssp);
  fits_yystate = *fits_yyssp;
  goto fits_yyerrlab1;


/*-------------------------------------------------------------.
| fits_yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
fits_yyerrlab1:
  fits_yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      fits_yyn = fits_yypact[fits_yystate];
      if (fits_yyn != YYPACT_NINF)
	{
	  fits_yyn += YYTERROR;
	  if (0 <= fits_yyn && fits_yyn <= YYLAST && fits_yycheck[fits_yyn] == YYTERROR)
	    {
	      fits_yyn = fits_yytable[fits_yyn];
	      if (0 < fits_yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (fits_yyssp == fits_yyss)
	YYABORT;


      fits_yydestruct ("Error: popping",
		  fits_yystos[fits_yystate], fits_yyvsp);
      YYPOPSTACK (1);
      fits_yystate = *fits_yyssp;
      YY_STACK_PRINT (fits_yyss, fits_yyssp);
    }

  if (fits_yyn == YYFINAL)
    YYACCEPT;

  *++fits_yyvsp = fits_yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", fits_yystos[fits_yyn], fits_yyvsp, fits_yylsp);

  fits_yystate = fits_yyn;
  goto fits_yynewstate;


/*-------------------------------------.
| fits_yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
fits_yyacceptlab:
  fits_yyresult = 0;
  goto fits_yyreturn;

/*-----------------------------------.
| fits_yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
fits_yyabortlab:
  fits_yyresult = 1;
  goto fits_yyreturn;

#ifndef fits_yyoverflow
/*-------------------------------------------------.
| fits_yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
fits_yyexhaustedlab:
  fits_yyerror (YY_("memory exhausted"));
  fits_yyresult = 2;
  /* Fall through.  */
#endif

fits_yyreturn:
  if (fits_yychar != YYEOF && fits_yychar != YYEMPTY)
     fits_yydestruct ("Cleanup: discarding lookahead",
		 fits_yytoken, &fits_yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (fits_yylen);
  YY_STACK_PRINT (fits_yyss, fits_yyssp);
  while (fits_yyssp != fits_yyss)
    {
      fits_yydestruct ("Cleanup: popping",
		  fits_yystos[*fits_yyssp], fits_yyvsp);
      YYPOPSTACK (1);
    }
#ifndef fits_yyoverflow
  if (fits_yyss != fits_yyssa)
    YYSTACK_FREE (fits_yyss);
#endif
#if YYERROR_VERBOSE
  if (fits_yymsg != fits_yymsgbuf)
    YYSTACK_FREE (fits_yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (fits_yyresult);
}


#line 187 "unity-c,fits.y"



