YACC=@YACC@
LEX=@LEX@
LEX_OUTPUT_ROOT=@LEX_OUTPUT_ROOT@
CC=@CC@
CFLAGS=@CFLAGS@ -DYYDEBUG=1
CPPFLAGS=@CPPFLAGS@
AR=@AR@
RANLIB=@RANLIB@
DOXYGEN=@DOXYGEN@
DOT=@DOT@

INSTALL=@INSTALL@
#MKDIR_P=mkdir -p
MKDIR_P=@MKDIR_P@

prefix=@prefix@
exec_prefix=@exec_prefix@
datarootdir=@datarootdir@
bindir=@bindir@
libdir=@libdir@
docdir=@docdir@
includedir=@includedir@

# We should use libtool -- this library file name won't work everywhere
UNITYLIB=libunity.a

#PACKAGE_VERSION=@PACKAGE_VERSION@
LIBRARY_HOMEPAGE=@LIBRARY_HOMEPAGE@

PARSERLIST=@PARSERLIST@
PARSERS=$(patsubst %,unity-%,$(PARSERLIST))
# The OBJS are the object files which go in the library
# (ie, they don't include unity.o)
# Each of these OBJS has a corresponding .h file
OBJS=function-definitions unit-definitions unitylib writers
# These extra headers don't correspond to .c/.o files
EXTRA_HEADERS=known-syntaxes.h unity-constants.h unity.tab.h version.h


all: unity

unity: unity.o $(UNITYLIB)
	$(CC) -o $@ $(CFLAGS) unity.o -L. -lunity -lm

unity.o: unity.h version.h
unit-definitions.o: unit-definitions.h known-syntaxes.h
unitylib.o: known-syntaxes.h version.h
writers.o: known-syntaxes.h unit-definitions.h
function-definitions.o: function-definitions.h known-syntaxes.h


# Yes, we should use libtool...
$(UNITYLIB): $(OBJS:=.o) $(PARSERS:=.tab.o) $(EXTRA_HEADERS) unity-lexer.o
	rm -f $@
	$(AR) -r $@ $(OBJS:=.o) $(PARSERS:=.tab.o) unity-lexer.o
	$(RANLIB) $@


# The flex output includes a yyinput(), which is redundant in this
# case (and so causes a compilation error with -Wall -Werror), but
# which is omitted if (undocumented) YY_NO_INPUT is defined.
unity-lexer.o: unity-lexer.c unity.h
	$(CC) $(CFLAGS) $(CPPFLAGS) -DYY_NO_INPUT=1 -c -o unity-lexer.o unity-lexer.c

# Compile generated code without warnings-as-errors
# (the -Wno-parentheses warning-suppression is probably specific to gcc/clang,
# and can be removed, at the cost of some warning-noise, if it's incompatible
# with a particular compiler).
%.tab.o: %.tab.c unity.h
	$(CC) $(CFLAGS) -Wno-parentheses -c -o $@ $<


install: unity docs
	for d in $(bindir) $(includedir) $(libdir) $(docdir); do test -d $$d || $(MKDIR_P) $$d; done
	$(INSTALL) unity $(bindir)
	$(INSTALL) unitylib.h $(includedir)
	$(INSTALL) $(UNITYLIB) $(libdir)
	if test -d docs/html; then $(MKDIR_P) $(docdir)/unity && cp -R docs/* $(docdir)/unity; else :; fi

Makefile: Makefile.in ../../config.status
	cd ../..; ./config.status src/c/Makefile

check: $(UNITYLIB)
	cd test; $(MAKE) check

clean:
	rm -f *.o $(UNITYLIB) unity
	rm -Rf dist
	cd test; $(MAKE) clean
