/*
 * This is a GENERATED FILE -- do not edit.
 * See known-units.csv and buildtools/src/ParseUnits.java.
 */


/**
 * @file unity.h
 */

#include <stdio.h>              /* for NULL */
#include <stdlib.h>             /* for bsearch */
#include <string.h>             /* for strcmp */
#include <assert.h>

#define UNITY_INTERNAL 1
#include "unit-definitions.h"
#include "known-syntaxes.h"

static const char* known_syntaxes[] = { "cds", "ogip", "vounits", "fits", };
/* List of all the known units */
static UnitDef unit_defs[] = {
  { "http://qudt.org/vocab/unit#Byte", 0,
    "Byte",
    "Byte",
    "http://qudt.org/vocab/quantity#InformationEntropy",
    "1",
    NULL,
    "\\byte" },
  { "http://qudt.org/vocab/unit#Newton", 1,
    "Newton",
    "Newton",
    "http://qudt.org/vocab/quantity#Force",
    "L M T^-2",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Tesla", 2,
    "Tesla",
    "Tesla",
    "http://qudt.org/vocab/quantity#MagneticField",
    "M T^-2 I^-1",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Steradian", 3,
    "Steradian",
    "Steradian",
    "http://qudt.org/vocab/quantity#SolidAngle",
    "1",
    "The steradian (symbol: sr) is the SI unit of solid angle. It is used to describe two-dimensional angular spans in three-dimensional space, analogous to the way in which the radian describes angles in a plane.",
    NULL },
  { "http://qudt.org/vocab/unit#Kelvin", 4,
    "Kelvin",
    "Kelvin",
    "http://qudt.org/vocab/quantity#ThermodynamicTemperature",
    "Θ",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Lumen", 5,
    "Lumen",
    "Lumen",
    "http://qudt.org/vocab/quantity#LuminousFlux",
    "J",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#SolarRadius", 6,
    "solRad",
    "SolarRadius",
    "http://qudt.org/vocab/quantity#Length",
    "L",
    "6.9599e8 m",
    "R\\solar" },
  { "http://qudt.org/vocab/unit#MinuteTime", 7,
    "Minute Time",
    "MinuteTime",
    "http://qudt.org/vocab/quantity#Time",
    "T",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#DegreeAngle", 8,
    "Degree Angle",
    "DegreeAngle",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    "1",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#Crab", 9,
    "Crab",
    "Crab",
    "http://bitbucket/org/nxg/unity/ns/quantity#SpecificIntensity",
    "M T^-2",
    "The flux density, relative to that of the Crab.\nMentioned in the OGIP standard, but strongly disrecommended.\nThe OGIP standard permits only 'm' as a prefix.",
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel", 10,
    "chan",
    "DetectorChannel",
    "http://qudt.org/vocab/quantity#Dimensionless",
    "1",
    "One channel in a detector of some type",
    NULL },
  { "http://qudt.org/vocab/unit#Volt", 11,
    "Volt",
    "Volt",
    "http://qudt.org/vocab/quantity#EnergyPerElectricCharge",
    "L^2 M T^-3 I^-1",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#ADU", 12,
    "ADU",
    "ADU",
    "http://qudt.org/vocab/quantity#Dimensionless",
    "1",
    "Same as channel???",
    NULL },
  { "http://qudt.org/vocab/unit#Erg", 13,
    "Erg",
    "Erg",
    "http://qudt.org/vocab/quantity#EnergyAndWork",
    "L^2 M T^-2",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Pascal", 14,
    "Pascal",
    "Pascal",
    "http://qudt.org/vocab/quantity#ForcePerArea",
    "L^-1 M T^-2",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#AstronomicalUnit", 15,
    "Astronomical Unit",
    "AstronomicalUnit",
    "http://qudt.org/vocab/quantity#Length",
    "L",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude", 16,
    "mag",
    "StellarMagnitude",
    "http://bitbucket/org/nxg/unity/ns/quantity#StellarMagnitudeQuantityKind",
    "1",
    "Log of a ratio of intensities.  See http://en.wikipedia.org/wiki/Magnitude_(astronomy)",
    NULL },
  { "http://qudt.org/vocab/unit#Siemens", 17,
    "Siemens",
    "Siemens",
    "http://qudt.org/vocab/quantity#ElectricConductivity",
    "L^-2 M^-1 T^3 I^2",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#Jansky", 18,
    "Jy",
    "Jansky",
    "http://bitbucket/org/nxg/unity/ns/quantity#SpecificIntensity",
    "M T^-2",
    "10^-26 W m-2 Hz-1",
    NULL },
  { "http://qudt.org/vocab/unit#LightYear", 19,
    "Light Year",
    "LightYear",
    "http://qudt.org/vocab/quantity#Length",
    "L",
    "A unit of length defining the distance, in meters, that light travels in a vacuum in one year.",
    NULL },
  { "http://qudt.org/vocab/unit#SecondTime", 20,
    "Second",
    "SecondTime",
    "http://qudt.org/vocab/quantity#Time",
    "T",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Gauss", 21,
    "Gauss",
    "Gauss",
    "http://qudt.org/vocab/quantity#MagneticField",
    "M T^-2 I^-1",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond", 22,
    "mas",
    "MilliArcSecond",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    "1",
    "1/1000 of an arc-second, for use in those syntaxes which do not allow SI prefixes of arcsec",
    NULL },
  { "http://qudt.org/vocab/unit#Meter", 23,
    "Metre",
    "Meter",
    "http://qudt.org/vocab/quantity#Length",
    "L",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Coulomb", 24,
    "Coulomb",
    "Coulomb",
    "http://qudt.org/vocab/quantity#ElectricCharge",
    "T I",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#Rydberg", 25,
    "Ry",
    "Rydberg",
    "http://qudt.org/vocab/quantity#EnergyAndWork",
    "L^2 M T^-2",
    "Half of the Hartree energy, defined as 13.605692 eV.  See http://en.wikipedia.org/wiki/Hartree",
    NULL },
  { "http://qudt.org/vocab/unit#Decibel", 26,
    "Decibel",
    "Decibel",
    "http://qudt.org/vocab/quantity#SignalStrength",
    "L M T^-3 I^-1",
    NULL,
    "\\decibel" },
  { "http://qudt.org/vocab/unit#Hertz", 27,
    "Hertz",
    "Hertz",
    "http://qudt.org/vocab/quantity#Frequency",
    "T^-1",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Mole", 28,
    "Mole",
    "Mole",
    "http://qudt.org/vocab/quantity#AmountOfSubstance",
    "N",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Farad", 29,
    "Farad",
    "Farad",
    "http://qudt.org/vocab/quantity#Capacitance",
    "L^-2 M^-1 T^4 I^2",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#BesselianYear", 30,
    "Bessellian Year",
    "BesselianYear",
    "http://qudt.org/vocab/quantity#Time",
    "T",
    "The Besselian Year.\nSee the discussion in FITS WCS paper IV (section 4.2), and FITS v4.0 (section 9.3)\nfor important caveats concerning the use of the Besselian and Tropical years.",
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#Voxel", 31,
    "voxel",
    "Voxel",
    "http://qudt.org/vocab/quantity#Dimensionless",
    "1",
    "The 3-D analogue of a pixel",
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#Pixel", 32,
    "pixel",
    "Pixel",
    "http://qudt.org/vocab/quantity#Dimensionless",
    "1",
    "an element of a 2-D image",
    NULL },
  { "http://qudt.org/vocab/unit#Joule", 33,
    "Joule",
    "Joule",
    "http://qudt.org/vocab/quantity#EnergyAndWork",
    "L^2 M T^-2",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Number", 34,
    "Number",
    "Number",
    "http://qudt.org/vocab/quantity#Dimensionless",
    "1",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Watt", 35,
    "Watt",
    "Watt",
    "http://qudt.org/vocab/quantity#Power",
    "L^2 M T^-3",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Lux", 36,
    "Lux",
    "Lux",
    "http://qudt.org/vocab/quantity#LuminousFluxPerArea",
    "L^-2 J",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#ArcMinute", 37,
    "Arc Minute",
    "ArcMinute",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    "1",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Gram", 38,
    "Gramme",
    "Gram",
    "http://qudt.org/vocab/quantity#Mass",
    "M",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Bit", 39,
    "Bit",
    "Bit",
    "http://qudt.org/vocab/quantity#InformationEntropy",
    "1",
    "In information theory, a bit is the amount of information that, on average, can be stored in a discrete bit. It is thus the amount of information carried by a choice between two equally likely outcomes. One bit corresponds to about 0.693 nats (ln(2)), or 0.301 hartleys (log10(2)).",
    "\\bit" },
  { "http://qudt.org/vocab/unit#Weber", 40,
    "Weber",
    "Weber",
    "http://qudt.org/vocab/quantity#MagneticFlux",
    "L^2 M T^-2 I^-1",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#JulianCentury", 41,
    "Julian Century",
    "JulianCentury",
    "http://qudt.org/vocab/quantity#Time",
    "T",
    "One hundred Julian Years",
    NULL },
  { "http://qudt.org/vocab/unit#Barn", 42,
    "Barn",
    "Barn",
    "http://qudt.org/vocab/quantity#Area",
    "L^2",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#Photon", 43,
    "ph",
    "Photon",
    "http://qudt.org/vocab/quantity#Dimensionless",
    "1",
    "as in ev/photon",
    NULL },
  { "http://qudt.org/vocab/unit#Candela", 44,
    "Candela",
    "Candela",
    "http://qudt.org/vocab/quantity#LuminousIntensity",
    "J",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Percent", 45,
    "Percent",
    "Percent",
    "http://qudt.org/vocab/quantity#DimensionlessRatio",
    "1",
    NULL,
    "\\%" },
  { "http://bitbucket.org/nxg/unity/ns/unit#DistributionBin", 46,
    "bin",
    "DistributionBin",
    "http://qudt.org/vocab/quantity#Dimensionless",
    "1",
    "One division within a distribution",
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#SolarMass", 47,
    "Solar mass",
    "SolarMass",
    "http://qudt.org/vocab/quantity#Mass",
    "M",
    "The mass of the sun is 1.9891e30 kg",
    "M\\solar" },
  { "http://qudt.org/vocab/unit#Angstrom", 48,
    "Angstrom",
    "Angstrom",
    "http://qudt.org/vocab/quantity#Length",
    "L",
    "The ångström or angstrom (symbol Å) (the latter spelling, without diacritics, is now usually used in English) (pronounced /ˈæŋstrəm/; Swedish: [ˈɔŋstrøm]) is an internationally recognized unit of length equal to 0.1 nanometre or 1×10−10 metres. It is named after Anders Jonas Ångström. Although accepted for use, it is not formally defined within the International System of Units(SI). (That article lists the units that are so defined.) The ångström is often used in the natural sciences to express the sizes of atoms, lengths of chemical bonds and the wavelengths of electromagnetic radiation, and in technology for the dimensions of parts of integrated circuits. It is also commonly used in structural biology. [Wikipedia]",
    "\\angstrom" },
  { "http://qudt.org/vocab/unit#Day", 49,
    "Day",
    "Day",
    "http://qudt.org/vocab/quantity#Time",
    "T",
    "Mean solar day",
    NULL },
  { "http://qudt.org/vocab/unit#Radian", 50,
    "Radian",
    "Radian",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    "1",
    "The radian is the standard unit of angular measure, used in many areas of mathematics. It describes the plane angle subtended by a circular arc as the length of the arc divided by the radius of the arc. The unit was formerly a SI supplementary unit, but this category was abolished in 1995 and the radian is now considered a SI derived unit. The SI unit of solid angle measurement is the steradian.\nThe radian is represented by the symbol \"rad\" or, more rarely, by the superscript c (for \"circular measure\"). For example, an angle of 1.2 radians would be written as \"1.2 rad\" or \"1.2c\" (the second symbol is often mistaken for a degree: \"1.2°\"). As the ratio of two lengths, the radian is a \"pure number\" that needs no unit symbol, and in mathematical writing the symbol \"rad\" is almost always omitted. In the absence of any symbol radians are assumed, and when degrees are meant the symbol ° is used. [Wikipedia]",
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#Beam", 51,
    "beam",
    "Beam",
    "http://qudt.org/vocab/quantity#Dimensionless",
    "1",
    "as in Jy/beam",
    NULL },
  { "http://qudt.org/vocab/unit#Ohm", 52,
    "Ohm",
    "Ohm",
    "http://qudt.org/vocab/quantity#Resistance",
    "L^2 M T^-3 I^-2",
    NULL,
    "\\ohm" },
  { "http://qudt.org/vocab/unit#YearTropical", 53,
    "Year Tropical",
    "YearTropical",
    "http://qudt.org/vocab/quantity#Time",
    "T",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#JulianYear", 54,
    "Julian year",
    "JulianYear",
    "http://qudt.org/vocab/quantity#Time",
    "T",
    "31 557 600s (365.25d), peta a (Pa) forbidden",
    NULL },
  { "http://qudt.org/vocab/unit#Ampere", 55,
    "Ampere",
    "Ampere",
    "http://qudt.org/vocab/quantity#ElectricCurrent",
    "I",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#ElectronVolt", 56,
    "Electron Volt",
    "ElectronVolt",
    "http://qudt.org/vocab/quantity#EnergyAndWork",
    "L^2 M T^-2",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Debye", 57,
    "Debye",
    "Debye",
    "http://qudt.org/vocab/quantity#ElectricDipoleMoment",
    "L T I",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#ArcSecond", 58,
    "Arc Second",
    "ArcSecond",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    "1",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Parsec", 59,
    "Parsec",
    "Parsec",
    "http://qudt.org/vocab/quantity#Length",
    "L",
    "The parsec (parallax of one arcsecond; symbol: pc) is a unit of length, equal to just under 31 trillion (31×1012) kilometres (about 19 trillion miles), 206265 AU, or about 3.26 light-years. The parsec measurement unit is used in astronomy. It is defined as the length of the adjacent side of an imaginary right triangle in space. The two dimensions that specify this triangle are the parallax angle (defined as 1 arcsecond) and the opposite side (defined as 1 astronomical unit (AU), the distance from the Earth to the Sun). Given these two measurements, along with the rules of trigonometry, the length of the adjacent side (the parsec) can be found. [Wikipedia]",
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#Rayleigh", 60,
    "R",
    "Rayleigh",
    "http://bitbucket.org/nxg/unity/ns/unit#PhotonFlux",
    "L^-2 T^-2",
    "Unit of photon flux density: Wikipedia has units of 10^10 photons/m2/column/s.\nThe FITS spec has units of 1e10/(4PI) photons m-2 s-2 sr-1.\nSee http://en.wikipedia.org/wiki/Rayleigh_(unit).\nNo, I don't know how to express its dimensions in the QUDT system!\nWe go with the FITS definition here.",
    NULL },
  { "http://qudt.org/vocab/unit#Henry", 61,
    "Henry",
    "Henry",
    "http://qudt.org/vocab/quantity#Inductance",
    "L^2 M T^-2 I^-2",
    NULL,
    NULL },
  { "http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity", 62,
    "solLum",
    "SolarLuminosity",
    "http://qudt.org/vocab/quantity#Power",
    "L^2 M T^-3",
    "3.8268e26 W",
    "L\\solar" },
  { "http://qudt.org/vocab/unit#UnifiedAtomicMassUnit", 63,
    "Unified Atomic Mass Unit",
    "UnifiedAtomicMassUnit",
    "http://qudt.org/vocab/quantity#Mass",
    "M",
    NULL,
    NULL },
  { "http://qudt.org/vocab/unit#Hour", 64,
    "Hour",
    "Hour",
    "http://qudt.org/vocab/quantity#Time",
    "T",
    NULL,
    NULL },
};
static int unit_defs_length = 65;


// Reverse lookups, per-syntax maps going from symbols to UnitDefinitions

/* A lookup table, mapping symbols to an index in the unit_representation array above.
 * There are three arrays in here, separatedly ordered by symbol,
 * with the start offsets indicated by the unit_rep_per_syntax array below.
 */
static struct unit_representation unit_reps[] = {

  // unit representations for syntax cds
  { "%", 45, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Percent
  { "A", 55, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Ampere
  { "AU", 15, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#AstronomicalUnit
  { "Angstrom", 48, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Angstrom
  { "C", 24, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Coulomb
  { "D", 57, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Debye
  { "F", 29, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Farad
  { "H", 61, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Henry
  { "Hz", 27, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Hertz
  { "J", 33, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Joule
  { "Jy", 18, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Jansky
  { "K", 4, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Kelvin
  { "N", 1, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Newton
  { "Ohm", 52, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Ohm
  { "Pa", 14, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Pascal
  { "Ry", 25, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Rydberg
  { "S", 17, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Siemens
  { "T", 2, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Tesla
  { "V", 11, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Volt
  { "W", 35, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Watt
  { "Wb", 40, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Weber
  { "a", 54, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
  { "arcmin", 37, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#ArcMinute
  { "arcsec", 58, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#ArcSecond
  { "barn", 42, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Barn
  { "bit", 39, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Bit
  { "byte", 0, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Byte
  { "cd", 44, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Candela
  { "ct", 34, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Number
  { "d", 49, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Day
  { "deg", 8, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#DegreeAngle
  { "eV", 56, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#ElectronVolt
  { "g", 38, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Gram
  { "h", 64, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Hour
  { "lm", 5, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Lumen
  { "lx", 36, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Lux
  { "m", 23, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Meter
  { "mag", 16, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude
  { "mas", 22, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond
  { "min", 7, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#MinuteTime
  { "mol", 28, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Mole
  { "pc", 59, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Parsec
  { "pix", 32, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Pixel
  { "rad", 50, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Radian
  { "s", 20, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#SecondTime
  { "solLum", 62, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity
  { "solMass", 47, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarMass
  { "solRad", 6, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarRadius
  { "sr", 3, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Steradian
  { "yr", 54, 1, 0, 0, 1 }, // http://bitbucket.org/nxg/unity/ns/unit#JulianYear

  // unit representations for syntax ogip
  { "A", 55, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Ampere
  { "AU", 15, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#AstronomicalUnit
  { "C", 24, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Coulomb
  { "Crab", 9, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Crab
  { "F", 29, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Farad
  { "G", 21, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Gauss
  { "H", 61, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Henry
  { "Hz", 27, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Hertz
  { "J", 33, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Joule
  { "Jy", 18, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Jansky
  { "K", 4, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Kelvin
  { "N", 1, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Newton
  { "Pa", 14, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Pascal
  { "S", 17, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Siemens
  { "T", 2, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Tesla
  { "V", 11, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Volt
  { "W", 35, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Watt
  { "Wb", 40, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Weber
  { "angstrom", 48, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Angstrom
  { "arcmin", 37, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#ArcMinute
  { "arcsec", 58, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#ArcSecond
  { "barn", 42, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Barn
  { "bin", 46, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#DistributionBin
  { "byte", 0, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Byte
  { "cd", 44, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Candela
  { "chan", 10, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel
  { "count", 34, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Number
  { "d", 49, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Day
  { "deg", 8, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#DegreeAngle
  { "eV", 56, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#ElectronVolt
  { "erg", 13, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Erg
  { "g", 38, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Gram
  { "h", 64, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Hour
  { "lm", 5, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Lumen
  { "lx", 36, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Lux
  { "lyr", 19, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#LightYear
  { "m", 23, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Meter
  { "mag", 16, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude
  { "min", 7, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#MinuteTime
  { "mol", 28, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Mole
  { "ohm", 52, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Ohm
  { "pc", 59, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Parsec
  { "photon", 43, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Photon
  { "pixel", 32, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Pixel
  { "rad", 50, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Radian
  { "s", 20, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#SecondTime
  { "sr", 3, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Steradian
  { "voxel", 31, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Voxel
  { "yr", 54, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#JulianYear

  // unit representations for syntax vounits
  { "%", 45, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Percent
  { "A", 55, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Ampere
  { "AU", 15, 0, 0, 0, 1 }, // http://qudt.org/vocab/unit#AstronomicalUnit
  { "Angstrom", 48, 0, 0, 1, 1 }, // http://qudt.org/vocab/unit#Angstrom
  { "B", 0, 1, 1, 0, 0 }, // http://qudt.org/vocab/unit#Byte
  { "Ba", 30, 0, 0, 1, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#BesselianYear
  { "C", 24, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Coulomb
  { "D", 57, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Debye
  { "F", 29, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Farad
  { "G", 21, 1, 0, 1, 0 }, // http://qudt.org/vocab/unit#Gauss
  { "H", 61, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Henry
  { "Hz", 27, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Hertz
  { "J", 33, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Joule
  { "Jy", 18, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Jansky
  { "K", 4, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Kelvin
  { "N", 1, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Newton
  { "Ohm", 52, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Ohm
  { "Pa", 14, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Pascal
  { "R", 60, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Rayleigh
  { "Ry", 25, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Rydberg
  { "S", 17, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Siemens
  { "T", 2, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Tesla
  { "V", 11, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Volt
  { "W", 35, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Watt
  { "Wb", 40, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Weber
  { "a", 54, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
  { "adu", 12, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#ADU
  { "angstrom", 48, 0, 0, 1, 0 }, // http://qudt.org/vocab/unit#Angstrom
  { "arcmin", 37, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#ArcMinute
  { "arcsec", 58, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#ArcSecond
  { "au", 15, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#AstronomicalUnit
  { "barn", 42, 1, 0, 1, 0 }, // http://qudt.org/vocab/unit#Barn
  { "beam", 51, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Beam
  { "bin", 46, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#DistributionBin
  { "bit", 39, 1, 1, 0, 0 }, // http://qudt.org/vocab/unit#Bit
  { "byte", 0, 1, 1, 0, 1 }, // http://qudt.org/vocab/unit#Byte
  { "cd", 44, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Candela
  { "chan", 10, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel
  { "count", 34, 1, 0, 0, 1 }, // http://qudt.org/vocab/unit#Number
  { "ct", 34, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Number
  { "d", 49, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Day
  { "dB", 26, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Decibel
  { "deg", 8, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#DegreeAngle
  { "eV", 56, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#ElectronVolt
  { "erg", 13, 1, 0, 1, 0 }, // http://qudt.org/vocab/unit#Erg
  { "g", 38, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Gram
  { "h", 64, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Hour
  { "lm", 5, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Lumen
  { "lx", 36, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Lux
  { "lyr", 19, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#LightYear
  { "m", 23, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Meter
  { "mag", 16, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude
  { "mas", 22, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond
  { "min", 7, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#MinuteTime
  { "mol", 28, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Mole
  { "pc", 59, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Parsec
  { "ph", 43, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Photon
  { "photon", 43, 1, 0, 0, 1 }, // http://bitbucket.org/nxg/unity/ns/unit#Photon
  { "pix", 32, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Pixel
  { "pixel", 32, 1, 0, 0, 1 }, // http://bitbucket.org/nxg/unity/ns/unit#Pixel
  { "rad", 50, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Radian
  { "s", 20, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#SecondTime
  { "solLum", 62, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity
  { "solMass", 47, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarMass
  { "solRad", 6, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarRadius
  { "sr", 3, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Steradian
  { "ta", 53, 0, 0, 1, 0 }, // http://qudt.org/vocab/unit#YearTropical
  { "u", 63, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#UnifiedAtomicMassUnit
  { "voxel", 31, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Voxel
  { "yr", 54, 1, 0, 0, 1 }, // http://bitbucket.org/nxg/unity/ns/unit#JulianYear

  // unit representations for syntax fits
  { "A", 55, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Ampere
  { "AU", 15, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#AstronomicalUnit
  { "Angstrom", 48, 0, 0, 1, 0 }, // http://qudt.org/vocab/unit#Angstrom
  { "Ba", 30, 0, 0, 1, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#BesselianYear
  { "C", 24, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Coulomb
  { "D", 57, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Debye
  { "F", 29, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Farad
  { "G", 21, 1, 0, 1, 0 }, // http://qudt.org/vocab/unit#Gauss
  { "H", 61, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Henry
  { "Hz", 27, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Hertz
  { "J", 33, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Joule
  { "Jy", 18, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Jansky
  { "K", 4, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Kelvin
  { "N", 1, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Newton
  { "Ohm", 52, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Ohm
  { "Pa", 14, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Pascal
  { "R", 60, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Rayleigh
  { "Ry", 25, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Rydberg
  { "S", 17, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Siemens
  { "T", 2, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Tesla
  { "V", 11, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Volt
  { "W", 35, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Watt
  { "Wb", 40, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Weber
  { "a", 54, 1, 0, 0, 1 }, // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
  { "adu", 12, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#ADU
  { "arcmin", 37, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#ArcMinute
  { "arcsec", 58, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#ArcSecond
  { "barn", 42, 1, 0, 1, 0 }, // http://qudt.org/vocab/unit#Barn
  { "beam", 51, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Beam
  { "bin", 46, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#DistributionBin
  { "bit", 39, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Bit
  { "byte", 0, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Byte
  { "cd", 44, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Candela
  { "chan", 10, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel
  { "count", 34, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Number
  { "ct", 34, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Number
  { "cy", 41, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#JulianCentury
  { "d", 49, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Day
  { "deg", 8, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#DegreeAngle
  { "eV", 56, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#ElectronVolt
  { "erg", 13, 0, 0, 1, 0 }, // http://qudt.org/vocab/unit#Erg
  { "g", 38, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Gram
  { "h", 64, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#Hour
  { "lm", 5, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Lumen
  { "lx", 36, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Lux
  { "lyr", 19, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#LightYear
  { "m", 23, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Meter
  { "mag", 16, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude
  { "mas", 22, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond
  { "min", 7, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#MinuteTime
  { "mol", 28, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Mole
  { "pc", 59, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Parsec
  { "ph", 43, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Photon
  { "photon", 43, 0, 0, 0, 1 }, // http://bitbucket.org/nxg/unity/ns/unit#Photon
  { "pix", 32, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Pixel
  { "pixel", 32, 0, 0, 0, 1 }, // http://bitbucket.org/nxg/unity/ns/unit#Pixel
  { "rad", 50, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Radian
  { "s", 20, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#SecondTime
  { "solLum", 62, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity
  { "solMass", 47, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarMass
  { "solRad", 6, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#SolarRadius
  { "sr", 3, 1, 0, 0, 0 }, // http://qudt.org/vocab/unit#Steradian
  { "ta", 53, 0, 0, 1, 0 }, // http://qudt.org/vocab/unit#YearTropical
  { "u", 63, 0, 0, 0, 0 }, // http://qudt.org/vocab/unit#UnifiedAtomicMassUnit
  { "voxel", 31, 0, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#Voxel
  { "yr", 54, 1, 0, 0, 0 }, // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
};
struct { int map_start; int map_length; } unit_rep_per_syntax[] = {
  { 0, 50 }, /* cds */
  { 50, 49 }, /* ogip */
  { 99, 70 }, /* vounits */
  { 169, 66 }, /* fits */
};
/* Lookup between integers and UnitySyntax */
static UnitySyntax stxIndexes[] = {
  UNITY_SYNTAX_CDS,
  UNITY_SYNTAX_OGIP,
  UNITY_SYNTAX_VOUNITS,
  UNITY_SYNTAX_FITS,
};
static const int n_syntaxes = 4;


/*
 * A mapping, for each syntax, from unit number (index in the unit_defs array above)
 * to representation (index in the unit_reps array above)
 */
static int unit_to_representation[4][65] = {
  { 26, 12, 17, 48, 11, 34, 47, 39, 30, -1, -1, 18, -1, -1, 14, 2, 37, 16, 10, -1, 44, -1, 38, 36, 4, 15, -1, 8, 40, 6, -1, -1, 42, 9, 28, 19, 35, 22, 32, 25, 20, -1, 24, -1, 27, 0, -1, 46, 3, 29, 43, -1, 13, -1, 49, 1, 31, 5, 23, 41, -1, 7, 45, -1, 33, },
  { 73, 61, 64, 96, 60, 83, -1, 88, 78, 53, 75, 65, -1, 80, 62, 51, 87, 63, 59, 85, 95, 55, -1, 86, 52, -1, -1, 57, 89, 54, -1, 97, 93, 58, 76, 66, 84, 69, 81, -1, 67, -1, 71, 92, 74, -1, 72, -1, 68, 77, 94, -1, 90, -1, 98, 50, 79, -1, 70, 91, -1, 56, -1, -1, 82, },
  { 134, 114, 120, 164, 113, 146, 163, 152, 141, -1, 136, 121, 125, 143, 116, 101, 150, 119, 112, 148, 160, 108, 151, 149, 105, 118, 140, 110, 153, 107, 104, 167, 158, 111, 137, 122, 147, 127, 144, 133, 123, -1, 130, 156, 135, 99, 132, 162, 102, 139, 159, 131, 115, 165, 168, 100, 142, 106, 128, 154, 117, 109, 161, 166, 145, },
  { 200, 182, 188, 230, 181, 212, 229, 218, 207, -1, 202, 189, 193, 209, 184, 170, 216, 187, 180, 214, 226, 176, 217, 215, 173, 186, -1, 178, 219, 175, 172, 233, 224, 179, 204, 190, 213, 194, 210, 199, 191, 205, 196, 222, 201, -1, 198, 228, 171, 206, 225, 197, 183, 231, 192, 169, 208, 174, 195, 220, 185, 177, 227, 232, 211, },
};

static int unit_rep_compare(const void* key, const void* test)
{
    return strcmp((char*)key, ((UnitRep*)test)->symbol);
}

static int uu_syntax_to_integer(UnitySyntax syntax)
{
    int syntaxIdx;
    for (syntaxIdx = 0; syntaxIdx < n_syntaxes; syntaxIdx++) {
        if (stxIndexes[syntaxIdx] == syntax)
            break;
    }
    // if we haven't found a syntaxIdx, that's a programming error -- things are out of sync
    assert(syntaxIdx < n_syntaxes);
    return syntaxIdx;
}

/**
 * Retrieves a unit definition.  Returns NULL if the abbreviation is
 * not recognised in the indicated syntax (or if the syntax is invalid)
 * @param abbrev a unit abbreviation, for example "m" for "metre"
 * @param syntax one of the UNITY_SYNTAX_* constants
 * @return a pointer to a the unit definition which this abbreviation corresponds to
 */
const UnitDef* unity_get_unitdef_from_string(const char* abbrev, UnitySyntax syntax)
{
    UnitRep *map;
    UnitRep *one_unit_rep;
    int maplen;
    int rep_index;
    int syntaxIdx = uu_syntax_to_integer(syntax);

    map = &unit_reps[unit_rep_per_syntax[syntaxIdx].map_start];
    maplen = unit_rep_per_syntax[syntaxIdx].map_length;

    one_unit_rep = (UnitRep*)bsearch((void*)abbrev,
                                     (void*)map, maplen, sizeof(UnitRep),
                                     &unit_rep_compare);
    if (one_unit_rep != NULL) {
        rep_index = one_unit_rep->_unit_index;
        return &unit_defs[rep_index];
    } else {
        return NULL;
    }
}

// Given a unit definition, find its representation in the given syntax
UnitRep* u_get_unit_representation(const UnitDef* ud, const UnitySyntax syntax)
{
    int unit_rep_index;
    int syntaxIdx = uu_syntax_to_integer(syntax);

    if (ud == NULL) {
        // this unit doesn't have a base_unit_def -- it wasn't recognised at parse time
        return NULL;            /* JUMP OUT */
    }
    assert(ud->_idx >= 0 && ud->_idx <unit_defs_length);

    unit_rep_index = unit_to_representation[syntaxIdx][ud->_idx];
    if (unit_rep_index < 0) {
        return NULL;
    } else {
        return &unit_reps[unit_rep_index];
    }
}

// Given a unit definition, find its representation in _any_ syntax.
// Since there are no units which are not represented in at least one
// syntax, this will always return non-NULL.
UnitRep* u_get_unit_representation_any(const UnitDef* ud)
{
    int i;
    UnitRep* rval = NULL;

    for (i=0; i<n_syntaxes; i++) {
        rval = u_get_unit_representation(ud, stxIndexes[i]);
        if (rval != NULL)
            break;
    }
    assert(rval != NULL);
    return rval;
}

/**
 * Looks up the name of the syntax with the given index.  Thus, this
 * is the inverse of {@link #unity_identify_parser}.
 * @param syntax one of the constants UNITY_SYNTAX_FITS, ...
 * @returns a string name for the syntax, or NULL if this is an unrecognised syntax
 */
const char* unity_get_syntax_name(const UnitySyntax syntax)
{
    int syntaxIdx = uu_syntax_to_integer(syntax);
    return known_syntaxes[syntaxIdx];
}

/**
 * Returns the name of this unit, as a URI.  These unit definitions
 * follow the <a href='http://qudt.org'>QUDT</a> framework, though
 * they are not restricted to the units described there.
 *
 * <p>This framework also supports the definition of unit kinds and
 * dimensions.
 * @returns a URI as a string
 */
const char* unity_get_unit_uri(const UnitDef* d)
{
    return (d == NULL ? NULL : d->uri);
}
/**
 * Returns the name of the unit.  This is a human-readable name such
 * as 'Metre' or 'Julian year', not the abbreviation 'm'
 */
const char* unity_get_unit_name(const UnitDef* d)
{
    return (d == NULL ? NULL : d->name);
}
/**
 * Returns the type of the unit, as a URI naming the 'kind' of thing
 * this measures.  This indicates 'Length' or 'Capacitance'.
 */
const char* unity_get_unit_type(const UnitDef* d)
{
    return (d == NULL ? NULL : d->type);
}
/**
 * Returns the dimensions of the unit
 *
 * <p>A dimensions string consists of a sequence of capital letter
 * dimensions, and powers, for example "M L2T-2" for the
 * dimensions of "kg m2 s-2".
 *
 * <p>This method reports the dimensions of the unit in the syntax employed by QUDT.
 * The QUDT dimension string is a sequence of letters [ULMTΘINJ]
 * and powers.  These match the ‘quantities’ in ISO 80000-1, with the
 * addition of ‘U’, which the QUDT authors use when describing the
 * Lumen as Candela-steradian, with string {@code U J}, and the Lux, with
 * string {@code U L^-2 J}.
 *
 * <p>The known dimensions are
 * <table>
 * <tr><th>Symbol<th>Name<th>SI base unit
 * <tr><td>U<td>Dimensionless<td>Unity
 * <tr><td>L<td>Length<td>Metre
 * <tr><td>M<td>Mass<td>Kilogramme
 * <tr><td>T<td>Time<td>Second
 * <tr><td>I<td>Electric current<td>Ampère
 * <tr><td>Θ<td>Thermodynamic temperature<td>Kelvin
 * <tr><td>N<td>Amount of substance<td>Mole
 * <tr><td>J<td>Luminous intensity<td>Candela
 * </table>
 */
const char* unity_get_unit_dimension(const UnitDef* d)
{
    return (d == NULL ? NULL : d->dimension);
}
/**
 * Returns a description of the unit.  This may be NULL if there is
 * nothing more to be said beyond the unit name.
 */
const char* unity_get_unit_description(const UnitDef* d)
{
    return (d == NULL ? NULL : d->description);
}
/**
 * A LaTeX version of the unit symbol, if there is one defined.
 */
const char* unity_get_unit_latex_form(const UnitDef* d)
{
    return (d == NULL ? NULL : d->latex_form);
}
