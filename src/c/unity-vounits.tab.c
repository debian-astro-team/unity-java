#include "known-syntaxes.h"
#include <stdlib.h>
static UnitySyntax PARSE_SYNTAX = UNITY_SYNTAX_VOUNITS;
#define vounits_yylex yylex
#define vounits_yyerror yyerror
#define vounits_yylval yylval
#define vounits_yydebug yydebug
/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with vounits_yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum vounits_yytokentype {
     SIGNED_INTEGER = 258,
     UNSIGNED_INTEGER = 259,
     FLOAT = 260,
     STRING = 261,
     QUOTED_STRING = 262,
     VOUFLOAT = 263,
     CDSFLOAT = 264,
     WHITESPACE = 265,
     STARSTAR = 266,
     CARET = 267,
     DIVISION = 268,
     DOT = 269,
     STAR = 270,
     PERCENT = 271,
     OPEN_P = 272,
     CLOSE_P = 273,
     OPEN_SQ = 274,
     CLOSE_SQ = 275,
     LIT10 = 276,
     LIT1 = 277
   };
#endif
/* Tokens.  */
#define SIGNED_INTEGER 258
#define UNSIGNED_INTEGER 259
#define FLOAT 260
#define STRING 261
#define QUOTED_STRING 262
#define VOUFLOAT 263
#define CDSFLOAT 264
#define WHITESPACE 265
#define STARSTAR 266
#define CARET 267
#define DIVISION 268
#define DOT 269
#define STAR 270
#define PERCENT 271
#define OPEN_P 272
#define CLOSE_P 273
#define OPEN_SQ 274
#define CLOSE_SQ 275
#define LIT10 276
#define LIT1 277




/* Copy the first part of user declarations.  */
#line 3 "unity-c,vounits.y"

    #include <stdio.h>
    #include <string.h>
    #include <math.h>
    #define UNITY_INTERNAL 1
    #include "unity.h"
    int vounits_yylex(void);
    void vounits_yyerror(const char*);
    void force_lexer_state(int);


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 14 "unity-c,vounits.y"
{
        int i;
        float f;
        float l10f;  /* log10(f) */
        char c;
        char* s;
        struct unit_struct* u;
        struct unit_struct* uList;
        // uExpr appears only in the VOUnits case
        struct unit_expression* uExpr;
}
/* Line 193 of yacc.c.  */
#line 163 "vounits_yy-unity-vounits.tab.c"
	YYSTYPE;
# define vounits_yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 176 "vounits_yy-unity-vounits.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 vounits_yytype_uint8;
#else
typedef unsigned char vounits_yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 vounits_yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char vounits_yytype_int8;
#else
typedef short int vounits_yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 vounits_yytype_uint16;
#else
typedef unsigned short int vounits_yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 vounits_yytype_int16;
#else
typedef short int vounits_yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined vounits_yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined vounits_yyoverflow || YYERROR_VERBOSE */


#if (! defined vounits_yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union vounits_yyalloc
{
  vounits_yytype_int16 vounits_yyss;
  YYSTYPE vounits_yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union vounits_yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (vounits_yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T vounits_yyi;				\
	  for (vounits_yyi = 0; vounits_yyi < (Count); vounits_yyi++)	\
	    (To)[vounits_yyi] = (From)[vounits_yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T vounits_yynewbytes;						\
	YYCOPY (&vounits_yyptr->Stack, Stack, vounits_yysize);				\
	Stack = &vounits_yyptr->Stack;						\
	vounits_yynewbytes = vounits_yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	vounits_yyptr += vounits_yynewbytes / sizeof (*vounits_yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  21
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   48

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  23
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  17
/* YYNRULES -- Number of rules.  */
#define YYNRULES  35
/* YYNRULES -- Number of states.  */
#define YYNSTATES  52

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   277

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? vounits_yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const vounits_yytype_uint8 vounits_yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const vounits_yytype_uint8 vounits_yyprhs[] =
{
       0,     0,     3,     5,     8,    10,    12,    16,    18,    22,
      24,    26,    30,    31,    37,    39,    42,    46,    48,    50,
      52,    54,    56,    60,    62,    64,    67,    69,    71,    73,
      75,    79,    83,    89,    91,    93
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const vounits_yytype_int8 vounits_yyrhs[] =
{
      24,     0,    -1,    25,    -1,    31,    25,    -1,    22,    -1,
      26,    -1,    26,    32,    27,    -1,    27,    -1,    26,    39,
      27,    -1,    33,    -1,    28,    -1,    17,    25,    18,    -1,
      -1,     6,    17,    29,    30,    18,    -1,    25,    -1,    31,
      25,    -1,    21,    35,    36,    -1,    21,    -1,    22,    -1,
       8,    -1,    13,    -1,    34,    -1,    34,    35,    36,    -1,
       6,    -1,     7,    -1,     6,     7,    -1,    16,    -1,    11,
      -1,    38,    -1,    37,    -1,    17,    38,    18,    -1,    17,
       5,    18,    -1,    17,    38,    32,     4,    18,    -1,     3,
      -1,     4,    -1,    14,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const vounits_yytype_uint8 vounits_yyrline[] =
{
       0,    66,    66,    69,    72,    77,   101,   106,   109,   114,
     117,   120,   125,   125,   140,   143,   147,   150,   153,   156,
     161,   163,   164,   169,   172,   175,   178,   183,   185,   186,
     189,   192,   195,   201,   201,   203
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const vounits_yytname[] =
{
  "$end", "error", "$undefined", "SIGNED_INTEGER", "UNSIGNED_INTEGER",
  "FLOAT", "STRING", "QUOTED_STRING", "VOUFLOAT", "CDSFLOAT", "WHITESPACE",
  "STARSTAR", "CARET", "DIVISION", "DOT", "STAR", "PERCENT", "OPEN_P",
  "CLOSE_P", "OPEN_SQ", "CLOSE_SQ", "LIT10", "LIT1", "$accept", "input",
  "complete_expression", "product_of_units", "unit_expression",
  "function_application", "@1", "function_operand", "scalefactor",
  "division", "term", "unit", "power", "numeric_power",
  "parenthesized_number", "integer", "product", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const vounits_yytype_uint16 vounits_yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const vounits_yytype_uint8 vounits_yyr1[] =
{
       0,    23,    24,    24,    24,    25,    25,    26,    26,    27,
      27,    27,    29,    28,    30,    30,    31,    31,    31,    31,
      32,    33,    33,    34,    34,    34,    34,    35,    36,    36,
      37,    37,    37,    38,    38,    39
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const vounits_yytype_uint8 vounits_yyr2[] =
{
       0,     2,     1,     2,     1,     1,     3,     1,     3,     1,
       1,     3,     0,     5,     1,     2,     3,     1,     1,     1,
       1,     1,     3,     1,     1,     2,     1,     1,     1,     1,
       3,     3,     5,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const vounits_yytype_uint8 vounits_yydefact[] =
{
       0,    23,    24,    19,    26,     0,    17,    18,     0,     2,
       5,     7,    10,     0,     9,    21,    25,    12,     0,    27,
       0,     1,    20,    35,     0,     0,     3,     0,     0,    11,
      33,    34,     0,    16,    29,    28,     6,     8,    22,    18,
      14,     0,     0,     0,     0,    13,    15,    31,    30,     0,
       0,    32
};

/* YYDEFGOTO[NTERM-NUM].  */
static const vounits_yytype_int8 vounits_yydefgoto[] =
{
      -1,     8,     9,    10,    11,    12,    28,    41,    13,    24,
      14,    15,    20,    33,    34,    35,    25
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -11
static const vounits_yytype_int8 vounits_yypact[] =
{
      -4,     2,   -11,   -11,   -11,    19,   -10,    33,    34,   -11,
      18,   -11,   -11,    19,   -11,   -10,   -11,   -11,    22,   -11,
       7,   -11,   -11,   -11,    19,    19,   -11,     7,    -1,   -11,
     -11,   -11,    25,   -11,   -11,   -11,   -11,   -11,   -11,   -11,
     -11,    23,    19,    24,     9,   -11,   -11,   -11,   -11,    10,
      26,   -11
};

/* YYPGOTO[NTERM-NUM].  */
static const vounits_yytype_int8 vounits_yypgoto[] =
{
     -11,   -11,    -5,   -11,    14,   -11,   -11,   -11,    15,     1,
     -11,   -11,    31,    20,   -11,    16,   -11
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -5
static const vounits_yytype_int8 vounits_yytable[] =
{
      18,    19,     1,     2,     3,     1,     2,     3,    26,    16,
      30,    31,     4,     5,    50,     4,     5,     6,     7,    17,
       6,    39,    22,    40,    32,     1,     2,    48,    30,    31,
      43,    22,    23,    -4,    21,     4,     5,    46,    36,    37,
      29,    45,    47,    42,    51,    49,    27,    38,    44
};

static const vounits_yytype_uint8 vounits_yycheck[] =
{
       5,    11,     6,     7,     8,     6,     7,     8,    13,     7,
       3,     4,    16,    17,     4,    16,    17,    21,    22,    17,
      21,    22,    13,    28,    17,     6,     7,    18,     3,     4,
       5,    13,    14,     0,     0,    16,    17,    42,    24,    25,
      18,    18,    18,    28,    18,    44,    15,    27,    32
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const vounits_yytype_uint8 vounits_yystos[] =
{
       0,     6,     7,     8,    16,    17,    21,    22,    24,    25,
      26,    27,    28,    31,    33,    34,     7,    17,    25,    11,
      35,     0,    13,    14,    32,    39,    25,    35,    29,    18,
       3,     4,    17,    36,    37,    38,    27,    27,    36,    22,
      25,    30,    31,     5,    38,    18,    25,    18,    18,    32,
       4,    18
};

#define vounits_yyerrok		(vounits_yyerrstatus = 0)
#define vounits_yyclearin	(vounits_yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto vounits_yyacceptlab
#define YYABORT		goto vounits_yyabortlab
#define YYERROR		goto vounits_yyerrorlab


/* Like YYERROR except do call vounits_yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto vounits_yyerrlab

#define YYRECOVERING()  (!!vounits_yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (vounits_yychar == YYEMPTY && vounits_yylen == 1)				\
    {								\
      vounits_yychar = (Token);						\
      vounits_yylval = (Value);						\
      vounits_yytoken = YYTRANSLATE (vounits_yychar);				\
      YYPOPSTACK (1);						\
      goto vounits_yybackup;						\
    }								\
  else								\
    {								\
      vounits_yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `vounits_yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX vounits_yylex (YYLEX_PARAM)
#else
# define YYLEX vounits_yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (vounits_yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (vounits_yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      vounits_yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
vounits_yy_symbol_value_print (FILE *vounits_yyoutput, int vounits_yytype, YYSTYPE const * const vounits_yyvaluep)
#else
static void
vounits_yy_symbol_value_print (vounits_yyoutput, vounits_yytype, vounits_yyvaluep)
    FILE *vounits_yyoutput;
    int vounits_yytype;
    YYSTYPE const * const vounits_yyvaluep;
#endif
{
  if (!vounits_yyvaluep)
    return;
# ifdef YYPRINT
  if (vounits_yytype < YYNTOKENS)
    YYPRINT (vounits_yyoutput, vounits_yytoknum[vounits_yytype], *vounits_yyvaluep);
# else
  YYUSE (vounits_yyoutput);
# endif
  switch (vounits_yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
vounits_yy_symbol_print (FILE *vounits_yyoutput, int vounits_yytype, YYSTYPE const * const vounits_yyvaluep)
#else
static void
vounits_yy_symbol_print (vounits_yyoutput, vounits_yytype, vounits_yyvaluep)
    FILE *vounits_yyoutput;
    int vounits_yytype;
    YYSTYPE const * const vounits_yyvaluep;
#endif
{
  if (vounits_yytype < YYNTOKENS)
    YYFPRINTF (vounits_yyoutput, "token %s (", vounits_yytname[vounits_yytype]);
  else
    YYFPRINTF (vounits_yyoutput, "nterm %s (", vounits_yytname[vounits_yytype]);

  vounits_yy_symbol_value_print (vounits_yyoutput, vounits_yytype, vounits_yyvaluep);
  YYFPRINTF (vounits_yyoutput, ")");
}

/*------------------------------------------------------------------.
| vounits_yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
vounits_yy_stack_print (vounits_yytype_int16 *bottom, vounits_yytype_int16 *top)
#else
static void
vounits_yy_stack_print (bottom, top)
    vounits_yytype_int16 *bottom;
    vounits_yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (vounits_yydebug)							\
    vounits_yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
vounits_yy_reduce_print (YYSTYPE *vounits_yyvsp, int vounits_yyrule)
#else
static void
vounits_yy_reduce_print (vounits_yyvsp, vounits_yyrule)
    YYSTYPE *vounits_yyvsp;
    int vounits_yyrule;
#endif
{
  int vounits_yynrhs = vounits_yyr2[vounits_yyrule];
  int vounits_yyi;
  unsigned long int vounits_yylno = vounits_yyrline[vounits_yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     vounits_yyrule - 1, vounits_yylno);
  /* The symbols being reduced.  */
  for (vounits_yyi = 0; vounits_yyi < vounits_yynrhs; vounits_yyi++)
    {
      fprintf (stderr, "   $%d = ", vounits_yyi + 1);
      vounits_yy_symbol_print (stderr, vounits_yyrhs[vounits_yyprhs[vounits_yyrule] + vounits_yyi],
		       &(vounits_yyvsp[(vounits_yyi + 1) - (vounits_yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (vounits_yydebug)				\
    vounits_yy_reduce_print (vounits_yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
extern int vounits_yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef vounits_yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define vounits_yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
vounits_yystrlen (const char *vounits_yystr)
#else
static YYSIZE_T
vounits_yystrlen (vounits_yystr)
    const char *vounits_yystr;
#endif
{
  YYSIZE_T vounits_yylen;
  for (vounits_yylen = 0; vounits_yystr[vounits_yylen]; vounits_yylen++)
    continue;
  return vounits_yylen;
}
#  endif
# endif

# ifndef vounits_yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define vounits_yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
vounits_yystpcpy (char *vounits_yydest, const char *vounits_yysrc)
#else
static char *
vounits_yystpcpy (vounits_yydest, vounits_yysrc)
    char *vounits_yydest;
    const char *vounits_yysrc;
#endif
{
  char *vounits_yyd = vounits_yydest;
  const char *vounits_yys = vounits_yysrc;

  while ((*vounits_yyd++ = *vounits_yys++) != '\0')
    continue;

  return vounits_yyd - 1;
}
#  endif
# endif

# ifndef vounits_yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for vounits_yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from vounits_yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
vounits_yytnamerr (char *vounits_yyres, const char *vounits_yystr)
{
  if (*vounits_yystr == '"')
    {
      YYSIZE_T vounits_yyn = 0;
      char const *vounits_yyp = vounits_yystr;

      for (;;)
	switch (*++vounits_yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++vounits_yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (vounits_yyres)
	      vounits_yyres[vounits_yyn] = *vounits_yyp;
	    vounits_yyn++;
	    break;

	  case '"':
	    if (vounits_yyres)
	      vounits_yyres[vounits_yyn] = '\0';
	    return vounits_yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! vounits_yyres)
    return vounits_yystrlen (vounits_yystr);

  return vounits_yystpcpy (vounits_yyres, vounits_yystr) - vounits_yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
vounits_yysyntax_error (char *vounits_yyresult, int vounits_yystate, int vounits_yychar)
{
  int vounits_yyn = vounits_yypact[vounits_yystate];

  if (! (YYPACT_NINF < vounits_yyn && vounits_yyn <= YYLAST))
    return 0;
  else
    {
      int vounits_yytype = YYTRANSLATE (vounits_yychar);
      YYSIZE_T vounits_yysize0 = vounits_yytnamerr (0, vounits_yytname[vounits_yytype]);
      YYSIZE_T vounits_yysize = vounits_yysize0;
      YYSIZE_T vounits_yysize1;
      int vounits_yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *vounits_yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int vounits_yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *vounits_yyfmt;
      char const *vounits_yyf;
      static char const vounits_yyunexpected[] = "syntax error, unexpected %s";
      static char const vounits_yyexpecting[] = ", expecting %s";
      static char const vounits_yyor[] = " or %s";
      char vounits_yyformat[sizeof vounits_yyunexpected
		    + sizeof vounits_yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof vounits_yyor - 1))];
      char const *vounits_yyprefix = vounits_yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int vounits_yyxbegin = vounits_yyn < 0 ? -vounits_yyn : 0;

      /* Stay within bounds of both vounits_yycheck and vounits_yytname.  */
      int vounits_yychecklim = YYLAST - vounits_yyn + 1;
      int vounits_yyxend = vounits_yychecklim < YYNTOKENS ? vounits_yychecklim : YYNTOKENS;
      int vounits_yycount = 1;

      vounits_yyarg[0] = vounits_yytname[vounits_yytype];
      vounits_yyfmt = vounits_yystpcpy (vounits_yyformat, vounits_yyunexpected);

      for (vounits_yyx = vounits_yyxbegin; vounits_yyx < vounits_yyxend; ++vounits_yyx)
	if (vounits_yycheck[vounits_yyx + vounits_yyn] == vounits_yyx && vounits_yyx != YYTERROR)
	  {
	    if (vounits_yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		vounits_yycount = 1;
		vounits_yysize = vounits_yysize0;
		vounits_yyformat[sizeof vounits_yyunexpected - 1] = '\0';
		break;
	      }
	    vounits_yyarg[vounits_yycount++] = vounits_yytname[vounits_yyx];
	    vounits_yysize1 = vounits_yysize + vounits_yytnamerr (0, vounits_yytname[vounits_yyx]);
	    vounits_yysize_overflow |= (vounits_yysize1 < vounits_yysize);
	    vounits_yysize = vounits_yysize1;
	    vounits_yyfmt = vounits_yystpcpy (vounits_yyfmt, vounits_yyprefix);
	    vounits_yyprefix = vounits_yyor;
	  }

      vounits_yyf = YY_(vounits_yyformat);
      vounits_yysize1 = vounits_yysize + vounits_yystrlen (vounits_yyf);
      vounits_yysize_overflow |= (vounits_yysize1 < vounits_yysize);
      vounits_yysize = vounits_yysize1;

      if (vounits_yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (vounits_yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *vounits_yyp = vounits_yyresult;
	  int vounits_yyi = 0;
	  while ((*vounits_yyp = *vounits_yyf) != '\0')
	    {
	      if (*vounits_yyp == '%' && vounits_yyf[1] == 's' && vounits_yyi < vounits_yycount)
		{
		  vounits_yyp += vounits_yytnamerr (vounits_yyp, vounits_yyarg[vounits_yyi++]);
		  vounits_yyf += 2;
		}
	      else
		{
		  vounits_yyp++;
		  vounits_yyf++;
		}
	    }
	}
      return vounits_yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
vounits_yydestruct (const char *vounits_yymsg, int vounits_yytype, YYSTYPE *vounits_yyvaluep)
#else
static void
vounits_yydestruct (vounits_yymsg, vounits_yytype, vounits_yyvaluep)
    const char *vounits_yymsg;
    int vounits_yytype;
    YYSTYPE *vounits_yyvaluep;
#endif
{
  YYUSE (vounits_yyvaluep);

  if (!vounits_yymsg)
    vounits_yymsg = "Deleting";
  YY_SYMBOL_PRINT (vounits_yymsg, vounits_yytype, vounits_yyvaluep, vounits_yylocationp);

  switch (vounits_yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int vounits_yyparse (void *YYPARSE_PARAM);
#else
int vounits_yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int vounits_yyparse (void);
#else
int vounits_yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int vounits_yychar;

/* The semantic value of the look-ahead symbol.  */
extern YYSTYPE vounits_yylval;

/* Number of syntax errors so far.  */
int vounits_yynerrs;



/*----------.
| vounits_yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
vounits_yyparse (void *YYPARSE_PARAM)
#else
int
vounits_yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
vounits_yyparse (void)
#else
int
vounits_yyparse ()

#endif
#endif
{
  
  int vounits_yystate;
  int vounits_yyn;
  int vounits_yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int vounits_yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int vounits_yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char vounits_yymsgbuf[128];
  char *vounits_yymsg = vounits_yymsgbuf;
  YYSIZE_T vounits_yymsg_alloc = sizeof vounits_yymsgbuf;
#endif

  /* Three stacks and their tools:
     `vounits_yyss': related to states,
     `vounits_yyvs': related to semantic values,
     `vounits_yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow vounits_yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  vounits_yytype_int16 vounits_yyssa[YYINITDEPTH];
  vounits_yytype_int16 *vounits_yyss = vounits_yyssa;
  vounits_yytype_int16 *vounits_yyssp;

  /* The semantic value stack.  */
  YYSTYPE vounits_yyvsa[YYINITDEPTH];
  YYSTYPE *vounits_yyvs = vounits_yyvsa;
  YYSTYPE *vounits_yyvsp;



#define YYPOPSTACK(N)   (vounits_yyvsp -= (N), vounits_yyssp -= (N))

  YYSIZE_T vounits_yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE vounits_yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int vounits_yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  vounits_yystate = 0;
  vounits_yyerrstatus = 0;
  vounits_yynerrs = 0;
  vounits_yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  vounits_yyssp = vounits_yyss;
  vounits_yyvsp = vounits_yyvs;

  goto vounits_yysetstate;

/*------------------------------------------------------------.
| vounits_yynewstate -- Push a new state, which is found in vounits_yystate.  |
`------------------------------------------------------------*/
 vounits_yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  vounits_yyssp++;

 vounits_yysetstate:
  *vounits_yyssp = vounits_yystate;

  if (vounits_yyss + vounits_yystacksize - 1 <= vounits_yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T vounits_yysize = vounits_yyssp - vounits_yyss + 1;

#ifdef vounits_yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *vounits_yyvs1 = vounits_yyvs;
	vounits_yytype_int16 *vounits_yyss1 = vounits_yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if vounits_yyoverflow is a macro.  */
	vounits_yyoverflow (YY_("memory exhausted"),
		    &vounits_yyss1, vounits_yysize * sizeof (*vounits_yyssp),
		    &vounits_yyvs1, vounits_yysize * sizeof (*vounits_yyvsp),

		    &vounits_yystacksize);

	vounits_yyss = vounits_yyss1;
	vounits_yyvs = vounits_yyvs1;
      }
#else /* no vounits_yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto vounits_yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= vounits_yystacksize)
	goto vounits_yyexhaustedlab;
      vounits_yystacksize *= 2;
      if (YYMAXDEPTH < vounits_yystacksize)
	vounits_yystacksize = YYMAXDEPTH;

      {
	vounits_yytype_int16 *vounits_yyss1 = vounits_yyss;
	union vounits_yyalloc *vounits_yyptr =
	  (union vounits_yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (vounits_yystacksize));
	if (! vounits_yyptr)
	  goto vounits_yyexhaustedlab;
	YYSTACK_RELOCATE (vounits_yyss);
	YYSTACK_RELOCATE (vounits_yyvs);

#  undef YYSTACK_RELOCATE
	if (vounits_yyss1 != vounits_yyssa)
	  YYSTACK_FREE (vounits_yyss1);
      }
# endif
#endif /* no vounits_yyoverflow */

      vounits_yyssp = vounits_yyss + vounits_yysize - 1;
      vounits_yyvsp = vounits_yyvs + vounits_yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) vounits_yystacksize));

      if (vounits_yyss + vounits_yystacksize - 1 <= vounits_yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", vounits_yystate));

  goto vounits_yybackup;

/*-----------.
| vounits_yybackup.  |
`-----------*/
vounits_yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  vounits_yyn = vounits_yypact[vounits_yystate];
  if (vounits_yyn == YYPACT_NINF)
    goto vounits_yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (vounits_yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      vounits_yychar = YYLEX;
    }

  if (vounits_yychar <= YYEOF)
    {
      vounits_yychar = vounits_yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      vounits_yytoken = YYTRANSLATE (vounits_yychar);
      YY_SYMBOL_PRINT ("Next token is", vounits_yytoken, &vounits_yylval, &vounits_yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  vounits_yyn += vounits_yytoken;
  if (vounits_yyn < 0 || YYLAST < vounits_yyn || vounits_yycheck[vounits_yyn] != vounits_yytoken)
    goto vounits_yydefault;
  vounits_yyn = vounits_yytable[vounits_yyn];
  if (vounits_yyn <= 0)
    {
      if (vounits_yyn == 0 || vounits_yyn == YYTABLE_NINF)
	goto vounits_yyerrlab;
      vounits_yyn = -vounits_yyn;
      goto vounits_yyreduce;
    }

  if (vounits_yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (vounits_yyerrstatus)
    vounits_yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", vounits_yytoken, &vounits_yylval, &vounits_yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (vounits_yychar != YYEOF)
    vounits_yychar = YYEMPTY;

  vounits_yystate = vounits_yyn;
  *++vounits_yyvsp = vounits_yylval;

  goto vounits_yynewstate;


/*-----------------------------------------------------------.
| vounits_yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
vounits_yydefault:
  vounits_yyn = vounits_yydefact[vounits_yystate];
  if (vounits_yyn == 0)
    goto vounits_yyerrlab;
  goto vounits_yyreduce;


/*-----------------------------.
| vounits_yyreduce -- Do a reduction.  |
`-----------------------------*/
vounits_yyreduce:
  /* vounits_yyn is the number of a rule to reduce with.  */
  vounits_yylen = vounits_yyr2[vounits_yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  vounits_yyval = vounits_yyvsp[1-vounits_yylen];


  YY_REDUCE_PRINT (vounits_yyn);
  switch (vounits_yyn)
    {
        case 2:
#line 66 "unity-c,vounits.y"
    {
                u_receive_result(u_new_expression(0, (vounits_yyvsp[(1) - (1)].uList)));
          }
    break;

  case 3:
#line 69 "unity-c,vounits.y"
    {
                u_receive_result(u_new_expression((vounits_yyvsp[(1) - (2)].f), (vounits_yyvsp[(2) - (2)].uList)));
        }
    break;

  case 4:
#line 72 "unity-c,vounits.y"
    {
                u_receive_result(u_new_expression(0, NULL));
          }
    break;

  case 5:
#line 77 "unity-c,vounits.y"
    {
                (vounits_yyval.uList) = (vounits_yyvsp[(1) - (1)].uList);
          }
    break;

  case 6:
#line 101 "unity-c,vounits.y"
    {
                (vounits_yyval.uList) = u_unit_append((vounits_yyvsp[(1) - (3)].uList), u_unit_reciprocal((vounits_yyvsp[(3) - (3)].uList)));
          }
    break;

  case 7:
#line 106 "unity-c,vounits.y"
    {
            (vounits_yyval.uList) = (vounits_yyvsp[(1) - (1)].uList);
          }
    break;

  case 8:
#line 109 "unity-c,vounits.y"
    {
                      (vounits_yyval.uList) = u_unit_append((vounits_yyvsp[(1) - (3)].uList), (vounits_yyvsp[(3) - (3)].uList));
          }
    break;

  case 9:
#line 114 "unity-c,vounits.y"
    {
                (vounits_yyval.uList) = (vounits_yyvsp[(1) - (1)].u);
          }
    break;

  case 10:
#line 117 "unity-c,vounits.y"
    {
            (vounits_yyval.uList) = (vounits_yyvsp[(1) - (1)].uList);
          }
    break;

  case 11:
#line 120 "unity-c,vounits.y"
    {
            (vounits_yyval.uList) = (vounits_yyvsp[(2) - (3)].uList);
          }
    break;

  case 12:
#line 125 "unity-c,vounits.y"
    {
//                                    The following is BEGIN(vouinitial),
//                                    but BEGIN is defined internally to the generated lexer,
//                                    and isn't exposed, so...
//                                    we have to use the force_lexer_state function
//                                    that the Makefile smuggled into unity-lexer.c
//                                    ... YUK (no, I don't like this any more than you do)
force_lexer_state(lexer_vouinitial);
                                    }
    break;

  case 13:
#line 133 "unity-c,vounits.y"
    {
                      UnitExpression* ue = (vounits_yyvsp[(4) - (5)].uExpr);
                      (vounits_yyval.uList) = u_function_application((vounits_yyvsp[(1) - (5)].s),
                                                  ue->log_factor, ue->unit_sequence,
                                                  PARSE_SYNTAX);
                      free(ue);
          }
    break;

  case 14:
#line 140 "unity-c,vounits.y"
    {
                      (vounits_yyval.uExpr) = u_new_expression(0.0, (vounits_yyvsp[(1) - (1)].uList));
          }
    break;

  case 15:
#line 143 "unity-c,vounits.y"
    {
                      (vounits_yyval.uExpr) = u_new_expression((vounits_yyvsp[(1) - (2)].f), (vounits_yyvsp[(2) - (2)].uList));
        }
    break;

  case 16:
#line 147 "unity-c,vounits.y"
    { // eg 10^3
(vounits_yyval.f) = (vounits_yyvsp[(3) - (3)].f);
                  }
    break;

  case 17:
#line 150 "unity-c,vounits.y"
    {
            (vounits_yyval.f) = 1;  /* log(10) = 1 */
          }
    break;

  case 18:
#line 153 "unity-c,vounits.y"
    {
            (vounits_yyval.f) = 0;  /* log(1) = 0 */
          }
    break;

  case 19:
#line 156 "unity-c,vounits.y"
    {
                      (vounits_yyval.f) = (vounits_yyvsp[(1) - (1)].l10f);
          }
    break;

  case 21:
#line 163 "unity-c,vounits.y"
    { (vounits_yyval.u) = (vounits_yyvsp[(1) - (1)].u); }
    break;

  case 22:
#line 164 "unity-c,vounits.y"
    {
                      (vounits_yyval.u) = u_power_of_unit((vounits_yyvsp[(1) - (3)].u), (vounits_yyvsp[(3) - (3)].f));
          }
    break;

  case 23:
#line 169 "unity-c,vounits.y"
    {
                      (vounits_yyval.u) = u_new_unit((vounits_yyvsp[(1) - (1)].s), 1, PARSE_SYNTAX);
          }
    break;

  case 24:
#line 172 "unity-c,vounits.y"
    {
                      (vounits_yyval.u) = u_quoted_unit(NULL, (vounits_yyvsp[(1) - (1)].s), 1, PARSE_SYNTAX);
          }
    break;

  case 25:
#line 175 "unity-c,vounits.y"
    {
                      (vounits_yyval.u) = u_quoted_unit((vounits_yyvsp[(1) - (2)].s), (vounits_yyvsp[(2) - (2)].s), 1, PARSE_SYNTAX);
          }
    break;

  case 26:
#line 178 "unity-c,vounits.y"
    {
                      (vounits_yyval.u) = u_new_unit("%", 1, PARSE_SYNTAX);
          }
    break;

  case 28:
#line 185 "unity-c,vounits.y"
    { (vounits_yyval.f) = (vounits_yyvsp[(1) - (1)].i); }
    break;

  case 29:
#line 186 "unity-c,vounits.y"
    { (vounits_yyval.f) = (vounits_yyvsp[(1) - (1)].f); }
    break;

  case 30:
#line 189 "unity-c,vounits.y"
    {
            (vounits_yyval.f) = (vounits_yyvsp[(2) - (3)].i);
          }
    break;

  case 31:
#line 192 "unity-c,vounits.y"
    {
            (vounits_yyval.f) = (vounits_yyvsp[(2) - (3)].f);
          }
    break;

  case 32:
#line 195 "unity-c,vounits.y"
    {
            // This is certainly OK for FITS, not clear for OGIP
            (vounits_yyval.f) = ((float)(vounits_yyvsp[(2) - (5)].i))/((float)((vounits_yyvsp[(4) - (5)].i)));
          }
    break;


/* Line 1267 of yacc.c.  */
#line 1619 "vounits_yy-unity-vounits.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", vounits_yyr1[vounits_yyn], &vounits_yyval, &vounits_yyloc);

  YYPOPSTACK (vounits_yylen);
  vounits_yylen = 0;
  YY_STACK_PRINT (vounits_yyss, vounits_yyssp);

  *++vounits_yyvsp = vounits_yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  vounits_yyn = vounits_yyr1[vounits_yyn];

  vounits_yystate = vounits_yypgoto[vounits_yyn - YYNTOKENS] + *vounits_yyssp;
  if (0 <= vounits_yystate && vounits_yystate <= YYLAST && vounits_yycheck[vounits_yystate] == *vounits_yyssp)
    vounits_yystate = vounits_yytable[vounits_yystate];
  else
    vounits_yystate = vounits_yydefgoto[vounits_yyn - YYNTOKENS];

  goto vounits_yynewstate;


/*------------------------------------.
| vounits_yyerrlab -- here on detecting error |
`------------------------------------*/
vounits_yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!vounits_yyerrstatus)
    {
      ++vounits_yynerrs;
#if ! YYERROR_VERBOSE
      vounits_yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T vounits_yysize = vounits_yysyntax_error (0, vounits_yystate, vounits_yychar);
	if (vounits_yymsg_alloc < vounits_yysize && vounits_yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T vounits_yyalloc = 2 * vounits_yysize;
	    if (! (vounits_yysize <= vounits_yyalloc && vounits_yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      vounits_yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (vounits_yymsg != vounits_yymsgbuf)
	      YYSTACK_FREE (vounits_yymsg);
	    vounits_yymsg = (char *) YYSTACK_ALLOC (vounits_yyalloc);
	    if (vounits_yymsg)
	      vounits_yymsg_alloc = vounits_yyalloc;
	    else
	      {
		vounits_yymsg = vounits_yymsgbuf;
		vounits_yymsg_alloc = sizeof vounits_yymsgbuf;
	      }
	  }

	if (0 < vounits_yysize && vounits_yysize <= vounits_yymsg_alloc)
	  {
	    (void) vounits_yysyntax_error (vounits_yymsg, vounits_yystate, vounits_yychar);
	    vounits_yyerror (vounits_yymsg);
	  }
	else
	  {
	    vounits_yyerror (YY_("syntax error"));
	    if (vounits_yysize != 0)
	      goto vounits_yyexhaustedlab;
	  }
      }
#endif
    }



  if (vounits_yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (vounits_yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (vounits_yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  vounits_yydestruct ("Error: discarding",
		      vounits_yytoken, &vounits_yylval);
	  vounits_yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto vounits_yyerrlab1;


/*---------------------------------------------------.
| vounits_yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
vounits_yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label vounits_yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto vounits_yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (vounits_yylen);
  vounits_yylen = 0;
  YY_STACK_PRINT (vounits_yyss, vounits_yyssp);
  vounits_yystate = *vounits_yyssp;
  goto vounits_yyerrlab1;


/*-------------------------------------------------------------.
| vounits_yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
vounits_yyerrlab1:
  vounits_yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      vounits_yyn = vounits_yypact[vounits_yystate];
      if (vounits_yyn != YYPACT_NINF)
	{
	  vounits_yyn += YYTERROR;
	  if (0 <= vounits_yyn && vounits_yyn <= YYLAST && vounits_yycheck[vounits_yyn] == YYTERROR)
	    {
	      vounits_yyn = vounits_yytable[vounits_yyn];
	      if (0 < vounits_yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (vounits_yyssp == vounits_yyss)
	YYABORT;


      vounits_yydestruct ("Error: popping",
		  vounits_yystos[vounits_yystate], vounits_yyvsp);
      YYPOPSTACK (1);
      vounits_yystate = *vounits_yyssp;
      YY_STACK_PRINT (vounits_yyss, vounits_yyssp);
    }

  if (vounits_yyn == YYFINAL)
    YYACCEPT;

  *++vounits_yyvsp = vounits_yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", vounits_yystos[vounits_yyn], vounits_yyvsp, vounits_yylsp);

  vounits_yystate = vounits_yyn;
  goto vounits_yynewstate;


/*-------------------------------------.
| vounits_yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
vounits_yyacceptlab:
  vounits_yyresult = 0;
  goto vounits_yyreturn;

/*-----------------------------------.
| vounits_yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
vounits_yyabortlab:
  vounits_yyresult = 1;
  goto vounits_yyreturn;

#ifndef vounits_yyoverflow
/*-------------------------------------------------.
| vounits_yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
vounits_yyexhaustedlab:
  vounits_yyerror (YY_("memory exhausted"));
  vounits_yyresult = 2;
  /* Fall through.  */
#endif

vounits_yyreturn:
  if (vounits_yychar != YYEOF && vounits_yychar != YYEMPTY)
     vounits_yydestruct ("Cleanup: discarding lookahead",
		 vounits_yytoken, &vounits_yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (vounits_yylen);
  YY_STACK_PRINT (vounits_yyss, vounits_yyssp);
  while (vounits_yyssp != vounits_yyss)
    {
      vounits_yydestruct ("Cleanup: popping",
		  vounits_yystos[*vounits_yyssp], vounits_yyvsp);
      YYPOPSTACK (1);
    }
#ifndef vounits_yyoverflow
  if (vounits_yyss != vounits_yyssa)
    YYSTACK_FREE (vounits_yyss);
#endif
#if YYERROR_VERBOSE
  if (vounits_yymsg != vounits_yymsgbuf)
    YYSTACK_FREE (vounits_yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (vounits_yyresult);
}


#line 204 "unity-c,vounits.y"



