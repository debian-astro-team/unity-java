/*
 * This is a GENERATED FILE -- do not edit.
 * See known-functions.csv and buildtools/src/ParseFunctions.java
 */
package uk.me.nxg.unity;

import java.util.Map;
import java.util.List;


class UnityFunctionDefinitionMap extends FunctionDefinitionMap {
    FunctionDefinition[] fd;
    Map<String,FunctionDefinition> functionLookup = new java.util.HashMap<String,FunctionDefinition>();
    Map<Syntax,Map<FunctionDefinition,String>> allMappings = new java.util.HashMap<Syntax,Map<FunctionDefinition,String>>();
    // Map<String,FunctionDefinition> functionLookup = new java.util.HashMap<String,FunctionDefinition>();
    // Map<String,Map<FunctionDefinition,String>> allMappings = new java.util.HashMap<String,Map<FunctionDefinition,String>>();

    UnityFunctionDefinitionMap() {
        super();
    }

    public FunctionDefinitionMap.Resolver getResolver(Syntax syntax) {
        final Map<FunctionDefinition,String> m = allMappings.get(syntax);
        if (m == null) {
            return null;
        } else {
            return new FunctionDefinitionMap.Resolver() {
                public FunctionDefinition getFunctionDefinition(String functionName) {
                    return functionLookup.get(functionName);
                }
                public String getFunctionName(FunctionDefinition fd) {
                    return m.get(fd);
                }
            };
        }
    }

    UnityFunctionDefinitionMap initialize() {
    fd = new FunctionDefinition[13];
    fd[0] = new FunctionDefinition("Common Logarithm (base 10)", "\\log", "log");
    fd[1] = new FunctionDefinition("Natural Logarithm", "\\ln", "ln");
    fd[2] = new FunctionDefinition("Exponential", "\\exp", "exp");
    fd[3] = new FunctionDefinition("Square root", "\\sqrt", "sqrt");
    fd[4] = new FunctionDefinition("Sine", "\\sin", "sin");
    fd[5] = new FunctionDefinition("Cosine", "\\cos", "cos");
    fd[6] = new FunctionDefinition("Tangent", "\\tan", "tan");
    fd[7] = new FunctionDefinition("Arc sine", "\\arcsin", "asin");
    fd[8] = new FunctionDefinition("Arc cosine", "\\arccos", "acos");
    fd[9] = new FunctionDefinition("Arc tangent", "\\arctan", "atan");
    fd[10] = new FunctionDefinition("Hyperbolic sine", "\\sinh", "sinh");
    fd[11] = new FunctionDefinition("Hyperbolic cosine", "\\cosh", "cosh");
    fd[12] = new FunctionDefinition("Hyperbolic tangent", "\\tanh", "tanh");

    Map<FunctionDefinition,String> m;
    m = new java.util.HashMap<FunctionDefinition,String>();
    m.put(fd[2], "exp");
    functionLookup.put("exp", fd[2]);
    m.put(fd[1], "ln");
    functionLookup.put("ln", fd[1]);
    m.put(fd[0], "log");
    functionLookup.put("log", fd[0]);
    m.put(fd[3], "sqrt");
    functionLookup.put("sqrt", fd[3]);
    allMappings.put(Syntax.FITS, m);

    m = new java.util.HashMap<FunctionDefinition,String>();
    m.put(fd[8], "acos");
    functionLookup.put("acos", fd[8]);
    m.put(fd[7], "asin");
    functionLookup.put("asin", fd[7]);
    m.put(fd[9], "atan");
    functionLookup.put("atan", fd[9]);
    m.put(fd[5], "cos");
    functionLookup.put("cos", fd[5]);
    m.put(fd[11], "cosh");
    functionLookup.put("cosh", fd[11]);
    m.put(fd[2], "exp");
    functionLookup.put("exp", fd[2]);
    m.put(fd[1], "ln");
    functionLookup.put("ln", fd[1]);
    m.put(fd[0], "log");
    functionLookup.put("log", fd[0]);
    m.put(fd[4], "sin");
    functionLookup.put("sin", fd[4]);
    m.put(fd[10], "sinh");
    functionLookup.put("sinh", fd[10]);
    m.put(fd[3], "sqrt");
    functionLookup.put("sqrt", fd[3]);
    m.put(fd[6], "tan");
    functionLookup.put("tan", fd[6]);
    m.put(fd[12], "tanh");
    functionLookup.put("tanh", fd[12]);
    allMappings.put(Syntax.OGIP, m);

    m = new java.util.HashMap<FunctionDefinition,String>();
    allMappings.put(Syntax.CDS, m);

    m = new java.util.HashMap<FunctionDefinition,String>();
    m.put(fd[2], "exp");
    functionLookup.put("exp", fd[2]);
    m.put(fd[1], "ln");
    functionLookup.put("ln", fd[1]);
    m.put(fd[0], "log");
    functionLookup.put("log", fd[0]);
    m.put(fd[3], "sqrt");
    functionLookup.put("sqrt", fd[3]);
    allMappings.put(Syntax.VOUNITS, m);

        return this;
    }
}
