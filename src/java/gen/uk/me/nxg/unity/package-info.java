/* GENERATED FILE -- DO NOT EDIT */
/* Derived from ../package-info.txt by Makefile. */
/**
The Unity package provides a parser for unit strings.

<p>This is the unity parser
(version 1.1, released 2023 October 22),
which is a
Java class library
to help parse unit specification strings such as <code>W.mm**-2</code>.
There is also an associated
C library,
which uses the same grammars.
For more details, see
<a href='https://purl.org/nxg/dist/unity'>the library's home page</a>;
and <a href='https://heptapod.host/nxg/unity'>repository</a>.</p>

<p>As well as parsing various unit strings, the library can also
serialise a parsed expression in various formats, including the four
formats that it can parse, a LaTeX version with name
<code>latex</code> (which uses the <code>{siunitx}</code> package) and
a <code>debug</code> format which lists the parsed unit in an
unambiguous, but not otherwise useful, form.</p>

<h2>Parsing Units</h2>
<p>You can parse units using a couple of different syntaxes since,
unfortunately, there is no general consensus on which syntax the world
should agree on.  The ones supported
are as follows
(cf {@link uk.me.nxg.unity.Syntax} for the list of parsers)
:
</p>
<dl>
<dt>fits</dt>
<dd>See
    FITS, 3.0 Sect.4.3
    (<a href='https://doi.org/10.1051/0004-6361/201015362'>W.D. Pence et al., A&amp;A 524, A42, 2010</a>);
    v4.0 Sect.4.3 (<a href='https://fits.gsfc.nasa.gov/fits_standard.html'>FITS standards page</a>));
    and further comments in the
    <a href='https://doi.org/10.1051/0004-6361/201424653'>FITS WCS IV paper</a>.</dd>
<dt>ogip</dt>
<dd><a href='https://heasarc.gsfc.nasa.gov/docs/heasarc/ofwg/docs/general/ogip_93_001/'
>OGIP memo OGIP/93-001, 1993</a></dd>
<dt>cds</dt>
<dd><a href='https://vizier.u-strasbg.fr/vizier/doc/catstd-3.2.htx'
>Standards for Astronomical Catalogues, Version 2.0, section 3.2, 2000</a></dd>
<dt>vounits</dt>
<dd><a href='http://ivoa.net/Documents/VOUnits/'>IVOA VOUnits Recommendation</a></dd>
</dl>

<p>See also:</p>
<ul>
<li>The <a href='https://www.iau.org/static/publications/stylemanual1989.pdf'
>IAU style manual, section 5.1 (1989)</a> is by now rather old, but
appears to be one of the few existing standards for units, specific to
astronomy.</li>
<li>ISO/IEC 80000 (parts 1–13) describes a variety of units, including
the specification of the 'binary' prefixes kibi, mebi, and so on (see
ISO/IEC 80000-13 Sect.4,
and IEEE standard 1541-2002).</li>
<li>The VOUnits Recommendation discusses various tradeoffs and
conflicting specifications, at some length.</li>
</ul>


<p>Each of these has an associated <em>writer</em>, which allows you to
write a parsed UnitExpression to a string, in a format which should be
conformant with the particular syntax's standard.
See {@link uk.me.nxg.unity.UnitExpr#toString}.
</p>

<p>In addition, there is a <em>latex</em> writer, which produces a
formatted form for the the expression, in a form suitable for
inclusion in a LaTeX document, using the <code>siunitx</code>
package.  To use the resulting output in a LaTeX document, include the
following in the preamble of the file:</p>
<pre>
&#92;usepackage{siunitx}
&#92;DeclareSIQualifier&#92;solar{$&#92;odot$}
</pre>
<p>You may add any <code>siunitx</code> options that seem convenient,
and you may omit the declaration of
<code>\solar</code>
if the units in the document do not include the various solar ones.</p>

<p>The parsing is permissive, to the extent that it permits non-recognised and deprecated
units.  The result of the parse may be checked for conformance with
one or other standard using
the methods
{@link uk.me.nxg.unity.UnitExpr#allUnitsRecognised},
{@link uk.me.nxg.unity.UnitExpr#allUnitsRecommended} and
{@link uk.me.nxg.unity.UnitExpr#allUsageConstraintsSatisfied}.
Note that SI prefixes are still noticed for unrecognised units: thus <code>furlongs/fortnight</code>
will be parsed as femto-urlongs per femto-ortnight.  The same is not
true of recognised units: a <code>pixel/s</code> is a pixel per
second, and does not involve a pico-ixel.</p>

<h3>Known units</h3>

<p>The various unit syntaxes have different sets of ‘known units’, namely
units, and unit abbreviations, which the syntax blesses as
recommended, or at least acknowledged.  The list of knowns units in
the various syntaxes is below.</p>

<p>Much of the unit information, such as unit names and dimensions, is
derived from the <a href='http://www.qudt.org'>QUDT</a> unit ontology
which (from its self-description) was
‘developed for the NASA Exploration Initiatives Ontology Models
(NExIOM) project, a Constellation Program initiative at the AMES
Research Center (ARC).’</p>


<h2>Demo</h2>

<p>If you want to experiment with the library, you can do so using the jar file:</p>
<pre>
    % java -jar unity.jar -icds -oogip 'mm2/s'
    mm**2 /s
    % java -jar unity.jar -icds -ofits -v mm/s
    mm s-1
    check: all units recognised?           yes
    check: all units recommended?          yes
    check: all units satisfy constraints?  yes
    % java -jar unity.jar -ifits -ocds -v 'merg/s'
    merg/s
    check: all units recognised?           yes
    check: all units recommended?          no
    check: all units satisfy constraints?  no
    % java -jar unity.jar -icds -ofits -v 'merg/s'
    merg s-1
    check: all units recognised?           no
    check: all units recommended?          no
    check: all units satisfy constraints?  yes
</pre>
<p>In the latter cases, the <code>-v</code> option <em>validates</em> the input string
against various constraints.  The expression <code>mm/s</code> is completely valid
in all the syntaxes.  In the FITS syntax, the <code>erg</code> is a recognised
unit, but it is deprecated; although it is recognised, it is not
permitted to have SI prefixes.  In the CDS syntax, the <code>erg</code> is neither
recognised nor (a fortiori) recommended; since there are no
constraints on it in this syntax, it satisfies all of them (this
latter behaviour is admittedly slightly counterintuitive).</p>


<h2>Grammars supported</h2>

<p>The four supported grammars have a fair amount in common, but the
differences are nonetheless significant enough that they require
separate grammars.  Important differences are in the number of solidi
they allow in the units specifications, and the symbols they use for
products and powers.</p>

<p>Note that in each of the grammars here, the empty string is
<em>not</em> a valid units string – in particular, it is not taken to
indicate a dimensionless quantity.
If a particular context wishes to interpret such a string as indicating a
dimensionless quantity, or perhaps instead indicating ‘units unknown’,
then it should handle that case separately.
To obtain what would be the result of such a parse, use the
method {@link uk.me.nxg.unity.UnitExpr#getDimensionlessExpression}.
The VOUnits syntax, though it also deems the empty string to be
invalid, recognises the string <code>"1"</code> as indicating a dimensionless quantity.

<p>Current limitations:</p>
<ul>
<li>Currently ignores some of the odder unit restrictions (such as the OGIP requirement that 'Crab' can have a 'milli' prefix, but no other SI prefixes)</li>
</ul>

<p>In the grammars below, the common terminals are as follows:</p>
<ul>
<li>WHITESPACE: one or more whitespace characters
(in the grammars, whitespace is <em>not</em> permitted unless it
matches a WHITESPACE terminal)</li>
<li>STAR, DOT: a star or a dot, generally used to indicate multiplication</li>
<li>DIVISION: a slash</li>
<li>STARSTAR, CARET: the former is "**", the latter "^"; both are used to indicate exponentiation</li>
<li>OPEN_P, CLOSE_P: open and close parentheses</li>
<li>INTEGER, FLOAT: numbers; the syntax of FLOAT is
<code>[+-]?[1-9][0-9]*\.[0-9]+</code>
so that there are no exponents allowed;
the signed integers have a non-optional leading sign, the unsigned don't</li>
<li>STRING: a sequence of one or more upper- and lower-case ASCII letters, <code>[a-zA-Z]+</code></li>
<li>LIT10, LIT1: the literal strings "10" and "1"</li>
</ul>
<p>There are some other terminals used in some grammars.
See the VOUnits specification for further details.</p>

<h3>The FITS grammar</h3>

<pre>
input: complete_expression
        | scalefactor complete_expression
        | scalefactor WHITESPACE complete_expression
        | division unit_expression
        ;

complete_expression: product_of_units
        | product_of_units division unit_expression
        ;

product_of_units: unit_expression
        | product_of_units product unit_expression
        ;

unit_expression: term
        // m(2) is m^2, not function application
        | STRING parenthesized_number
        | function_application
        | OPEN_P complete_expression CLOSE_P
        ;

function_application: STRING OPEN_P complete_expression CLOSE_P ;

scalefactor: LIT10 power numeric_power
        | LIT10 SIGNED_INTEGER
        ;

division: DIVISION;

term: unit
        | unit numeric_power
        | unit power numeric_power
        ;

unit: STRING
        ;

power: CARET
        | STARSTAR
        ;

numeric_power: integer
        | parenthesized_number
        ;

parenthesized_number: OPEN_P integer CLOSE_P
        | OPEN_P FLOAT CLOSE_P
        | OPEN_P integer division UNSIGNED_INTEGER CLOSE_P
        ;

integer: SIGNED_INTEGER | UNSIGNED_INTEGER;

product: WHITESPACE | STAR | DOT;
</pre>

<h3>The OGIP grammar</h3>

<pre>
input: complete_expression
        | scalefactor complete_expression
        | scalefactor WHITESPACE complete_expression
        ;

complete_expression: product_of_units
        ;

product_of_units: unit_expression
        | division unit_expression
        | product_of_units product unit_expression
        | product_of_units division unit_expression
        ;

unit_expression: term
        | function_application
        | OPEN_P complete_expression CLOSE_P
        ;

function_application: STRING OPEN_P complete_expression CLOSE_P ;

scalefactor: LIT10 power numeric_power
        | LIT10
        | FLOAT
        ;

division: DIVISION | WHITESPACE DIVISION
        | WHITESPACE DIVISION WHITESPACE | DIVISION WHITESPACE;

term: unit
        | unit power numeric_power
        ;

unit: STRING
        ;

power: STARSTAR;

numeric_power: UNSIGNED_INTEGER
        | FLOAT
        | parenthesized_number
        ;

parenthesized_number: OPEN_P integer CLOSE_P
        | OPEN_P FLOAT CLOSE_P
        | OPEN_P integer division UNSIGNED_INTEGER CLOSE_P
        ;

integer: SIGNED_INTEGER | UNSIGNED_INTEGER;

product: WHITESPACE | STAR | WHITESPACE STAR
       | WHITESPACE STAR WHITESPACE | STAR WHITESPACE;
</pre>

<h3>The CDS grammar</h3>

<p>This is quite similar to the OGIP grammar, but with more
restrictions.</p>
<p>The <code>CDSFLOAT</code> terminal is a string matching the regular
expression
<code>[0-9]+\.[0-9]+x10[-+][0-9]+</code>
(that is, something resembling <code>1.5x10+11</code>).
The termainals <code>OPEN_SQ</code> and <code>CLOSE_SQ</code> are opening
and closing square brackets <code>[...]</code>.
</p>
<pre>
input: complete_expression
        | scalefactor complete_expression
        ;

complete_expression: product_of_units
        ;

product_of_units: unit_expression
        | division unit_expression
        | product_of_units product unit_expression
        | product_of_units division unit_expression
        ;

unit_expression: term
        | function_application
        | OPEN_P complete_expression CLOSE_P
        ;

function_application: OPEN_SQ complete_expression CLOSE_SQ
        ;

scalefactor: LIT10 power numeric_power
        | LIT10 SIGNED_INTEGER
        | UNSIGNED_INTEGER
        | LIT10
        | CDSFLOAT
        | FLOAT
        ;

division: DIVISION;

term: unit
        | unit numeric_power
        ;

unit: STRING
        | PERCENT
        ;

power: STARSTAR;

numeric_power: integer
        ;


integer: SIGNED_INTEGER | UNSIGNED_INTEGER;

product: DOT;
</pre>

<h3>The VOUnits grammar</h3>

<p>The <code>VOUFLOAT</code> and <code>QUOTED_STRING</code> features
are extensions beyond the other grammars.  These aside, this syntax is
a strict subset of the FITS and CDS grammars, in the sense that any
VOUnit unit string, without these extensions, is a valid FITS and CDS
string, too), and it is almost a subset of the OGIP grammar, except
that it uses the dot for multiplication rather than star.</p>
<p>The <code>VOUFLOAT</code> terminal is a string matching either of the
regular expressions
<code>0\.[0-9]+([eE][+-]?[0-9]+)?</code> or
<code>[1-9][0-9]*(\.[0-9]+)?([eE][+-]?[0-9]+)?</code>
(that is, something resembling, for example, <code>0.123</code> or
<code>1.5e+11</code>).  Also <code>QUOTED_STRING</code> is a
<code>STRING</code> enclosed in single quotes <code>'...'</code>.</p>
<pre>
input: complete_expression
        | scalefactor complete_expression
        | LIT1
        ;

complete_expression: product_of_units
        | product_of_units division unit_expression
        ;

product_of_units: unit_expression
        | product_of_units product unit_expression
        ;

unit_expression: term
        | function_application
        | OPEN_P complete_expression CLOSE_P
        ;

function_application: STRING OPEN_P  function_operand CLOSE_P ;
function_operand: complete_expression
        | scalefactor complete_expression ;

scalefactor: LIT10 power numeric_power
        | LIT10
        | LIT1
        | VOUFLOAT
        ;

division: DIVISION;

term: unit
        | unit power numeric_power
        ;

unit: STRING
        | QUOTED_STRING
        | STRING QUOTED_STRING
        | PERCENT
        ;

power: STARSTAR;

numeric_power: integer
        | parenthesized_number
        ;

parenthesized_number: OPEN_P integer CLOSE_P
        | OPEN_P FLOAT CLOSE_P
        | OPEN_P integer division UNSIGNED_INTEGER CLOSE_P
        ;

integer: SIGNED_INTEGER | UNSIGNED_INTEGER;

product: DOT;
</pre>


<h3>The lists of known units</h3>

Below are the lists of known units, taken from Pence et al, the OGIP
specification, and the CDS specification.

In the columns below, a <code>1</code> indicates that the unit is permitted,
<code>s</code> indicates that SI prefixes are allowed,
<code>b</code> that IEC binary prefixes are allowed,
<code>d</code> that the unit is deprecated in some way,
and <code>p</code> that the symbol is the preferred one in this syntax
(where there is more than one symbol that maps to this unit).
The CDS standard doesn’t indicate which units may or may not take SI prefixes:
in the table below, we generally follow the FITS prescription, except
where the CDS specification positively suggests otherwise.
Where there are two possible abbreviations for a unit in a syntax,
(eg FITS allows ‘pixel’ and ‘pix’), we prefer the one marked with a ‘p’.

<table>
<caption>The lists of known units</caption>
<tr><td>unit</td><td>meaning</td><td>FITS</td><td>OGIP</td><td>CDS</td><td>VOUnits</td></tr>
<tr><td>%</td><td>qudt:Percent</td><td></td><td></td><td>1</td><td>1</td></tr>
<tr><td>A</td><td>qudt:Ampere</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>a</td><td>unity:JulianYear</td><td>1ps</td><td></td><td>1s</td><td>1s</td></tr>
<tr><td>adu</td><td>unity:ADU</td><td>1</td><td></td><td></td><td>1s</td></tr>
<tr><td>Angstrom</td><td>qudt:Angstrom</td><td>1d</td><td></td><td>1</td><td>1dp</td></tr>
<tr><td>angstrom</td><td>qudt:Angstrom</td><td></td><td>1</td><td></td><td>1d</td></tr>
<tr><td>arcmin</td><td>qudt:ArcMinute</td><td>1</td><td>1</td><td>1</td><td>1s</td></tr>
<tr><td>arcsec</td><td>qudt:ArcSecond</td><td>1</td><td>1</td><td>1s</td><td>1s</td></tr>
<tr><td>AU</td><td>qudt:AstronomicalUnit</td><td>1</td><td>1</td><td>1</td><td>1p</td></tr>
<tr><td>au</td><td>qudt:AstronomicalUnit</td><td></td><td></td><td></td><td>1</td></tr>
<tr><td>Ba</td><td>unity:BesselianYear</td><td>1d</td><td></td><td></td><td>1d</td></tr>
<tr><td>barn</td><td>qudt:Barn</td><td>1sd</td><td>1</td><td>1s</td><td>1sd</td></tr>
<tr><td>beam</td><td>unity:Beam</td><td>1</td><td></td><td></td><td>1s</td></tr>
<tr><td>bin</td><td>unity:DistributionBin</td><td>1</td><td>1</td><td></td><td>1s</td></tr>
<tr><td>bit</td><td>qudt:Bit</td><td>1s</td><td></td><td>1s</td><td>1sb</td></tr>
<tr><td>byte</td><td>qudt:Byte</td><td>1s</td><td>1</td><td>1s</td><td>1sbp</td></tr>
<tr><td>B</td><td>qudt:Byte</td><td></td><td></td><td></td><td>1sb</td></tr>
<tr><td>C</td><td>qudt:Coulomb</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>cd</td><td>qudt:Candela</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>chan</td><td>unity:DetectorChannel</td><td>1</td><td>1</td><td></td><td>1s</td></tr>
<tr><td>count</td><td>qudt:Number</td><td>1</td><td>1</td><td></td><td>1sp</td></tr>
<tr><td>Crab</td><td>unity:Crab</td><td></td><td>1s</td><td></td><td></td></tr>
<tr><td>ct</td><td>qudt:Number</td><td>1</td><td></td><td>1</td><td>1s</td></tr>
<tr><td>cy</td><td>unity:JulianCentury</td><td>1</td><td></td><td></td><td></td></tr>
<tr><td>d</td><td>qudt:Day</td><td>1</td><td>1</td><td>1</td><td>1s</td></tr>
<tr><td>dB</td><td>qudt:Decibel</td><td></td><td></td><td></td><td>1</td></tr>
<tr><td>D</td><td>qudt:Debye</td><td>1</td><td></td><td>1</td><td>1s</td></tr>
<tr><td>deg</td><td>qudt:DegreeAngle</td><td>1</td><td>1</td><td>1</td><td>1s</td></tr>
<tr><td>erg</td><td>qudt:Erg</td><td>1d</td><td>1</td><td></td><td>1sd</td></tr>
<tr><td>eV</td><td>qudt:ElectronVolt</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>F</td><td>qudt:Farad</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>g</td><td>qudt:Gram</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>G</td><td>qudt:Gauss</td><td>1sd</td><td>1</td><td></td><td>1sd</td></tr>
<tr><td>H</td><td>qudt:Henry</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>h</td><td>qudt:Hour</td><td>1</td><td>1</td><td>1</td><td>1s</td></tr>
<tr><td>Hz</td><td>qudt:Hertz</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>J</td><td>qudt:Joule</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>Jy</td><td>unity:Jansky</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>K</td><td>qudt:Kelvin</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>lm</td><td>qudt:Lumen</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>lx</td><td>qudt:Lux</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>lyr</td><td>qudt:LightYear</td><td>1</td><td>1</td><td></td><td>1s</td></tr>
<tr><td>m</td><td>qudt:Meter</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>mag</td><td>unity:StellarMagnitude</td><td>1s</td><td>1</td><td>1s</td><td>1s</td></tr>
<tr><td>mas</td><td>unity:MilliArcSecond</td><td>1</td><td></td><td>1</td><td>1</td></tr>
<tr><td>min</td><td>qudt:MinuteTime</td><td>1</td><td>1</td><td>1</td><td>1s</td></tr>
<tr><td>mol</td><td>qudt:Mole</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>N</td><td>qudt:Newton</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>Ohm</td><td>qudt:Ohm</td><td>1s</td><td></td><td>1s</td><td>1s</td></tr>
<tr><td>ohm</td><td>qudt:Ohm</td><td></td><td>1s</td><td></td><td></td></tr>
<tr><td>Pa</td><td>qudt:Pascal</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>pc</td><td>qudt:Parsec</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>ph</td><td>unity:Photon</td><td>1</td><td></td><td></td><td>1s</td></tr>
<tr><td>photon</td><td>unity:Photon</td><td>1p</td><td>1</td><td></td><td>1sp</td></tr>
<tr><td>pix</td><td>unity:Pixel</td><td>1</td><td></td><td>1</td><td>1s</td></tr>
<tr><td>pixel</td><td>unity:Pixel</td><td>1p</td><td>1</td><td></td><td>1sp</td></tr>
<tr><td>R</td><td>unity:Rayleigh</td><td>1s</td><td></td><td></td><td>1s</td></tr>
<tr><td>rad</td><td>qudt:Radian</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>Ry</td><td>unity:Rydberg</td><td>1</td><td></td><td>1s</td><td>1s</td></tr>
<tr><td>s</td><td>qudt:SecondTime</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>S</td><td>qudt:Siemens</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>solLum</td><td>unity:SolarLuminosity</td><td>1</td><td></td><td>1</td><td>1s</td></tr>
<tr><td>solMass</td><td>unity:SolarMass</td><td>1</td><td></td><td>1</td><td>1s</td></tr>
<tr><td>solRad</td><td>unity:SolarRadius</td><td>1</td><td></td><td>1</td><td>1s</td></tr>
<tr><td>sr</td><td>qudt:Steradian</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>T</td><td>qudt:Tesla</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>ta</td><td>qudt:YearTropical</td><td>1d</td><td></td><td></td><td>1d</td></tr>
<tr><td>u</td><td>qudt:UnifiedAtomicMassUnit</td><td>1</td><td></td><td></td><td>1s</td></tr>
<tr><td>V</td><td>qudt:Volt</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>voxel</td><td>unity:Voxel</td><td>1</td><td>1</td><td></td><td>1s</td></tr>
<tr><td>W</td><td>qudt:Watt</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>Wb</td><td>qudt:Weber</td><td>1s</td><td>1s</td><td>1s</td><td>1s</td></tr>
<tr><td>yr</td><td>unity:JulianYear</td><td>1s</td><td>1</td><td>1sp</td><td>1sp</td></tr>
</table>

*/
package uk.me.nxg.unity;
