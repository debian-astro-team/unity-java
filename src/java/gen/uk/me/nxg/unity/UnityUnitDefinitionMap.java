/*
 * This is a GENERATED FILE -- do not edit.
 * See known-units.csv and buildtools/uk/me/nxg/unity/ParseUnits.java.
 */
package uk.me.nxg.unity;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

class UnityUnitDefinitionMap extends UnitDefinitionMap {
    private Map<Syntax,Map<String,UnitDefinition>> syntaxToUnitLookupMap = null;
    private Map<Syntax,Map<UnitDefinition,UnitRepresentation>> syntaxToUnitRepresentationMap = null;
    private Map<String,UnitDefinition> uriToUnitLookupMap = null;

    UnityUnitDefinitionMap() {
        super();
    }

    // This is essentially just m.put(u,newRep), except that if a mapping
    // already exists for u, we append or prepend the new one, depending on
    // whether isPreferred is false or true, respectively.
    private void mapUnitToRepresentation(Map<UnitDefinition,UnitRepresentation> m,
                                         UnitDefinition u,
                                         UnitRepresentation newRep,
                                         boolean isPreferred) {
        UnitRepresentation r = m.get(u);
        if (r == null) {
            // simple
            m.put(u, newRep);
        } else {
            if (isPreferred) {
                newRep.add(r);    // add the existing to the end of the new
                m.put(u, newRep); // replace the existing with the new
            } else {
                r.add(newRep);    // add the new to the end of the existing
            }
        }
    }

    @Override public Resolver getResolver(final Syntax syntax) {
        final Map<String,UnitDefinition> syntaxMap
                = syntaxToUnitLookupMap.get(syntax);
        final Map<UnitDefinition,UnitRepresentation> unitMap
                = syntaxToUnitRepresentationMap.get(syntax);
        if (syntaxMap == null || unitMap == null) {
            return null;
        } else {
            return new Resolver() {
                public UnitDefinition lookupSymbol(String symbol) {
                    return syntaxMap.get(symbol);
                }
                public UnitRepresentation lookupUnit(UnitDefinition unit) {
                    return unitMap.get(unit);
                }
                public Syntax getSyntax() {
                    return syntax;
                }
            };
        }
    }

    @Override public UnitDefinition lookupUnitDefinition(String uri) {
        return uriToUnitLookupMap.get(uri);
    }

    @Override UnitDefinitionMap initialize() {
        List<UnitDefinition> defList = new java.util.ArrayList<UnitDefinition>();

uriToUnitLookupMap = new HashMap<String,UnitDefinition>(96);
UnitDefinition ud;

ud = new UnitDefinition("http://qudt.org/vocab/unit#Byte",
    "Byte",
    "Byte",
    "http://qudt.org/vocab/quantity#InformationEntropy",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    "\\byte");
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Byte", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Newton",
    "Newton",
    "Newton",
    "http://qudt.org/vocab/quantity#Force",
    new Dimensions(new float[]{1.0f,1.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Newton", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Tesla",
    "Tesla",
    "Tesla",
    "http://qudt.org/vocab/quantity#MagneticField",
    new Dimensions(new float[]{0.0f,1.0f,-2.0f,-1.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Tesla", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Steradian",
    "Steradian",
    "Steradian",
    "http://qudt.org/vocab/quantity#SolidAngle",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "The steradian (symbol: sr) is the SI unit of solid angle. It is used to describe two-dimensional angular spans in three-dimensional space, analogous to the way in which the radian describes angles in a plane.",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Steradian", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Kelvin",
    "Kelvin",
    "Kelvin",
    "http://qudt.org/vocab/quantity#ThermodynamicTemperature",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Kelvin", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Lumen",
    "Lumen",
    "Lumen",
    "http://qudt.org/vocab/quantity#LuminousFlux",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,1.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Lumen", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#SolarRadius",
    "solRad",
    "SolarRadius",
    "http://qudt.org/vocab/quantity#Length",
    new Dimensions(new float[]{1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "6.9599e8 m",
    "R\\solar");
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#SolarRadius", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#MinuteTime",
    "Minute Time",
    "MinuteTime",
    "http://qudt.org/vocab/quantity#Time",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#MinuteTime", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#DegreeAngle",
    "Degree Angle",
    "DegreeAngle",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#DegreeAngle", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#Crab",
    "Crab",
    "Crab",
    "http://bitbucket/org/nxg/unity/ns/quantity#SpecificIntensity",
    new Dimensions(new float[]{0.0f,1.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    "The flux density, relative to that of the Crab.\nMentioned in the OGIP standard, but strongly disrecommended.\nThe OGIP standard permits only 'm' as a prefix.",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#Crab", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel",
    "chan",
    "DetectorChannel",
    "http://qudt.org/vocab/quantity#Dimensionless",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "One channel in a detector of some type",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Volt",
    "Volt",
    "Volt",
    "http://qudt.org/vocab/quantity#EnergyPerElectricCharge",
    new Dimensions(new float[]{2.0f,1.0f,-3.0f,-1.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Volt", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#ADU",
    "ADU",
    "ADU",
    "http://qudt.org/vocab/quantity#Dimensionless",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "Same as channel???",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#ADU", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Erg",
    "Erg",
    "Erg",
    "http://qudt.org/vocab/quantity#EnergyAndWork",
    new Dimensions(new float[]{2.0f,1.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Erg", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Pascal",
    "Pascal",
    "Pascal",
    "http://qudt.org/vocab/quantity#ForcePerArea",
    new Dimensions(new float[]{-1.0f,1.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Pascal", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#AstronomicalUnit",
    "Astronomical Unit",
    "AstronomicalUnit",
    "http://qudt.org/vocab/quantity#Length",
    new Dimensions(new float[]{1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#AstronomicalUnit", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude",
    "mag",
    "StellarMagnitude",
    "http://bitbucket/org/nxg/unity/ns/quantity#StellarMagnitudeQuantityKind",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "Log of a ratio of intensities.  See http://en.wikipedia.org/wiki/Magnitude_(astronomy)",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Siemens",
    "Siemens",
    "Siemens",
    "http://qudt.org/vocab/quantity#ElectricConductivity",
    new Dimensions(new float[]{-2.0f,-1.0f,3.0f,2.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Siemens", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#Jansky",
    "Jy",
    "Jansky",
    "http://bitbucket/org/nxg/unity/ns/quantity#SpecificIntensity",
    new Dimensions(new float[]{0.0f,1.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    "10^-26 W m-2 Hz-1",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#Jansky", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#LightYear",
    "Light Year",
    "LightYear",
    "http://qudt.org/vocab/quantity#Length",
    new Dimensions(new float[]{1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "A unit of length defining the distance, in meters, that light travels in a vacuum in one year.",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#LightYear", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#SecondTime",
    "Second",
    "SecondTime",
    "http://qudt.org/vocab/quantity#Time",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#SecondTime", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Gauss",
    "Gauss",
    "Gauss",
    "http://qudt.org/vocab/quantity#MagneticField",
    new Dimensions(new float[]{0.0f,1.0f,-2.0f,-1.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Gauss", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond",
    "mas",
    "MilliArcSecond",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "1/1000 of an arc-second, for use in those syntaxes which do not allow SI prefixes of arcsec",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Meter",
    "Metre",
    "Meter",
    "http://qudt.org/vocab/quantity#Length",
    new Dimensions(new float[]{1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Meter", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Coulomb",
    "Coulomb",
    "Coulomb",
    "http://qudt.org/vocab/quantity#ElectricCharge",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,1.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Coulomb", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#Rydberg",
    "Ry",
    "Rydberg",
    "http://qudt.org/vocab/quantity#EnergyAndWork",
    new Dimensions(new float[]{2.0f,1.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    "Half of the Hartree energy, defined as 13.605692 eV.  See http://en.wikipedia.org/wiki/Hartree",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#Rydberg", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Decibel",
    "Decibel",
    "Decibel",
    "http://qudt.org/vocab/quantity#SignalStrength",
    new Dimensions(new float[]{1.0f,1.0f,-3.0f,-1.0f,0.0f,0.0f,0.0f,}),
    null,
    "\\decibel");
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Decibel", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Hertz",
    "Hertz",
    "Hertz",
    "http://qudt.org/vocab/quantity#Frequency",
    new Dimensions(new float[]{0.0f,0.0f,-1.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Hertz", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Mole",
    "Mole",
    "Mole",
    "http://qudt.org/vocab/quantity#AmountOfSubstance",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Mole", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Farad",
    "Farad",
    "Farad",
    "http://qudt.org/vocab/quantity#Capacitance",
    new Dimensions(new float[]{-2.0f,-1.0f,4.0f,2.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Farad", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#BesselianYear",
    "Bessellian Year",
    "BesselianYear",
    "http://qudt.org/vocab/quantity#Time",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,}),
    "The Besselian Year.\nSee the discussion in FITS WCS paper IV (section 4.2), and FITS v4.0 (section 9.3)\nfor important caveats concerning the use of the Besselian and Tropical years.",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#BesselianYear", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#Voxel",
    "voxel",
    "Voxel",
    "http://qudt.org/vocab/quantity#Dimensionless",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "The 3-D analogue of a pixel",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#Voxel", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#Pixel",
    "pixel",
    "Pixel",
    "http://qudt.org/vocab/quantity#Dimensionless",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "an element of a 2-D image",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#Pixel", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Joule",
    "Joule",
    "Joule",
    "http://qudt.org/vocab/quantity#EnergyAndWork",
    new Dimensions(new float[]{2.0f,1.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Joule", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Number",
    "Number",
    "Number",
    "http://qudt.org/vocab/quantity#Dimensionless",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Number", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Watt",
    "Watt",
    "Watt",
    "http://qudt.org/vocab/quantity#Power",
    new Dimensions(new float[]{2.0f,1.0f,-3.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Watt", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Lux",
    "Lux",
    "Lux",
    "http://qudt.org/vocab/quantity#LuminousFluxPerArea",
    new Dimensions(new float[]{-2.0f,0.0f,0.0f,0.0f,0.0f,0.0f,1.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Lux", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#ArcMinute",
    "Arc Minute",
    "ArcMinute",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#ArcMinute", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Gram",
    "Gramme",
    "Gram",
    "http://qudt.org/vocab/quantity#Mass",
    new Dimensions(new float[]{0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Gram", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Bit",
    "Bit",
    "Bit",
    "http://qudt.org/vocab/quantity#InformationEntropy",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "In information theory, a bit is the amount of information that, on average, can be stored in a discrete bit. It is thus the amount of information carried by a choice between two equally likely outcomes. One bit corresponds to about 0.693 nats (ln(2)), or 0.301 hartleys (log10(2)).",
    "\\bit");
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Bit", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Weber",
    "Weber",
    "Weber",
    "http://qudt.org/vocab/quantity#MagneticFlux",
    new Dimensions(new float[]{2.0f,1.0f,-2.0f,-1.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Weber", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#JulianCentury",
    "Julian Century",
    "JulianCentury",
    "http://qudt.org/vocab/quantity#Time",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,}),
    "One hundred Julian Years",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#JulianCentury", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Barn",
    "Barn",
    "Barn",
    "http://qudt.org/vocab/quantity#Area",
    new Dimensions(new float[]{2.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Barn", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#Photon",
    "ph",
    "Photon",
    "http://qudt.org/vocab/quantity#Dimensionless",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "as in ev/photon",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#Photon", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Candela",
    "Candela",
    "Candela",
    "http://qudt.org/vocab/quantity#LuminousIntensity",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,1.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Candela", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Percent",
    "Percent",
    "Percent",
    "http://qudt.org/vocab/quantity#DimensionlessRatio",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    "\\%");
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Percent", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#DistributionBin",
    "bin",
    "DistributionBin",
    "http://qudt.org/vocab/quantity#Dimensionless",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "One division within a distribution",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#DistributionBin", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#SolarMass",
    "Solar mass",
    "SolarMass",
    "http://qudt.org/vocab/quantity#Mass",
    new Dimensions(new float[]{0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "The mass of the sun is 1.9891e30 kg",
    "M\\solar");
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#SolarMass", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Angstrom",
    "Angstrom",
    "Angstrom",
    "http://qudt.org/vocab/quantity#Length",
    new Dimensions(new float[]{1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "The ångström or angstrom (symbol Å) (the latter spelling, without diacritics, is now usually used in English) (pronounced /ˈæŋstrəm/; Swedish: [ˈɔŋstrøm]) is an internationally recognized unit of length equal to 0.1 nanometre or 1×10−10 metres. It is named after Anders Jonas Ångström. Although accepted for use, it is not formally defined within the International System of Units(SI). (That article lists the units that are so defined.) The ångström is often used in the natural sciences to express the sizes of atoms, lengths of chemical bonds and the wavelengths of electromagnetic radiation, and in technology for the dimensions of parts of integrated circuits. It is also commonly used in structural biology. [Wikipedia]",
    "\\angstrom");
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Angstrom", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Day",
    "Day",
    "Day",
    "http://qudt.org/vocab/quantity#Time",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,}),
    "Mean solar day",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Day", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Radian",
    "Radian",
    "Radian",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "The radian is the standard unit of angular measure, used in many areas of mathematics. It describes the plane angle subtended by a circular arc as the length of the arc divided by the radius of the arc. The unit was formerly a SI supplementary unit, but this category was abolished in 1995 and the radian is now considered a SI derived unit. The SI unit of solid angle measurement is the steradian.\nThe radian is represented by the symbol \"rad\" or, more rarely, by the superscript c (for \"circular measure\"). For example, an angle of 1.2 radians would be written as \"1.2 rad\" or \"1.2c\" (the second symbol is often mistaken for a degree: \"1.2°\"). As the ratio of two lengths, the radian is a \"pure number\" that needs no unit symbol, and in mathematical writing the symbol \"rad\" is almost always omitted. In the absence of any symbol radians are assumed, and when degrees are meant the symbol ° is used. [Wikipedia]",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Radian", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#Beam",
    "beam",
    "Beam",
    "http://qudt.org/vocab/quantity#Dimensionless",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "as in Jy/beam",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#Beam", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Ohm",
    "Ohm",
    "Ohm",
    "http://qudt.org/vocab/quantity#Resistance",
    new Dimensions(new float[]{2.0f,1.0f,-3.0f,-2.0f,0.0f,0.0f,0.0f,}),
    null,
    "\\ohm");
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Ohm", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#YearTropical",
    "Year Tropical",
    "YearTropical",
    "http://qudt.org/vocab/quantity#Time",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#YearTropical", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#JulianYear",
    "Julian year",
    "JulianYear",
    "http://qudt.org/vocab/quantity#Time",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,}),
    "31 557 600s (365.25d), peta a (Pa) forbidden",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#JulianYear", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Ampere",
    "Ampere",
    "Ampere",
    "http://qudt.org/vocab/quantity#ElectricCurrent",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Ampere", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#ElectronVolt",
    "Electron Volt",
    "ElectronVolt",
    "http://qudt.org/vocab/quantity#EnergyAndWork",
    new Dimensions(new float[]{2.0f,1.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#ElectronVolt", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Debye",
    "Debye",
    "Debye",
    "http://qudt.org/vocab/quantity#ElectricDipoleMoment",
    new Dimensions(new float[]{1.0f,0.0f,1.0f,1.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Debye", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#ArcSecond",
    "Arc Second",
    "ArcSecond",
    "http://qudt.org/vocab/quantity#PlaneAngle",
    new Dimensions(new float[]{0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#ArcSecond", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Parsec",
    "Parsec",
    "Parsec",
    "http://qudt.org/vocab/quantity#Length",
    new Dimensions(new float[]{1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    "The parsec (parallax of one arcsecond; symbol: pc) is a unit of length, equal to just under 31 trillion (31×1012) kilometres (about 19 trillion miles), 206265 AU, or about 3.26 light-years. The parsec measurement unit is used in astronomy. It is defined as the length of the adjacent side of an imaginary right triangle in space. The two dimensions that specify this triangle are the parallax angle (defined as 1 arcsecond) and the opposite side (defined as 1 astronomical unit (AU), the distance from the Earth to the Sun). Given these two measurements, along with the rules of trigonometry, the length of the adjacent side (the parsec) can be found. [Wikipedia]",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Parsec", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#Rayleigh",
    "R",
    "Rayleigh",
    "http://bitbucket.org/nxg/unity/ns/unit#PhotonFlux",
    new Dimensions(new float[]{-2.0f,0.0f,-2.0f,0.0f,0.0f,0.0f,0.0f,}),
    "Unit of photon flux density: Wikipedia has units of 10^10 photons/m2/column/s.\nThe FITS spec has units of 1e10/(4PI) photons m-2 s-2 sr-1.\nSee http://en.wikipedia.org/wiki/Rayleigh_(unit).\nNo, I don't know how to express its dimensions in the QUDT system!\nWe go with the FITS definition here.",
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#Rayleigh", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Henry",
    "Henry",
    "Henry",
    "http://qudt.org/vocab/quantity#Inductance",
    new Dimensions(new float[]{2.0f,1.0f,-2.0f,-2.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Henry", ud);

ud = new UnitDefinition("http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity",
    "solLum",
    "SolarLuminosity",
    "http://qudt.org/vocab/quantity#Power",
    new Dimensions(new float[]{2.0f,1.0f,-3.0f,0.0f,0.0f,0.0f,0.0f,}),
    "3.8268e26 W",
    "L\\solar");
defList.add(ud);
uriToUnitLookupMap.put("http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#UnifiedAtomicMassUnit",
    "Unified Atomic Mass Unit",
    "UnifiedAtomicMassUnit",
    "http://qudt.org/vocab/quantity#Mass",
    new Dimensions(new float[]{0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#UnifiedAtomicMassUnit", ud);

ud = new UnitDefinition("http://qudt.org/vocab/unit#Hour",
    "Hour",
    "Hour",
    "http://qudt.org/vocab/quantity#Time",
    new Dimensions(new float[]{0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,}),
    null,
    null);
defList.add(ud);
uriToUnitLookupMap.put("http://qudt.org/vocab/unit#Hour", ud);



// Reverse lookups, per-syntax maps going from symbols to UnitDefinitions
syntaxToUnitLookupMap = new HashMap<Syntax,Map<String,UnitDefinition>>(8);
syntaxToUnitRepresentationMap = new HashMap<Syntax,Map<UnitDefinition,UnitRepresentation>>(8);
Map<String,UnitDefinition> symbolToUnitLookup;
Map<UnitDefinition,UnitRepresentation> unitToRepresentationLookup;


// Lookup table for syntax cds
symbolToUnitLookup = new HashMap<String,UnitDefinition>();
unitToRepresentationLookup = new HashMap<UnitDefinition,UnitRepresentation>();
ud = defList.get(5); // http://qudt.org/vocab/unit#Lumen
symbolToUnitLookup.put("lm", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lm"), false);
ud = defList.get(8); // http://qudt.org/vocab/unit#DegreeAngle
symbolToUnitLookup.put("deg", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "deg"), false);
ud = defList.get(28); // http://qudt.org/vocab/unit#Mole
symbolToUnitLookup.put("mol", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "mol"), false);
ud = defList.get(39); // http://qudt.org/vocab/unit#Bit
symbolToUnitLookup.put("bit", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "bit"), false);
ud = defList.get(36); // http://qudt.org/vocab/unit#Lux
symbolToUnitLookup.put("lx", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lx"), false);
ud = defList.get(14); // http://qudt.org/vocab/unit#Pascal
symbolToUnitLookup.put("Pa", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Pa"), false);
ud = defList.get(56); // http://qudt.org/vocab/unit#ElectronVolt
symbolToUnitLookup.put("eV", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "eV"), false);
ud = defList.get(50); // http://qudt.org/vocab/unit#Radian
symbolToUnitLookup.put("rad", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "rad"), false);
ud = defList.get(52); // http://qudt.org/vocab/unit#Ohm
symbolToUnitLookup.put("Ohm", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Ohm"), false);
ud = defList.get(42); // http://qudt.org/vocab/unit#Barn
symbolToUnitLookup.put("barn", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "barn"), false);
ud = defList.get(54); // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
symbolToUnitLookup.put("yr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "yr"), true);
ud = defList.get(32); // http://bitbucket.org/nxg/unity/ns/unit#Pixel
symbolToUnitLookup.put("pix", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "pix"), false);
ud = defList.get(45); // http://qudt.org/vocab/unit#Percent
symbolToUnitLookup.put("%", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "%"), false);
ud = defList.get(0); // http://qudt.org/vocab/unit#Byte
symbolToUnitLookup.put("byte", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "byte"), false);
ud = defList.get(27); // http://qudt.org/vocab/unit#Hertz
symbolToUnitLookup.put("Hz", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Hz"), false);
ud = defList.get(15); // http://qudt.org/vocab/unit#AstronomicalUnit
symbolToUnitLookup.put("AU", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "AU"), false);
ud = defList.get(37); // http://qudt.org/vocab/unit#ArcMinute
symbolToUnitLookup.put("arcmin", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "arcmin"), false);
ud = defList.get(48); // http://qudt.org/vocab/unit#Angstrom
symbolToUnitLookup.put("Angstrom", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "Angstrom"), false);
ud = defList.get(55); // http://qudt.org/vocab/unit#Ampere
symbolToUnitLookup.put("A", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "A"), false);
ud = defList.get(24); // http://qudt.org/vocab/unit#Coulomb
symbolToUnitLookup.put("C", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "C"), false);
ud = defList.get(57); // http://qudt.org/vocab/unit#Debye
symbolToUnitLookup.put("D", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "D"), false);
ud = defList.get(58); // http://qudt.org/vocab/unit#ArcSecond
symbolToUnitLookup.put("arcsec", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "arcsec"), false);
ud = defList.get(29); // http://qudt.org/vocab/unit#Farad
symbolToUnitLookup.put("F", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "F"), false);
ud = defList.get(61); // http://qudt.org/vocab/unit#Henry
symbolToUnitLookup.put("H", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "H"), false);
ud = defList.get(47); // http://bitbucket.org/nxg/unity/ns/unit#SolarMass
symbolToUnitLookup.put("solMass", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "solMass"), false);
ud = defList.get(33); // http://qudt.org/vocab/unit#Joule
symbolToUnitLookup.put("J", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "J"), false);
ud = defList.get(4); // http://qudt.org/vocab/unit#Kelvin
symbolToUnitLookup.put("K", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "K"), false);
ud = defList.get(1); // http://qudt.org/vocab/unit#Newton
symbolToUnitLookup.put("N", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "N"), false);
ud = defList.get(16); // http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude
symbolToUnitLookup.put("mag", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "mag"), false);
ud = defList.get(17); // http://qudt.org/vocab/unit#Siemens
symbolToUnitLookup.put("S", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "S"), false);
ud = defList.get(7); // http://qudt.org/vocab/unit#MinuteTime
symbolToUnitLookup.put("min", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "min"), false);
ud = defList.get(2); // http://qudt.org/vocab/unit#Tesla
symbolToUnitLookup.put("T", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "T"), false);
ud = defList.get(11); // http://qudt.org/vocab/unit#Volt
symbolToUnitLookup.put("V", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "V"), false);
ud = defList.get(35); // http://qudt.org/vocab/unit#Watt
symbolToUnitLookup.put("W", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "W"), false);
ud = defList.get(22); // http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond
symbolToUnitLookup.put("mas", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "mas"), false);
ud = defList.get(62); // http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity
symbolToUnitLookup.put("solLum", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "solLum"), false);
ud = defList.get(3); // http://qudt.org/vocab/unit#Steradian
symbolToUnitLookup.put("sr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "sr"), false);
ud = defList.get(54); // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
symbolToUnitLookup.put("a", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "a"), false);
ud = defList.get(44); // http://qudt.org/vocab/unit#Candela
symbolToUnitLookup.put("cd", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "cd"), false);
ud = defList.get(49); // http://qudt.org/vocab/unit#Day
symbolToUnitLookup.put("d", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "d"), false);
ud = defList.get(25); // http://bitbucket.org/nxg/unity/ns/unit#Rydberg
symbolToUnitLookup.put("Ry", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Ry"), false);
ud = defList.get(38); // http://qudt.org/vocab/unit#Gram
symbolToUnitLookup.put("g", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "g"), false);
ud = defList.get(64); // http://qudt.org/vocab/unit#Hour
symbolToUnitLookup.put("h", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "h"), false);
ud = defList.get(40); // http://qudt.org/vocab/unit#Weber
symbolToUnitLookup.put("Wb", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Wb"), false);
ud = defList.get(23); // http://qudt.org/vocab/unit#Meter
symbolToUnitLookup.put("m", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "m"), false);
ud = defList.get(6); // http://bitbucket.org/nxg/unity/ns/unit#SolarRadius
symbolToUnitLookup.put("solRad", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "solRad"), false);
ud = defList.get(18); // http://bitbucket.org/nxg/unity/ns/unit#Jansky
symbolToUnitLookup.put("Jy", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Jy"), false);
ud = defList.get(34); // http://qudt.org/vocab/unit#Number
symbolToUnitLookup.put("ct", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "ct"), false);
ud = defList.get(59); // http://qudt.org/vocab/unit#Parsec
symbolToUnitLookup.put("pc", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "pc"), false);
ud = defList.get(20); // http://qudt.org/vocab/unit#SecondTime
symbolToUnitLookup.put("s", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "s"), false);
syntaxToUnitLookupMap.put(Syntax.CDS, symbolToUnitLookup);
syntaxToUnitRepresentationMap.put(Syntax.CDS, unitToRepresentationLookup);

// Lookup table for syntax ogip
symbolToUnitLookup = new HashMap<String,UnitDefinition>();
unitToRepresentationLookup = new HashMap<UnitDefinition,UnitRepresentation>();
ud = defList.get(5); // http://qudt.org/vocab/unit#Lumen
symbolToUnitLookup.put("lm", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lm"), false);
ud = defList.get(19); // http://qudt.org/vocab/unit#LightYear
symbolToUnitLookup.put("lyr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "lyr"), false);
ud = defList.get(46); // http://bitbucket.org/nxg/unity/ns/unit#DistributionBin
symbolToUnitLookup.put("bin", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "bin"), false);
ud = defList.get(8); // http://qudt.org/vocab/unit#DegreeAngle
symbolToUnitLookup.put("deg", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "deg"), false);
ud = defList.get(28); // http://qudt.org/vocab/unit#Mole
symbolToUnitLookup.put("mol", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "mol"), false);
ud = defList.get(36); // http://qudt.org/vocab/unit#Lux
symbolToUnitLookup.put("lx", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lx"), false);
ud = defList.get(14); // http://qudt.org/vocab/unit#Pascal
symbolToUnitLookup.put("Pa", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Pa"), false);
ud = defList.get(56); // http://qudt.org/vocab/unit#ElectronVolt
symbolToUnitLookup.put("eV", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "eV"), false);
ud = defList.get(50); // http://qudt.org/vocab/unit#Radian
symbolToUnitLookup.put("rad", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "rad"), false);
ud = defList.get(42); // http://qudt.org/vocab/unit#Barn
symbolToUnitLookup.put("barn", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "barn"), false);
ud = defList.get(54); // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
symbolToUnitLookup.put("yr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "yr"), false);
ud = defList.get(0); // http://qudt.org/vocab/unit#Byte
symbolToUnitLookup.put("byte", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "byte"), false);
ud = defList.get(34); // http://qudt.org/vocab/unit#Number
symbolToUnitLookup.put("count", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "count"), false);
ud = defList.get(27); // http://qudt.org/vocab/unit#Hertz
symbolToUnitLookup.put("Hz", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Hz"), false);
ud = defList.get(15); // http://qudt.org/vocab/unit#AstronomicalUnit
symbolToUnitLookup.put("AU", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "AU"), false);
ud = defList.get(52); // http://qudt.org/vocab/unit#Ohm
symbolToUnitLookup.put("ohm", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "ohm"), false);
ud = defList.get(48); // http://qudt.org/vocab/unit#Angstrom
symbolToUnitLookup.put("angstrom", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "angstrom"), false);
ud = defList.get(37); // http://qudt.org/vocab/unit#ArcMinute
symbolToUnitLookup.put("arcmin", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "arcmin"), false);
ud = defList.get(55); // http://qudt.org/vocab/unit#Ampere
symbolToUnitLookup.put("A", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "A"), false);
ud = defList.get(24); // http://qudt.org/vocab/unit#Coulomb
symbolToUnitLookup.put("C", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "C"), false);
ud = defList.get(58); // http://qudt.org/vocab/unit#ArcSecond
symbolToUnitLookup.put("arcsec", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "arcsec"), false);
ud = defList.get(29); // http://qudt.org/vocab/unit#Farad
symbolToUnitLookup.put("F", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "F"), false);
ud = defList.get(21); // http://qudt.org/vocab/unit#Gauss
symbolToUnitLookup.put("G", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "G"), false);
ud = defList.get(61); // http://qudt.org/vocab/unit#Henry
symbolToUnitLookup.put("H", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "H"), false);
ud = defList.get(33); // http://qudt.org/vocab/unit#Joule
symbolToUnitLookup.put("J", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "J"), false);
ud = defList.get(4); // http://qudt.org/vocab/unit#Kelvin
symbolToUnitLookup.put("K", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "K"), false);
ud = defList.get(1); // http://qudt.org/vocab/unit#Newton
symbolToUnitLookup.put("N", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "N"), false);
ud = defList.get(9); // http://bitbucket.org/nxg/unity/ns/unit#Crab
symbolToUnitLookup.put("Crab", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Crab"), false);
ud = defList.get(16); // http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude
symbolToUnitLookup.put("mag", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "mag"), false);
ud = defList.get(17); // http://qudt.org/vocab/unit#Siemens
symbolToUnitLookup.put("S", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "S"), false);
ud = defList.get(7); // http://qudt.org/vocab/unit#MinuteTime
symbolToUnitLookup.put("min", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "min"), false);
ud = defList.get(2); // http://qudt.org/vocab/unit#Tesla
symbolToUnitLookup.put("T", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "T"), false);
ud = defList.get(11); // http://qudt.org/vocab/unit#Volt
symbolToUnitLookup.put("V", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "V"), false);
ud = defList.get(35); // http://qudt.org/vocab/unit#Watt
symbolToUnitLookup.put("W", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "W"), false);
ud = defList.get(13); // http://qudt.org/vocab/unit#Erg
symbolToUnitLookup.put("erg", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "erg"), false);
ud = defList.get(32); // http://bitbucket.org/nxg/unity/ns/unit#Pixel
symbolToUnitLookup.put("pixel", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "pixel"), false);
ud = defList.get(3); // http://qudt.org/vocab/unit#Steradian
symbolToUnitLookup.put("sr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "sr"), false);
ud = defList.get(44); // http://qudt.org/vocab/unit#Candela
symbolToUnitLookup.put("cd", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "cd"), false);
ud = defList.get(49); // http://qudt.org/vocab/unit#Day
symbolToUnitLookup.put("d", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "d"), false);
ud = defList.get(38); // http://qudt.org/vocab/unit#Gram
symbolToUnitLookup.put("g", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "g"), false);
ud = defList.get(64); // http://qudt.org/vocab/unit#Hour
symbolToUnitLookup.put("h", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "h"), false);
ud = defList.get(40); // http://qudt.org/vocab/unit#Weber
symbolToUnitLookup.put("Wb", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Wb"), false);
ud = defList.get(23); // http://qudt.org/vocab/unit#Meter
symbolToUnitLookup.put("m", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "m"), false);
ud = defList.get(18); // http://bitbucket.org/nxg/unity/ns/unit#Jansky
symbolToUnitLookup.put("Jy", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Jy"), false);
ud = defList.get(43); // http://bitbucket.org/nxg/unity/ns/unit#Photon
symbolToUnitLookup.put("photon", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "photon"), false);
ud = defList.get(59); // http://qudt.org/vocab/unit#Parsec
symbolToUnitLookup.put("pc", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "pc"), false);
ud = defList.get(20); // http://qudt.org/vocab/unit#SecondTime
symbolToUnitLookup.put("s", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "s"), false);
ud = defList.get(31); // http://bitbucket.org/nxg/unity/ns/unit#Voxel
symbolToUnitLookup.put("voxel", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "voxel"), false);
ud = defList.get(10); // http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel
symbolToUnitLookup.put("chan", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "chan"), false);
syntaxToUnitLookupMap.put(Syntax.OGIP, symbolToUnitLookup);
syntaxToUnitRepresentationMap.put(Syntax.OGIP, unitToRepresentationLookup);

// Lookup table for syntax vounits
symbolToUnitLookup = new HashMap<String,UnitDefinition>();
unitToRepresentationLookup = new HashMap<UnitDefinition,UnitRepresentation>();
ud = defList.get(51); // http://bitbucket.org/nxg/unity/ns/unit#Beam
symbolToUnitLookup.put("beam", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "beam"), false);
ud = defList.get(5); // http://qudt.org/vocab/unit#Lumen
symbolToUnitLookup.put("lm", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lm"), false);
ud = defList.get(19); // http://qudt.org/vocab/unit#LightYear
symbolToUnitLookup.put("lyr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lyr"), false);
ud = defList.get(46); // http://bitbucket.org/nxg/unity/ns/unit#DistributionBin
symbolToUnitLookup.put("bin", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "bin"), false);
ud = defList.get(8); // http://qudt.org/vocab/unit#DegreeAngle
symbolToUnitLookup.put("deg", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "deg"), false);
ud = defList.get(28); // http://qudt.org/vocab/unit#Mole
symbolToUnitLookup.put("mol", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "mol"), false);
ud = defList.get(39); // http://qudt.org/vocab/unit#Bit
symbolToUnitLookup.put("bit", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, true, false, "bit"), false);
ud = defList.get(36); // http://qudt.org/vocab/unit#Lux
symbolToUnitLookup.put("lx", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lx"), false);
ud = defList.get(14); // http://qudt.org/vocab/unit#Pascal
symbolToUnitLookup.put("Pa", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Pa"), false);
ud = defList.get(56); // http://qudt.org/vocab/unit#ElectronVolt
symbolToUnitLookup.put("eV", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "eV"), false);
ud = defList.get(50); // http://qudt.org/vocab/unit#Radian
symbolToUnitLookup.put("rad", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "rad"), false);
ud = defList.get(52); // http://qudt.org/vocab/unit#Ohm
symbolToUnitLookup.put("Ohm", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Ohm"), false);
ud = defList.get(42); // http://qudt.org/vocab/unit#Barn
symbolToUnitLookup.put("barn", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, true, "barn"), false);
ud = defList.get(54); // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
symbolToUnitLookup.put("yr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "yr"), true);
ud = defList.get(32); // http://bitbucket.org/nxg/unity/ns/unit#Pixel
symbolToUnitLookup.put("pix", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "pix"), false);
ud = defList.get(45); // http://qudt.org/vocab/unit#Percent
symbolToUnitLookup.put("%", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "%"), false);
ud = defList.get(0); // http://qudt.org/vocab/unit#Byte
symbolToUnitLookup.put("byte", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, true, false, "byte"), true);
ud = defList.get(34); // http://qudt.org/vocab/unit#Number
symbolToUnitLookup.put("count", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "count"), true);
ud = defList.get(27); // http://qudt.org/vocab/unit#Hertz
symbolToUnitLookup.put("Hz", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Hz"), false);
ud = defList.get(12); // http://bitbucket.org/nxg/unity/ns/unit#ADU
symbolToUnitLookup.put("adu", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "adu"), false);
ud = defList.get(15); // http://qudt.org/vocab/unit#AstronomicalUnit
symbolToUnitLookup.put("AU", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "AU"), true);
ud = defList.get(15); // http://qudt.org/vocab/unit#AstronomicalUnit
symbolToUnitLookup.put("au", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "au"), false);
ud = defList.get(48); // http://qudt.org/vocab/unit#Angstrom
symbolToUnitLookup.put("angstrom", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, true, "angstrom"), false);
ud = defList.get(37); // http://qudt.org/vocab/unit#ArcMinute
symbolToUnitLookup.put("arcmin", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "arcmin"), false);
ud = defList.get(48); // http://qudt.org/vocab/unit#Angstrom
symbolToUnitLookup.put("Angstrom", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, true, "Angstrom"), true);
ud = defList.get(55); // http://qudt.org/vocab/unit#Ampere
symbolToUnitLookup.put("A", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "A"), false);
ud = defList.get(0); // http://qudt.org/vocab/unit#Byte
symbolToUnitLookup.put("B", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, true, false, "B"), false);
ud = defList.get(24); // http://qudt.org/vocab/unit#Coulomb
symbolToUnitLookup.put("C", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "C"), false);
ud = defList.get(57); // http://qudt.org/vocab/unit#Debye
symbolToUnitLookup.put("D", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "D"), false);
ud = defList.get(58); // http://qudt.org/vocab/unit#ArcSecond
symbolToUnitLookup.put("arcsec", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "arcsec"), false);
ud = defList.get(29); // http://qudt.org/vocab/unit#Farad
symbolToUnitLookup.put("F", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "F"), false);
ud = defList.get(21); // http://qudt.org/vocab/unit#Gauss
symbolToUnitLookup.put("G", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, true, "G"), false);
ud = defList.get(61); // http://qudt.org/vocab/unit#Henry
symbolToUnitLookup.put("H", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "H"), false);
ud = defList.get(47); // http://bitbucket.org/nxg/unity/ns/unit#SolarMass
symbolToUnitLookup.put("solMass", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "solMass"), false);
ud = defList.get(33); // http://qudt.org/vocab/unit#Joule
symbolToUnitLookup.put("J", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "J"), false);
ud = defList.get(4); // http://qudt.org/vocab/unit#Kelvin
symbolToUnitLookup.put("K", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "K"), false);
ud = defList.get(1); // http://qudt.org/vocab/unit#Newton
symbolToUnitLookup.put("N", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "N"), false);
ud = defList.get(60); // http://bitbucket.org/nxg/unity/ns/unit#Rayleigh
symbolToUnitLookup.put("R", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "R"), false);
ud = defList.get(16); // http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude
symbolToUnitLookup.put("mag", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "mag"), false);
ud = defList.get(17); // http://qudt.org/vocab/unit#Siemens
symbolToUnitLookup.put("S", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "S"), false);
ud = defList.get(7); // http://qudt.org/vocab/unit#MinuteTime
symbolToUnitLookup.put("min", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "min"), false);
ud = defList.get(2); // http://qudt.org/vocab/unit#Tesla
symbolToUnitLookup.put("T", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "T"), false);
ud = defList.get(11); // http://qudt.org/vocab/unit#Volt
symbolToUnitLookup.put("V", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "V"), false);
ud = defList.get(35); // http://qudt.org/vocab/unit#Watt
symbolToUnitLookup.put("W", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "W"), false);
ud = defList.get(13); // http://qudt.org/vocab/unit#Erg
symbolToUnitLookup.put("erg", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, true, "erg"), false);
ud = defList.get(32); // http://bitbucket.org/nxg/unity/ns/unit#Pixel
symbolToUnitLookup.put("pixel", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "pixel"), true);
ud = defList.get(26); // http://qudt.org/vocab/unit#Decibel
symbolToUnitLookup.put("dB", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "dB"), false);
ud = defList.get(22); // http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond
symbolToUnitLookup.put("mas", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "mas"), false);
ud = defList.get(30); // http://bitbucket.org/nxg/unity/ns/unit#BesselianYear
symbolToUnitLookup.put("Ba", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, true, "Ba"), false);
ud = defList.get(62); // http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity
symbolToUnitLookup.put("solLum", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "solLum"), false);
ud = defList.get(3); // http://qudt.org/vocab/unit#Steradian
symbolToUnitLookup.put("sr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "sr"), false);
ud = defList.get(54); // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
symbolToUnitLookup.put("a", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "a"), false);
ud = defList.get(44); // http://qudt.org/vocab/unit#Candela
symbolToUnitLookup.put("cd", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "cd"), false);
ud = defList.get(49); // http://qudt.org/vocab/unit#Day
symbolToUnitLookup.put("d", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "d"), false);
ud = defList.get(25); // http://bitbucket.org/nxg/unity/ns/unit#Rydberg
symbolToUnitLookup.put("Ry", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Ry"), false);
ud = defList.get(38); // http://qudt.org/vocab/unit#Gram
symbolToUnitLookup.put("g", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "g"), false);
ud = defList.get(64); // http://qudt.org/vocab/unit#Hour
symbolToUnitLookup.put("h", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "h"), false);
ud = defList.get(40); // http://qudt.org/vocab/unit#Weber
symbolToUnitLookup.put("Wb", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Wb"), false);
ud = defList.get(23); // http://qudt.org/vocab/unit#Meter
symbolToUnitLookup.put("m", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "m"), false);
ud = defList.get(53); // http://qudt.org/vocab/unit#YearTropical
symbolToUnitLookup.put("ta", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, true, "ta"), false);
ud = defList.get(6); // http://bitbucket.org/nxg/unity/ns/unit#SolarRadius
symbolToUnitLookup.put("solRad", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "solRad"), false);
ud = defList.get(18); // http://bitbucket.org/nxg/unity/ns/unit#Jansky
symbolToUnitLookup.put("Jy", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Jy"), false);
ud = defList.get(43); // http://bitbucket.org/nxg/unity/ns/unit#Photon
symbolToUnitLookup.put("photon", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "photon"), true);
ud = defList.get(34); // http://qudt.org/vocab/unit#Number
symbolToUnitLookup.put("ct", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "ct"), false);
ud = defList.get(59); // http://qudt.org/vocab/unit#Parsec
symbolToUnitLookup.put("pc", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "pc"), false);
ud = defList.get(20); // http://qudt.org/vocab/unit#SecondTime
symbolToUnitLookup.put("s", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "s"), false);
ud = defList.get(63); // http://qudt.org/vocab/unit#UnifiedAtomicMassUnit
symbolToUnitLookup.put("u", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "u"), false);
ud = defList.get(31); // http://bitbucket.org/nxg/unity/ns/unit#Voxel
symbolToUnitLookup.put("voxel", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "voxel"), false);
ud = defList.get(43); // http://bitbucket.org/nxg/unity/ns/unit#Photon
symbolToUnitLookup.put("ph", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "ph"), false);
ud = defList.get(10); // http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel
symbolToUnitLookup.put("chan", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "chan"), false);
syntaxToUnitLookupMap.put(Syntax.VOUNITS, symbolToUnitLookup);
syntaxToUnitRepresentationMap.put(Syntax.VOUNITS, unitToRepresentationLookup);

// Lookup table for syntax fits
symbolToUnitLookup = new HashMap<String,UnitDefinition>();
unitToRepresentationLookup = new HashMap<UnitDefinition,UnitRepresentation>();
ud = defList.get(51); // http://bitbucket.org/nxg/unity/ns/unit#Beam
symbolToUnitLookup.put("beam", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "beam"), false);
ud = defList.get(5); // http://qudt.org/vocab/unit#Lumen
symbolToUnitLookup.put("lm", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lm"), false);
ud = defList.get(19); // http://qudt.org/vocab/unit#LightYear
symbolToUnitLookup.put("lyr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "lyr"), false);
ud = defList.get(46); // http://bitbucket.org/nxg/unity/ns/unit#DistributionBin
symbolToUnitLookup.put("bin", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "bin"), false);
ud = defList.get(8); // http://qudt.org/vocab/unit#DegreeAngle
symbolToUnitLookup.put("deg", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "deg"), false);
ud = defList.get(28); // http://qudt.org/vocab/unit#Mole
symbolToUnitLookup.put("mol", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "mol"), false);
ud = defList.get(39); // http://qudt.org/vocab/unit#Bit
symbolToUnitLookup.put("bit", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "bit"), false);
ud = defList.get(36); // http://qudt.org/vocab/unit#Lux
symbolToUnitLookup.put("lx", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "lx"), false);
ud = defList.get(14); // http://qudt.org/vocab/unit#Pascal
symbolToUnitLookup.put("Pa", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Pa"), false);
ud = defList.get(56); // http://qudt.org/vocab/unit#ElectronVolt
symbolToUnitLookup.put("eV", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "eV"), false);
ud = defList.get(50); // http://qudt.org/vocab/unit#Radian
symbolToUnitLookup.put("rad", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "rad"), false);
ud = defList.get(52); // http://qudt.org/vocab/unit#Ohm
symbolToUnitLookup.put("Ohm", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Ohm"), false);
ud = defList.get(42); // http://qudt.org/vocab/unit#Barn
symbolToUnitLookup.put("barn", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, true, "barn"), false);
ud = defList.get(54); // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
symbolToUnitLookup.put("yr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "yr"), false);
ud = defList.get(32); // http://bitbucket.org/nxg/unity/ns/unit#Pixel
symbolToUnitLookup.put("pix", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "pix"), false);
ud = defList.get(0); // http://qudt.org/vocab/unit#Byte
symbolToUnitLookup.put("byte", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "byte"), false);
ud = defList.get(34); // http://qudt.org/vocab/unit#Number
symbolToUnitLookup.put("count", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "count"), false);
ud = defList.get(27); // http://qudt.org/vocab/unit#Hertz
symbolToUnitLookup.put("Hz", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Hz"), false);
ud = defList.get(12); // http://bitbucket.org/nxg/unity/ns/unit#ADU
symbolToUnitLookup.put("adu", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "adu"), false);
ud = defList.get(15); // http://qudt.org/vocab/unit#AstronomicalUnit
symbolToUnitLookup.put("AU", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "AU"), false);
ud = defList.get(37); // http://qudt.org/vocab/unit#ArcMinute
symbolToUnitLookup.put("arcmin", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "arcmin"), false);
ud = defList.get(48); // http://qudt.org/vocab/unit#Angstrom
symbolToUnitLookup.put("Angstrom", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, true, "Angstrom"), false);
ud = defList.get(55); // http://qudt.org/vocab/unit#Ampere
symbolToUnitLookup.put("A", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "A"), false);
ud = defList.get(24); // http://qudt.org/vocab/unit#Coulomb
symbolToUnitLookup.put("C", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "C"), false);
ud = defList.get(57); // http://qudt.org/vocab/unit#Debye
symbolToUnitLookup.put("D", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "D"), false);
ud = defList.get(58); // http://qudt.org/vocab/unit#ArcSecond
symbolToUnitLookup.put("arcsec", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "arcsec"), false);
ud = defList.get(29); // http://qudt.org/vocab/unit#Farad
symbolToUnitLookup.put("F", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "F"), false);
ud = defList.get(21); // http://qudt.org/vocab/unit#Gauss
symbolToUnitLookup.put("G", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, true, "G"), false);
ud = defList.get(61); // http://qudt.org/vocab/unit#Henry
symbolToUnitLookup.put("H", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "H"), false);
ud = defList.get(47); // http://bitbucket.org/nxg/unity/ns/unit#SolarMass
symbolToUnitLookup.put("solMass", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "solMass"), false);
ud = defList.get(33); // http://qudt.org/vocab/unit#Joule
symbolToUnitLookup.put("J", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "J"), false);
ud = defList.get(4); // http://qudt.org/vocab/unit#Kelvin
symbolToUnitLookup.put("K", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "K"), false);
ud = defList.get(1); // http://qudt.org/vocab/unit#Newton
symbolToUnitLookup.put("N", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "N"), false);
ud = defList.get(60); // http://bitbucket.org/nxg/unity/ns/unit#Rayleigh
symbolToUnitLookup.put("R", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "R"), false);
ud = defList.get(16); // http://bitbucket.org/nxg/unity/ns/unit#StellarMagnitude
symbolToUnitLookup.put("mag", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "mag"), false);
ud = defList.get(17); // http://qudt.org/vocab/unit#Siemens
symbolToUnitLookup.put("S", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "S"), false);
ud = defList.get(7); // http://qudt.org/vocab/unit#MinuteTime
symbolToUnitLookup.put("min", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "min"), false);
ud = defList.get(2); // http://qudt.org/vocab/unit#Tesla
symbolToUnitLookup.put("T", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "T"), false);
ud = defList.get(11); // http://qudt.org/vocab/unit#Volt
symbolToUnitLookup.put("V", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "V"), false);
ud = defList.get(35); // http://qudt.org/vocab/unit#Watt
symbolToUnitLookup.put("W", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "W"), false);
ud = defList.get(13); // http://qudt.org/vocab/unit#Erg
symbolToUnitLookup.put("erg", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, true, "erg"), false);
ud = defList.get(32); // http://bitbucket.org/nxg/unity/ns/unit#Pixel
symbolToUnitLookup.put("pixel", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "pixel"), true);
ud = defList.get(22); // http://bitbucket.org/nxg/unity/ns/unit#MilliArcSecond
symbolToUnitLookup.put("mas", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "mas"), false);
ud = defList.get(30); // http://bitbucket.org/nxg/unity/ns/unit#BesselianYear
symbolToUnitLookup.put("Ba", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, true, "Ba"), false);
ud = defList.get(62); // http://bitbucket.org/nxg/unity/ns/unit#SolarLuminosity
symbolToUnitLookup.put("solLum", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "solLum"), false);
ud = defList.get(3); // http://qudt.org/vocab/unit#Steradian
symbolToUnitLookup.put("sr", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "sr"), false);
ud = defList.get(54); // http://bitbucket.org/nxg/unity/ns/unit#JulianYear
symbolToUnitLookup.put("a", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "a"), true);
ud = defList.get(44); // http://qudt.org/vocab/unit#Candela
symbolToUnitLookup.put("cd", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "cd"), false);
ud = defList.get(49); // http://qudt.org/vocab/unit#Day
symbolToUnitLookup.put("d", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "d"), false);
ud = defList.get(25); // http://bitbucket.org/nxg/unity/ns/unit#Rydberg
symbolToUnitLookup.put("Ry", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "Ry"), false);
ud = defList.get(38); // http://qudt.org/vocab/unit#Gram
symbolToUnitLookup.put("g", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "g"), false);
ud = defList.get(64); // http://qudt.org/vocab/unit#Hour
symbolToUnitLookup.put("h", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "h"), false);
ud = defList.get(40); // http://qudt.org/vocab/unit#Weber
symbolToUnitLookup.put("Wb", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Wb"), false);
ud = defList.get(23); // http://qudt.org/vocab/unit#Meter
symbolToUnitLookup.put("m", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "m"), false);
ud = defList.get(53); // http://qudt.org/vocab/unit#YearTropical
symbolToUnitLookup.put("ta", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, true, "ta"), false);
ud = defList.get(6); // http://bitbucket.org/nxg/unity/ns/unit#SolarRadius
symbolToUnitLookup.put("solRad", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "solRad"), false);
ud = defList.get(18); // http://bitbucket.org/nxg/unity/ns/unit#Jansky
symbolToUnitLookup.put("Jy", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "Jy"), false);
ud = defList.get(43); // http://bitbucket.org/nxg/unity/ns/unit#Photon
symbolToUnitLookup.put("photon", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "photon"), true);
ud = defList.get(34); // http://qudt.org/vocab/unit#Number
symbolToUnitLookup.put("ct", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "ct"), false);
ud = defList.get(59); // http://qudt.org/vocab/unit#Parsec
symbolToUnitLookup.put("pc", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "pc"), false);
ud = defList.get(20); // http://qudt.org/vocab/unit#SecondTime
symbolToUnitLookup.put("s", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(true, false, false, "s"), false);
ud = defList.get(63); // http://qudt.org/vocab/unit#UnifiedAtomicMassUnit
symbolToUnitLookup.put("u", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "u"), false);
ud = defList.get(31); // http://bitbucket.org/nxg/unity/ns/unit#Voxel
symbolToUnitLookup.put("voxel", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "voxel"), false);
ud = defList.get(41); // http://bitbucket.org/nxg/unity/ns/unit#JulianCentury
symbolToUnitLookup.put("cy", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "cy"), false);
ud = defList.get(43); // http://bitbucket.org/nxg/unity/ns/unit#Photon
symbolToUnitLookup.put("ph", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "ph"), false);
ud = defList.get(10); // http://bitbucket.org/nxg/unity/ns/unit#DetectorChannel
symbolToUnitLookup.put("chan", ud);
mapUnitToRepresentation(unitToRepresentationLookup, ud, new UnitRepresentation(false, false, false, "chan"), false);
syntaxToUnitLookupMap.put(Syntax.FITS, symbolToUnitLookup);
syntaxToUnitRepresentationMap.put(Syntax.FITS, unitToRepresentationLookup);

        return this;
    }
}

