package uk.me.nxg.unity;

class Token {
  public short i;     // the integer corresponding to this token
  public String name; // the name of the token
  public String type; // the type of the value (s/i/f), or null if there is no value
  Token(short i, String name, String type) { this.i=i; this.name=name; this.type=(type.equals("") ? null : type); };

  static final short LIT1 = Parser_fits.LIT1;
  static final short STAR = Parser_fits.STAR;
  static final short QUOTED_STRING = Parser_fits.QUOTED_STRING;
  static final short SIGNED_INTEGER = Parser_fits.SIGNED_INTEGER;
  static final short OPEN_P = Parser_fits.OPEN_P;
  static final short DIVISION = Parser_fits.DIVISION;
  static final short CARET = Parser_fits.CARET;
  static final short LIT10 = Parser_fits.LIT10;
  static final short UNSIGNED_INTEGER = Parser_fits.UNSIGNED_INTEGER;
  static final short CLOSE_P = Parser_fits.CLOSE_P;
  static final short VOUFLOAT = Parser_fits.VOUFLOAT;
  static final short STARSTAR = Parser_fits.STARSTAR;
  static final short DOT = Parser_fits.DOT;
  static final short STRING = Parser_fits.STRING;
  static final short FLOAT = Parser_fits.FLOAT;
  static final short WHITESPACE = Parser_fits.WHITESPACE;
  static final short CDSFLOAT = Parser_fits.CDSFLOAT;
  static final short CLOSE_SQ = Parser_fits.CLOSE_SQ;
  static final short OPEN_SQ = Parser_fits.OPEN_SQ;
  static final short PERCENT = Parser_fits.PERCENT;

  private static java.util.Map<Integer,Token> m = new java.util.HashMap<Integer,Token>();
  static {
    m.put(Integer.valueOf(LIT1), new Token(LIT1, "LIT1", ""));
    m.put(Integer.valueOf(STAR), new Token(STAR, "STAR", ""));
    m.put(Integer.valueOf(QUOTED_STRING), new Token(QUOTED_STRING, "QUOTED_STRING", "s"));
    m.put(Integer.valueOf(SIGNED_INTEGER), new Token(SIGNED_INTEGER, "SIGNED_INTEGER", "i"));
    m.put(Integer.valueOf(OPEN_P), new Token(OPEN_P, "OPEN_P", ""));
    m.put(Integer.valueOf(DIVISION), new Token(DIVISION, "DIVISION", ""));
    m.put(Integer.valueOf(CARET), new Token(CARET, "CARET", ""));
    m.put(Integer.valueOf(LIT10), new Token(LIT10, "LIT10", ""));
    m.put(Integer.valueOf(UNSIGNED_INTEGER), new Token(UNSIGNED_INTEGER, "UNSIGNED_INTEGER", "i"));
    m.put(Integer.valueOf(CLOSE_P), new Token(CLOSE_P, "CLOSE_P", ""));
    m.put(Integer.valueOf(VOUFLOAT), new Token(VOUFLOAT, "VOUFLOAT", "l"));
    m.put(Integer.valueOf(STARSTAR), new Token(STARSTAR, "STARSTAR", ""));
    m.put(Integer.valueOf(DOT), new Token(DOT, "DOT", ""));
    m.put(Integer.valueOf(STRING), new Token(STRING, "STRING", "s"));
    m.put(Integer.valueOf(FLOAT), new Token(FLOAT, "FLOAT", "f"));
    m.put(Integer.valueOf(WHITESPACE), new Token(WHITESPACE, "WHITESPACE", ""));
    m.put(Integer.valueOf(CDSFLOAT), new Token(CDSFLOAT, "CDSFLOAT", "l"));
    m.put(Integer.valueOf(CLOSE_SQ), new Token(CLOSE_SQ, "CLOSE_SQ", ""));
    m.put(Integer.valueOf(OPEN_SQ), new Token(OPEN_SQ, "OPEN_SQ", ""));
    m.put(Integer.valueOf(PERCENT), new Token(PERCENT, "PERCENT", ""));
  }
  public static Token lookup(int i) { return m.get(Integer.valueOf(i)); };
}
