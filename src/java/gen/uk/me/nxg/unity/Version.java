package uk.me.nxg.unity;

/**
 * Manage version information
 */
public class Version {
    private static final String versionString = "1.1, released 2023 October 22 (Hg-dc4b67299583 (tag 1.1) at https://heptapod.host/nxg/unity)";
    private static final int versionNumber = 1001000;

    private Version() {
        // constructor is protected on purpuse
    }

    /**
     * Indicate the package version, as a printable string giving the package name and version
     * @return a string representation of the version
     */
    public static String versionString() {
        return versionString;
    }

    /**
     * Indicate the package version, as an integer.
     * The number returned is major-version * 1e6 + minor-version * 1e3 + release.
     * @return an integer representation of the version
     */
    public static int versionInteger() {
        return versionNumber;
    }
}
