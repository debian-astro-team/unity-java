//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";



package uk.me.nxg.unity;



//#line 4 "unity-java,vounits.y"
import java.util.List;
//#line 19 "Parser_vounits.java"




class Parser_vounits
             extends uk.me.nxg.unity.Parser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//## **user defined:UnitVal
String   yytext;//user variable to return contextual strings
UnitVal yyval; //used to return semantic vals from action routines
//UnitVal yylval;//the 'lval' (result) I got from yylex()
UnitVal valstk[] = new UnitVal[YYSTACKSIZE];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
final void val_init()
{
  yyval=new UnitVal();
  yylval=new UnitVal();
  valptr=-1;
}
final void val_push(UnitVal val)
{
  try {
    valptr++;
    valstk[valptr]=val;
  }
  catch (ArrayIndexOutOfBoundsException e) {
    int oldsize = valstk.length;
    int newsize = oldsize*2;
    UnitVal[] newstack = new UnitVal[newsize];
    System.arraycopy(valstk,0,newstack,0,oldsize);
    valstk = newstack;
    valstk[valptr]=val;
  }
}
final UnitVal val_pop()
{
  return valstk[valptr--];
}
final void val_drop(int cnt)
{
  valptr -= cnt;
}
final UnitVal val_peek(int relative)
{
  return valstk[valptr-relative];
}
final UnitVal dup_yyval(UnitVal val)
{
  return val;
}
//#### end semantic value section ####
final static short SIGNED_INTEGER=257;
final static short UNSIGNED_INTEGER=258;
final static short FLOAT=259;
final static short STRING=260;
final static short QUOTED_STRING=261;
final static short VOUFLOAT=262;
final static short CDSFLOAT=263;
final static short WHITESPACE=264;
final static short STARSTAR=265;
final static short CARET=266;
final static short DIVISION=267;
final static short DOT=268;
final static short STAR=269;
final static short PERCENT=270;
final static short OPEN_P=271;
final static short CLOSE_P=272;
final static short OPEN_SQ=273;
final static short CLOSE_SQ=274;
final static short LIT10=275;
final static short LIT1=276;
final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    0,    0,    8,    8,    6,    6,    7,    7,    7,
   14,    9,   11,   11,   10,   10,   10,   10,   12,    3,
    3,    4,    4,    4,    4,   15,    1,    1,    2,    2,
    2,    5,    5,   13,
};
final static short yylen[] = {                            2,
    1,    2,    1,    1,    3,    1,    3,    1,    1,    3,
    0,    5,    1,    2,    3,    1,    1,    1,    1,    1,
    3,    1,    1,    2,    1,    1,    1,    1,    3,    3,
    5,    1,    1,    1,
};
final static short yydefred[] = {                         0,
    0,   23,   18,   25,    0,    0,    0,    0,    8,    0,
    0,    6,    1,    9,    0,   24,   11,    0,   26,    0,
    0,   19,   34,    0,    0,    2,    0,   10,   32,   33,
    0,   15,   28,   27,   21,    5,    7,   17,   13,    0,
    0,    0,    0,   14,   12,   30,   29,    0,    0,   31,
};
final static short yydgoto[] = {                          8,
   32,   33,    9,   10,   34,   11,   12,   13,   14,   15,
   41,   24,   25,   27,   20,
};
final static short yysindex[] = {                      -256,
 -235,    0,    0,    0, -243, -236,    0,    0,    0, -236,
 -224,    0,    0,    0, -243,    0,    0, -263,    0, -250,
 -250,    0,    0, -243, -243,    0, -237,    0,    0,    0,
 -246,    0,    0,    0,    0,    0,    0,    0,    0, -243,
 -240, -223, -225,    0,    0,    0,    0, -221, -222,    0,
};
final static short yyrindex[] = {                         0,
    1,    0,    0,    0,    0, -230,   16,    0,    0,    3,
    2,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
};
final static short yygindex[] = {                         0,
   27,    0,    0,    0,   20,    0,   21,   -5,    0,   25,
    0,   10,    0,    0,   44,
};
final static int YYTABLESIZE=287;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         18,
   22,    4,   20,    1,    2,    3,   29,   30,   28,   26,
   29,   30,   42,    4,    5,    3,    1,    2,    6,    7,
   31,   39,    1,    2,    3,   16,    4,    5,   19,   16,
   16,   45,    4,    5,   44,   17,   49,    6,   38,   16,
   16,   22,   22,   23,   36,   37,   47,   35,   46,   50,
   43,   40,   48,   21,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,   22,    0,   22,   22,   20,
   20,    0,   22,    4,   20,   17,   17,    0,    0,    0,
    0,    0,    0,    0,    0,   17,   17,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                          5,
    0,    0,    0,  260,  261,  262,  257,  258,  272,   15,
  257,  258,  259,  270,  271,    0,  260,  261,  275,  276,
  271,   27,  260,  261,  262,  261,  270,  271,  265,  260,
  261,  272,  270,  271,   40,  271,  258,  275,  276,  270,
  271,  267,  267,  268,   24,   25,  272,   21,  272,  272,
   31,   27,   43,   10,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  265,   -1,  267,  268,  267,
  268,   -1,  272,  272,  272,  260,  261,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  270,  271,
};
}
final static short YYFINAL=8;
final static short YYMAXTOKEN=276;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,"SIGNED_INTEGER","UNSIGNED_INTEGER","FLOAT","STRING",
"QUOTED_STRING","VOUFLOAT","CDSFLOAT","WHITESPACE","STARSTAR","CARET",
"DIVISION","DOT","STAR","PERCENT","OPEN_P","CLOSE_P","OPEN_SQ","CLOSE_SQ",
"LIT10","LIT1",
};
final static String yyrule[] = {
"$accept : input",
"input : complete_expression",
"input : scalefactor complete_expression",
"input : LIT1",
"complete_expression : product_of_units",
"complete_expression : product_of_units division unit_expression",
"product_of_units : unit_expression",
"product_of_units : product_of_units product unit_expression",
"unit_expression : term",
"unit_expression : function_application",
"unit_expression : OPEN_P complete_expression CLOSE_P",
"$$1 :",
"function_application : STRING OPEN_P $$1 function_operand CLOSE_P",
"function_operand : complete_expression",
"function_operand : scalefactor complete_expression",
"scalefactor : LIT10 power numeric_power",
"scalefactor : LIT10",
"scalefactor : LIT1",
"scalefactor : VOUFLOAT",
"division : DIVISION",
"term : unit",
"term : unit power numeric_power",
"unit : STRING",
"unit : QUOTED_STRING",
"unit : STRING QUOTED_STRING",
"unit : PERCENT",
"power : STARSTAR",
"numeric_power : integer",
"numeric_power : parenthesized_number",
"parenthesized_number : OPEN_P integer CLOSE_P",
"parenthesized_number : OPEN_P FLOAT CLOSE_P",
"parenthesized_number : OPEN_P integer division UNSIGNED_INTEGER CLOSE_P",
"integer : SIGNED_INTEGER",
"integer : UNSIGNED_INTEGER",
"product : DOT",
};

//#line 193 "unity-java,vounits.y"

//private Yylex lexer;
private int yylex()
{
    int rval = -1;
    try {
        yylval = new UnitVal(); // invalid UnitVal
        rval = lexer.yylex();
    } catch (java.io.IOException e) {
        System.err.println("IO exception: " + e);
        rval = -1;
    }
    return rval;
}
void yyerror(String error) 
        throws UnitParserException
{
    setParseResult(null);
    // lexer.parsePosition() returns a zero-offset count of characters
    throw new UnitParserException("unity parser error at character " + (lexer.parsePosition()+1) + ": " + error);
}
/* Parser(java.io.Reader r) { */
/*     lexer = new Yylex(r, this); */
/* } */
private UnitExpr parseResult;
private void setParseResult(UnitExpr u) {
    parseResult = u;
}
UnitExpr getParseResult() {
    return parseResult;
}
//#line 315 "Parser_vounits.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
protected int yyparse()
throws uk.me.nxg.unity.UnitParserException
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 46 "unity-java,vounits.y"
{
                        this.setParseResult(new UnitExpr(0, val_peek(0).uList));
          }
break;
case 2:
//#line 49 "unity-java,vounits.y"
{
                        this.setParseResult(new UnitExpr(val_peek(1).f, val_peek(0).uList));
        }
break;
case 3:
//#line 52 "unity-java,vounits.y"
{
			this.setParseResult(UnitExpr.getDimensionlessExpression());
          }
break;
case 4:
//#line 57 "unity-java,vounits.y"
{
                      yyval.uList = val_peek(0).uList;
          }
break;
case 5:
//#line 81 "unity-java,vounits.y"
{
                      val_peek(2).uList.addAll(OneUnit.reciprocate(val_peek(0).uList)); yyval.uList = val_peek(2).uList;
          }
break;
case 6:
//#line 86 "unity-java,vounits.y"
{
            yyval.uList = val_peek(0).uList;
          }
break;
case 7:
//#line 89 "unity-java,vounits.y"
{
                      val_peek(2).uList.addAll(val_peek(0).uList); yyval.uList = val_peek(2).uList;
          }
break;
case 8:
//#line 94 "unity-java,vounits.y"
{
                      List<OneUnit> newList = new java.util.LinkedList<OneUnit>();
                      newList.add(val_peek(0).u);
                      yyval.uList = newList;
          }
break;
case 9:
//#line 99 "unity-java,vounits.y"
{
            yyval.uList = val_peek(0).uList;
          }
break;
case 10:
//#line 102 "unity-java,vounits.y"
{
            yyval.uList = val_peek(1).uList;
          }
break;
case 11:
//#line 107 "unity-java,vounits.y"
{
lexer.yybegin(Yylex.vouinitial);
/*                                    The following is BEGIN(vouinitial),*/
/*                                    but BEGIN is defined internally to the generated lexer,*/
/*                                    and isn't exposed, so...*/
/*                                    we have to use the force_lexer_state function*/
/*                                    that the Makefile smuggled into unity-lexer.c*/
/*                                    ... YUK (no, I don't like this any more than you do)*/
                                    }
break;
case 12:
//#line 115 "unity-java,vounits.y"
{
                      FunctionDefinition fd = functionMaker.make(val_peek(4).s);
                      UnitExpr ue = val_peek(1).uExpr;
                      List<OneUnit> newList = new java.util.LinkedList<OneUnit>();
                      newList.add(fd == null
                           ? new FunctionOfUnit(val_peek(4).s, ue.getUnitsList(), ue.getLogFactor())
                           : new FunctionOfUnit(fd, ue.getUnitsList(), ue.getLogFactor()));
                      yyval.uList = newList;
          }
break;
case 13:
//#line 124 "unity-java,vounits.y"
{
                      yyval.uExpr = new UnitExpr(0.0, val_peek(0).uList);
          }
break;
case 14:
//#line 127 "unity-java,vounits.y"
{
                      yyval.uExpr = new UnitExpr(val_peek(1).f, val_peek(0).uList);
        }
break;
case 15:
//#line 131 "unity-java,vounits.y"
{ /* eg 10^3*/
yyval.f = Double.valueOf(val_peek(0).f);
                  }
break;
case 16:
//#line 134 "unity-java,vounits.y"
{
            yyval.f = 1;  /* log(10) = 1 */
          }
break;
case 17:
//#line 137 "unity-java,vounits.y"
{
            yyval.f = 0;  /* log(1) = 0 */
          }
break;
case 18:
//#line 140 "unity-java,vounits.y"
{
                      yyval.f = val_peek(0).l10f;
          }
break;
case 20:
//#line 147 "unity-java,vounits.y"
{ yyval.u = val_peek(0).u; }
break;
case 21:
//#line 148 "unity-java,vounits.y"
{
                      yyval.u = val_peek(2).u.pow(val_peek(0).f);
          }
break;
case 22:
//#line 153 "unity-java,vounits.y"
{
                      yyval.u = unitMaker.make(val_peek(0).s, 1);
          }
break;
case 23:
//#line 156 "unity-java,vounits.y"
{
                      OneUnit u = unitMaker.makeQuotedUnit(null, val_peek(0).s, 1);
                      if (u == null) yyerror("unexpected error creating unit: " + val_peek(0).s);
                      yyval.u = u;
          }
break;
case 24:
//#line 161 "unity-java,vounits.y"
{
                      OneUnit u = unitMaker.makeQuotedUnit(val_peek(1).s, val_peek(0).s, 1);
                      if (u == null) yyerror("error creating unit -- bad prefix?: " + val_peek(1).s);
                      yyval.u = u;
          }
break;
case 25:
//#line 166 "unity-java,vounits.y"
{
                      yyval.u = unitMaker.make("%", 1);
          }
break;
case 27:
//#line 173 "unity-java,vounits.y"
{ yyval.f = val_peek(0).i; }
break;
case 28:
//#line 174 "unity-java,vounits.y"
{ yyval.f = val_peek(0).f; }
break;
case 29:
//#line 177 "unity-java,vounits.y"
{
            yyval.f = val_peek(1).i;
          }
break;
case 30:
//#line 180 "unity-java,vounits.y"
{
            yyval.f = val_peek(1).f;
          }
break;
case 31:
//#line 183 "unity-java,vounits.y"
{
            /* This is certainly OK for FITS, not clear for OGIP*/
            yyval.f = ((float)val_peek(3).i)/((float)(val_peek(1).i));
          }
break;
//#line 651 "Parser_vounits.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
//## The -Jnorun option was used ##
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
Parser_vounits()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
Parser_vounits(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
