package uk.me.nxg.unity;

interface SyntaxMaker {
    public Parser makeParser()
            throws UnitParserException;
    public UnitWriter makeWriter(UnitExpr e)
            throws UnwritableExpression;
}
