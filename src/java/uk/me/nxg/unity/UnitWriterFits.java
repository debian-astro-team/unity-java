package uk.me.nxg.unity;

class UnitWriterFits extends UnitWriter {

    public UnitWriterFits(UnitExpr ue) {
        super(ue);
    }

    public String write() {
        StringBuilder sb = new StringBuilder();
        boolean first = true;

        try {
            if (ue.getLogFactor() != 0.0) {
                sb.append("10^");
                writeNumber(sb, ue.getLogFactor());
                first = false;
            }
            for (OneUnit u : ue) {
                // FIXME: should I reorder the units to get all of the negative powers to the end?
                if (first) {
                    first = false;
                } else {
                    sb.append(' ');
                }
                sb.append(u.unitString(Syntax.FITS));
                double e = u.getExponent();
                if (e != 1.0) {
                    writeNumber(sb, e);
                }
            }
            return sb.toString();
        } catch (UnitParserException e) {
            throw new AssertionError("units system failed to recognise built-in syntax FITS: " + e);
        }
    }


    void writeNumber(StringBuilder sb, double d) {
        writeNumber(sb, d, true, false, false, false);
    }
}
