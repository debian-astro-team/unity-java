package uk.me.nxg.unity;

class UnitWriterVOUnits extends UnitWriter {

    public UnitWriterVOUnits(UnitExpr ue) {
        super(ue);
    }

    public String write() {
        StringBuilder sb = new StringBuilder();
        boolean first = true;

        // special-case the dimensionless expression
        if (ue.size() == 0) return "1";

        try {
            if (ue.getLogFactor() == 0.0) {
                // do nothing
            } else if (ue.getLogFactor() == 1.0) {
                // special-case this
                sb.append("10");
            } else {
                writeNumber(sb, ue.getLogFactor(), true, false, false, true);
            }

            for (OneUnit u : ue) {
                // FIXME: should I reorder the units to get all of the negative powers to the end?
                if (first) {
                    first = false;
                } else {
                    sb.append('.');
                }
                sb.append(u.unitString(Syntax.VOUNITS));
                double e = u.getExponent();
                if (e != 1.0) {
                    sb.append("**");
                    writeNumber(sb, e);
                }
            }
            return sb.toString();
        } catch (UnitParserException e) {
            throw new AssertionError("units system failed to recognise built-in syntax VOUnits: " + e);
        }
    }


    void writeNumber(StringBuilder sb, double d) {
        writeNumber(sb, d, true, false, false, false);
    }
}
