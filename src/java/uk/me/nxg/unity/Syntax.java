package uk.me.nxg.unity;


/**
 * An enumeration of the allowed syntaxes within the Unity libary.
 */
public enum Syntax
{
    /**
     * The syntax for VOUnit-format strings.
     * This is intended to be
     * as nearly as possible in the intersection of the various
     * other grammars. It is a strict subset of the FITS and CDS
     * grammars (in the sense that any VOUnit unit string is a valid
     * FITS and CDS string, too), and it is almost a subset of the
     * OGIP grammar, except that it uses the dot for multiplication
     * rather than star.  See <a
     * href='http://ivoa.net/Documents/VOUnits/' >IVOA VOUnits
     * Proposed Recommendation</a>.
     */
    VOUNITS("vounits", true, true),

    /**
     * The syntax for FITS-format strings.
     * Parses unit strings according to the prescriptions in the
     * FITS specification, v3.0, section 4.3
     * (W.D. Pence et al., A&amp;A 524, A42, 2010.
     * <a href='https://doi.org/10.1051/0004-6361/201015362'
     *    >doi:10.1051/0004-6361/201015362</a>).
     */
    FITS("fits", true, true),

    /**
     * The syntax for OGIP-format strings.
     * The format defined in
     * <a href='https://heasarc.gsfc.nasa.gov/docs/heasarc/ofwg/docs/general/ogip_93_001/'
     *    >OGIP memo OGIP/93-001, 1993</a>)
     */
    OGIP("ogip", true, true),

    /**
     * The syntax for CDS-format strings.
     * A syntax based on the CDS document
     * <em>Standards for Astronomical Catalogues, Version 2.0</em>, 2000,
     * specifically.
     * See <a href='https://vizier.u-strasbg.fr/vizier/doc/catstd-3.2.htx'
     *     ><code>section 3.2</code></a>
     */
    CDS("cds", true, true),

    /** The formatter (not parser) for LaTeX/siunitx output. */
    LATEX("latex", false, true),

    /** The formatter (not parser) for debugging output.  This is
     * intended to display the results of a parse unambiguously.  The
     * format of the output is not specified, and may change without
     * notice.
     */
    DEBUG("debug", false, true),

    /**
     * A wildcard ‘syntax’, denoting all available syntaxes.
     * This syntax is neither readable nor writable.
     */
    ALL("all", false, false);

    private String abbrev;
    private boolean is_readable_p;
    private boolean is_writable_p;

    Syntax(String abbrev, boolean is_readable_p, boolean is_writable_p)
    {
        this.abbrev = abbrev;
        this.is_readable_p = is_readable_p;
        this.is_writable_p = is_writable_p;
    }

    /** Returns a string version of the syntax name */
    public String toString()
    {
        return abbrev;
    }

    /**
     * Indicates whether the syntax is a readable one.  The LaTeX and
     * ‘debug’ syntaxes, for example, are write-only.
     * @return true if the syntax is readable
     */
    public boolean isReadable()
    {
        return is_readable_p;
    }

    /**
     * Indicates whether the syntax is a writable one.  At present,
     * all the syntaxes are writable, except for {@code ALL}.
     * @return true if the syntax is writable
     */
    public boolean isWritable()
    {
        return is_writable_p;
    }

    /**
     * Look up a syntax enumeration from a string name.
     * @param name the name of a syntax
     * @return a Syntax, or null if the name was not recognised
     */
    public static Syntax lookup(String name)
    {
        for (Syntax s : Syntax.values()) {
            if (s.abbrev.equals(name)) {
                return s;
            }
        }
        return null;
    }
}
