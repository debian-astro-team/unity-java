package uk.me.nxg.unity;

/**
 * Thrown when an expression cannot be written in a particular syntax.
 * For example, the CDS syntax permits only the log() function in a unit string,
 * and nothing at all can be written in the notional {@link uk.me.nxg.unity.Syntax#ALL}.
 */
public class UnwritableExpression extends UnitParserException {
    /**
     * Specify serialVersionUID. We don't expect to serialize this,
     * but this suppresses a javac warning.
     */
    private static final long serialVersionUID = 1L;

    UnwritableExpression() {
        super();
    }
    UnwritableExpression(String message) {
        super(message);
    }
    UnwritableExpression(String message, Throwable cause) {
        super(message,cause);
    }
    UnwritableExpression(Throwable cause) {
        super(cause);
    }
}
