package uk.me.nxg.unity;

/**
 * Describes a function which is associated with a particular syntax.
 * The only difference from a basic {@link FunctionDefinition} is that
 * this is able to provide a {@link #name} for the function, in a
 * particular syntax.
 */
class FunctionDefinitionInSyntax extends FunctionDefinition {
    private final String name;

    FunctionDefinitionInSyntax(String description, String latexForm, String name) {
        super(description, latexForm, name);
        this.name = name;
    }

    public String name() {
        return name;
    }
}
