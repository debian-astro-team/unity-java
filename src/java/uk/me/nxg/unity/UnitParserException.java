package uk.me.nxg.unity;

/**
 * Thrown when an expression cannot be parsed.
 */
public class UnitParserException extends Exception {

    /**
     * Specify serialVersionUID. We don't expect to serialize this,
     * but this suppresses a javac warning.
     */
    private static final long serialVersionUID = 1L;

    UnitParserException() {
        super();
    }
    UnitParserException(String message) {
        super(message);
    }
    UnitParserException(String message, Throwable cause) {
        super(message,cause);
    }
    UnitParserException(Throwable cause) {
        super(cause);
    }
}
